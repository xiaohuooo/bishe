---
title: 个人项目
language_tabs:
  - shell: Shell
  - http: HTTP
  - javascript: JavaScript
  - ruby: Ruby
  - python: Python
  - php: PHP
  - java: Java
  - go: Go
toc_footers: []
includes: []
search: true
code_clipboard: true
highlight_theme: darkula
headingLevel: 2
generator: "@tarslib/widdershins v4.0.22"

---

# 个人项目

Base URLs:

* <a href="https://pure-deep-silkworm.ngrok-free.app">座位预定: https://pure-deep-silkworm.ngrok-free.app</a>

# Authentication

# PreviewBook/学生管理

## POST 学生注册

POST /registerStudent

> Body 请求参数

```json
{
  "username": "admin4",
  "password": "Szrz@2023",
  "confirmPassword": "Szrz@2023",
  "code": "",
  "uuid": ""
}
```

### 请求参数

|名称|位置|类型|必选|中文名|说明|
|---|---|---|---|---|---|
|Authorization|header|string| 否 ||none|
|body|body|object| 否 ||none|
|» loginName|body|string| 是 | 登录名称 |none|
|» userName|body|string| 是 | 用户名称 |none|
|» email|body|string| 是 | 用户邮箱|none|
|» phonenumber|body|string| 是 | 手机号码|none|
|» password|body|string| 是 | 密码|none|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 学生列表（分页）

GET /system/user/list

### 请求参数

|名称|位置|类型|必选|中文名|说明|
|---|---|---|---|---|---|
|userType|query|string| 否 ||none|
|Authorization|header|string| 否 ||none|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## PUT 修改用户

PUT /system/user

> Body 请求参数

```json
"{\r\n    // \"createBy\": \"\",\r\n    // \"createTime\": \"2024-03-07 23:01:16\",\r\n    // \"updateBy\": null,\r\n    // \"updateTime\": null,\r\n    \"remark\": null,\r\n    \"userId\": 102,\r\n    // \"deptId\": null,\r\n    \"userName\": \"admin6\",\r\n    \"userType\": \"00\",\r\n    \"nickName\": \"admin6\",\r\n    \"email\": \"archyqx@gmail.com\",\r\n    \"phonenumber\": \"18829539116\",\r\n    \"sex\": \"0\",\r\n    // \"avatar\": \"\",\r\n    \"password\": \"\",\r\n    \"status\": \"0\",\r\n    // \"delFlag\": \"0\",\r\n    // \"loginIp\": \"\",\r\n    // \"loginDate\": null,\r\n    // \"dept\": null,\r\n    // \"roles\": [],\r\n    // \"roleIds\": [],\r\n    // \"postIds\": [],\r\n    // \"roleId\": null,\r\n    \"score\": 10,\r\n    \"reservationId\": null,\r\n    \"admin\": false\r\n}"
```

### 请求参数

|名称|位置|类型|必选|中文名|说明|
|---|---|---|---|---|---|
|body|body|object| 否 ||none|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 修改保存用户

POST /system/user/edit

> Body 请求参数

```json
{}
```

### 请求参数

|名称|位置|类型|必选|中文名|说明|
|---|---|---|---|---|---|
|body|body|object| 否 ||none|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 根据用户编号获取详细信息

GET /system/user/userId

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## DELETE 删除用户

DELETE /system/user/userIds

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## PUT 重置密码

PUT /system/user/resetPwd

> Body 请求参数

```json
{
  "userId": 101,
  "password": "123456"
}
```

### 请求参数

|名称|位置|类型|必选|中文名|说明|
|---|---|---|---|---|---|
|body|body|object| 否 ||none|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# PreviewBook/管理员

## POST 新增管理员

POST /system/user

> Body 请求参数

```json
"{\r\n    // \"createBy\": \"admin\",\r\n    // \"createTime\": \"2024-03-07 20:29:32\",\r\n    // \"updateBy\": null,\r\n    // \"updateTime\": null,\r\n    // \"loginIp\": \"172.24.87.101\",\r\n    // \"loginDate\": \"2024-03-08T16:32:36.000+08:00\",\r\n    // \"userId\": 100,\r\n    // \"delFlag\": \"0\",\r\n    // \"userType\": \"00\",\r\n    // \"nickName\": \"2024030705\",\r\n    \"deptId\": 100,\r\n    \"userName\": \"2024030705\",\r\n    \"remark\": \"111\",\r\n    \"email\": \"archyqx@gmail.com\",\r\n    \"phonenumber\": \"18829539116\",\r\n    \"sex\": \"0\",\r\n    \"avatar\": \"\",\r\n    \"password\": \"\",\r\n    \"status\": \"0\",\r\n    \"dept\": null,\r\n    \"roles\": [],\r\n    \"roleIds\": [\r\n        2\r\n    ],\r\n    \"postIds\": [\r\n        1\r\n    ],\r\n    \"roleId\": null,\r\n    \"score\": null,\r\n    \"reservationId\": null,\r\n    \"admin\": false\r\n}"
```

### 请求参数

|名称|位置|类型|必选|中文名|说明|
|---|---|---|---|---|---|
|body|body|object| 否 ||none|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# PreviewBook/通知与公告

## GET 查询公告列表

GET /system/notice/list

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 新增通知公告

POST /system/notice

> Body 请求参数

```json
"{\r\n    \"noticeTitle\": \"标题\", //公告标题\r\n    \"noticeType\": \"1\", //公告类型 0为公告 1为通知\r\n    \"noticeContent\": \"<p>2134额</p>\", //公告内容\r\n    \"status\": \"0\" //状态 0为正常 1为关闭\r\n}"
```

### 请求参数

|名称|位置|类型|必选|中文名|说明|
|---|---|---|---|---|---|
|body|body|object| 否 ||none|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## PUT 修改通知公告

PUT /system/notice

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 删除通知公告

POST /system/notice/noticeIds

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# PreviewBook/意见反馈

## GET 查询意见反馈列表

GET /system/feedback/list

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 新增保存意见反馈

POST /system/feedback

> Body 请求参数

```json
"{\r\n    // \"createBy\": \"\",\r\n    // \"createTime\": \"2024-03-06 21:35:55\",\r\n    // \"updateBy\": \"\",\r\n    // \"updateTime\": \"2024-03-06 21:36:14\",\r\n    // \"delFlag\": \"\"\r\n    // \"id\": 1,\r\n    \"feedbackTitle\": \"111\",//意见反馈标题\r\n    \"feedbackContent\": \"1111\",//意见反馈内容\r\n    \"feedbackLocation\": \"111\",//意见反馈位置信息\r\n    // \"feedbackHandle\": \"\",//是否已处理  （默认0、1为已处理）\r\n    \"remark\": \"\"\r\n}"
```

### 请求参数

|名称|位置|类型|必选|中文名|说明|
|---|---|---|---|---|---|
|body|body|object| 否 ||none|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## PUT 修改保存意见反馈

PUT /system/feedback

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## DELETE 删除意见反馈

DELETE /system/feedback/ids

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 获取意见反馈详细信息

GET /system/feedback/id

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# PreviewBook/座位管理

## GET 查询座位管理列表

GET /system/seat/list

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 新增座位管理

POST /system/seat

> Body 请求参数

```json
"{\r\n    // \"createBy\": \"\",\r\n    // \"createTime\": \"2024-03-07 08:54:04\",\r\n    // \"updateBy\": \"\",\r\n    // \"updateTime\": null,\r\n    // \"delFlag\": \"\"\r\n    \"remark\": \"\",\r\n    // \"seatId\": 1,\r\n    \"seatLocation\": \"图书馆一层A区\",\r\n    // \"seatStatus\": null,\r\n    \"seatNumber\": \"01\",\r\n\r\n}"
```

### 请求参数

|名称|位置|类型|必选|中文名|说明|
|---|---|---|---|---|---|
|body|body|object| 否 ||none|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## PUT 修改座位管理

PUT /system/seat

> Body 请求参数

```json
"{\r\n    // \"createBy\": \"\",\r\n    // \"createTime\": \"2024-03-07 20:34:51\",\r\n    // \"updateBy\": \"\",\r\n    // \"updateTime\": \"2024-03-09 16:57:59\",\r\n    \"remark\": \"121\",\r\n    \"seatId\": 3,\r\n    \"seatLocation\": \"自习室A区\",\r\n    \"seatStatus\": \"0\",//座位状态 0为正常 1为故障\r\n    \"seatNumber\": \"01\",\r\n    // \"delFlag\": \"0\"\r\n}"
```

### 请求参数

|名称|位置|类型|必选|中文名|说明|
|---|---|---|---|---|---|
|body|body|object| 否 ||none|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## DELETE 删除座位管理

DELETE /system/seat/seatIds

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 获取座位管理详细信息

GET /system/seat/seatId

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# PreviewBook/座位预约管理

## GET 查询座位预约列表

GET /system/reservation/list

### 请求参数

|名称|位置|类型|必选|中文名|说明|
|---|---|---|---|---|---|
|pageNum|query|string| 否 ||none|
|pageSize|query|string| 否 ||none|
|studentId|query|string| 否 ||用户id|

> 返回示例

> 成功

```json
{
  "total": 15,
  "rows": [
    {
      "createBy": "",
      "createTime": "2024-03-07 22:06:38",
      "updateBy": "",
      "updateTime": "2024-03-08 15:36:46",
      "remark": null,
      "reservationId": 9,
      "studentId": 100,
      "seatId": 6,
      "reservationDate": "2024-03-08",
      "timeSlot": 2,
      "isActive": "0",
      "isCancal": "0",
      "delFlag": "0"
    },
    {
      "createBy": "",
      "createTime": "2024-03-08 12:54:17",
      "updateBy": "",
      "updateTime": "2024-03-09 08:51:25",
      "remark": null,
      "reservationId": 11,
      "studentId": 100,
      "seatId": 3,
      "reservationDate": "2024-03-09",
      "timeSlot": 1,
      "isActive": "1",
      "isCancal": "0",
      "delFlag": "0"
    },
    {
      "createBy": "",
      "createTime": "2024-03-08 13:00:52",
      "updateBy": "",
      "updateTime": "2024-03-09 09:01:47",
      "remark": null,
      "reservationId": 13,
      "studentId": 100,
      "seatId": 5,
      "reservationDate": "2024-03-09",
      "timeSlot": 2,
      "isActive": "0",
      "isCancal": "1",
      "delFlag": "0"
    },
    {
      "createBy": "",
      "createTime": "2024-03-08 13:05:15",
      "updateBy": "",
      "updateTime": "2024-03-09 09:05:42",
      "remark": null,
      "reservationId": 14,
      "studentId": 100,
      "seatId": 4,
      "reservationDate": "2024-03-09",
      "timeSlot": 3,
      "isActive": "0",
      "isCancal": "1",
      "delFlag": "0"
    },
    {
      "createBy": "",
      "createTime": "2024-03-08 13:05:23",
      "updateBy": "",
      "updateTime": "2024-03-09 09:10:16",
      "remark": null,
      "reservationId": 15,
      "studentId": 100,
      "seatId": 4,
      "reservationDate": "2024-03-09",
      "timeSlot": 2,
      "isActive": "1",
      "isCancal": "0",
      "delFlag": "0"
    },
    {
      "createBy": "",
      "createTime": "2024-03-08 13:05:27",
      "updateBy": "",
      "updateTime": "2024-03-09 09:10:43",
      "remark": null,
      "reservationId": 16,
      "studentId": 100,
      "seatId": 4,
      "reservationDate": "2024-03-09",
      "timeSlot": 1,
      "isActive": "1",
      "isCancal": "0",
      "delFlag": "0"
    },
    {
      "createBy": "",
      "createTime": "2024-03-08 16:06:02",
      "updateBy": "",
      "updateTime": "2024-03-09 09:51:38",
      "remark": null,
      "reservationId": 17,
      "studentId": 100,
      "seatId": 3,
      "reservationDate": "2024-03-09",
      "timeSlot": 3,
      "isActive": "2",
      "isCancal": "0",
      "delFlag": "0"
    },
    {
      "createBy": "",
      "createTime": "2024-03-09 09:53:45",
      "updateBy": "",
      "updateTime": null,
      "remark": null,
      "reservationId": 18,
      "studentId": 100,
      "seatId": 3,
      "reservationDate": null,
      "timeSlot": null,
      "isActive": "0",
      "isCancal": "0",
      "delFlag": "0"
    },
    {
      "createBy": "",
      "createTime": "2024-03-09 09:53:57",
      "updateBy": "",
      "updateTime": null,
      "remark": null,
      "reservationId": 19,
      "studentId": 100,
      "seatId": 3,
      "reservationDate": null,
      "timeSlot": null,
      "isActive": "0",
      "isCancal": "0",
      "delFlag": "0"
    },
    {
      "createBy": "",
      "createTime": "2024-03-09 10:27:04",
      "updateBy": "",
      "updateTime": null,
      "remark": null,
      "reservationId": 20,
      "studentId": 100,
      "seatId": 6,
      "reservationDate": "2024-03-10",
      "timeSlot": 2,
      "isActive": "0",
      "isCancal": "0",
      "delFlag": "0"
    }
  ],
  "code": 200,
  "msg": "查询成功"
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

状态码 **200**

|名称|类型|必选|约束|中文名|说明|
|---|---|---|---|---|---|
|» total|integer|true|none||none|
|» rows|[object]|true|none||none|
|»» createBy|string|true|none||none|
|»» createTime|string|true|none||none|
|»» updateBy|string|true|none||none|
|»» updateTime|string¦null|true|none||none|
|»» remark|null|true|none||none|
|»» reservationId|integer|true|none||none|
|»» studentId|integer|true|none||none|
|»» seatId|integer|true|none||none|
|»» reservationDate|string¦null|true|none||none|
|»» timeSlot|integer¦null|true|none||none|
|»» isActive|string|true|none||none|
|»» isCancal|string|true|none||none|
|»» delFlag|string|true|none||none|
|» code|integer|true|none||none|
|» msg|string|true|none||none|

## POST ②新增保存座位预约

POST /system/reservation/reservation

> Body 请求参数

```json
"{\r\n    // \"createBy\": \"\",\r\n    // \"createTime\": \"2024-03-07 08:57:21\",\r\n    // \"updateBy\": \"\",\r\n    // \"updateTime\": null,\r\n    // \"delFlag\": \"\"\r\n    // \"reservationId\": 1,\r\n    // \"remark\": \"111\",\r\n    \"studentId\": \"100\",\r\n    \"seatId\": 9,\r\n    \"reservationDate\": \"2024-03-07\",\r\n    \"timeSlot\": 3 //1上午2下午3晚上\r\n    // \"isActive\": \"0\",//是否入座 0为未入座 1为已入座 2为退座\r\n    // \"isCancal\": \"0\",//是否取消预约 0为未取消 1为已取消\r\n}"
```

### 请求参数

|名称|位置|类型|必选|中文名|说明|
|---|---|---|---|---|---|
|Authorization|header|string| 否 ||none|
|body|body|object| 否 ||none|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## PUT 修改保存座位预约

PUT /system/reservation

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## DELETE 删除座位预约

DELETE /system/reservation/reservationIds

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST ①查询明日座位预约情况

POST /system/seat/seatList

### 请求参数

|名称|位置|类型|必选|中文名|说明|
|---|---|---|---|---|---|
|seatNumber|query|string| 否 ||座位编号（非必传）|
|remark|query|string| 否 ||1上午2下午3晚上|
|seatLocation|query|string| 否 ||座位位置（非必传）|
|Authorization|header|string| 是 ||none|

> 返回示例

> 成功

```json
{
  "total": 4,
  "rows": [
    {
      "createBy": "",
      "createTime": "2024-03-07 20:34:51",
      "updateBy": "",
      "updateTime": null,
      "remark": "1",
      "seatId": 3,
      "seatLocation": "自习室A区",
      "seatStatus": "2",
      "seatNumber": "01",
      "delFlag": "0"
    },
    {
      "createBy": "",
      "createTime": "2024-03-07 20:35:05",
      "updateBy": "",
      "updateTime": null,
      "remark": null,
      "seatId": 4,
      "seatLocation": "自习室B区",
      "seatStatus": "2",
      "seatNumber": "02",
      "delFlag": "0"
    },
    {
      "createBy": "",
      "createTime": "2024-03-07 20:35:22",
      "updateBy": "",
      "updateTime": null,
      "remark": null,
      "seatId": 5,
      "seatLocation": "自习室A区",
      "seatStatus": "0",
      "seatNumber": "02",
      "delFlag": "0"
    },
    {
      "createBy": "",
      "createTime": "2024-03-07 20:35:29",
      "updateBy": "",
      "updateTime": null,
      "remark": null,
      "seatId": 6,
      "seatLocation": "自习室A区",
      "seatStatus": "0",
      "seatNumber": "05",
      "delFlag": "0"
    }
  ],
  "code": 200,
  "msg": "查询成功"
}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

状态码 **200**

|名称|类型|必选|约束|中文名|说明|
|---|---|---|---|---|---|
|» total|integer|true|none||none|
|» rows|[object]|true|none||none|
|»» createBy|string|true|none||none|
|»» createTime|string|true|none||none|
|»» updateBy|string|true|none||none|
|»» updateTime|null|true|none||none|
|»» remark|string¦null|true|none||none|
|»» seatId|integer|true|none||none|
|»» seatLocation|string|true|none||none|
|»» seatStatus|string|true|none||none|
|»» seatNumber|string|true|none||none|
|»» delFlag|string|true|none||none|
|» code|integer|true|none||none|
|» msg|string|true|none||none|

## POST ③入座

POST /system/reservation/takePlace

> Body 请求参数

```json
"{\r\n    \"reservationId\":12//预约编号\r\n}"
```

### 请求参数

|名称|位置|类型|必选|中文名|说明|
|---|---|---|---|---|---|
|body|body|object| 否 ||none|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST ④退座

POST /system/reservation/cancelPlace

> Body 请求参数

```json
{}
```

### 请求参数

|名称|位置|类型|必选|中文名|说明|
|---|---|---|---|---|---|
|body|body|object| 否 ||none|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 取消预约

POST /system/reservation/cancelReservation

> Body 请求参数

```json
"{\r\n    \"reservationId\":12//预约编号\r\n}"
```

### 请求参数

|名称|位置|类型|必选|中文名|说明|
|---|---|---|---|---|---|
|body|body|object| 否 ||none|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET ③查询用户当日预约

GET /system/reservation/listTod

### 请求参数

|名称|位置|类型|必选|中文名|说明|
|---|---|---|---|---|---|
|Authorization|header|string| 否 ||none|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 返回座位

POST /system/reservation/returnSeat

> Body 请求参数

```json
"{\r\n    \"seatId\":12//座位编号\r\n}"
```

### 请求参数

|名称|位置|类型|必选|中文名|说明|
|---|---|---|---|---|---|
|body|body|object| 否 ||none|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## POST 离开座位

POST /system/reservation/leaveSeat

> Body 请求参数

```json
{}
```

### 请求参数

|名称|位置|类型|必选|中文名|说明|
|---|---|---|---|---|---|
|body|body|object| 否 ||none|

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

## GET 获取座位预约详细信息

GET /system/reservation/reservationId

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

|状态码|状态码含义|说明|数据模型|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|成功|Inline|

### 返回数据结构

# 数据模型

