import request from './request'

// 登录
export function login (data) {
  return request({
    'url': '/login',
    'method': 'post',
    headers: {
      isToken: false,
    },
    // data: data
    data: data
  })
}

// 读取用户信息
export function getInfo (data) {
  return request({
    'url': '/getInfo',
    'method': 'get',
  })
}

// 查询座位列表
export function seatList (data) {
  return request({
    'url': '/system/seat/list',
    'method': 'get',
    data: data,
  })
}
// 查询座位列表
export function seatSeatList (data) {
  return request({
    'url': '/system/seat/seatList',
    'method': 'post',
    params: data,

  })
}

// 新增座位预约
export function reservationAdd (data) {
  return request({
    'url': '/system/reservation/reservation',
    'method': 'post',
    data: data,
  })
}

// 查询用户当日预约
export function listTod (data) {
  return request({
    'url': '/system/reservation/listTod',
    'method': 'get',
    // data: data,
  })
}
// 查询用户预约
export function reservationListApi (data) {
  return request({
    'url': '/system/reservation/list',
    'method': 'get',
    // data: data,
    params: data,
  })
}


// 查询公告
export function noticeList (data) {
  return request({
    'url': '/system/notice/list',
    'method': 'get',
    // data: data,
  })
}

// 入座
export function takePlace (data) {
  return request({
    'url': '/system/reservation/takePlace',
    'method': 'post',
    data: data,
  })
}
// 返回座位
export function returnSeat (data) {
  return request({
    'url': '/system/reservation/returnSeat',
    'method': 'post',
    data: data,
  })
}

// 取消预约
export function cancelReservation (data) {
  return request({
    'url': '/system/reservation/cancelReservation',
    'method': 'post',
    data: data,
  })
}


// 新增意见
export function feedbackAdd (data) {
  return request({
    'url': '/system/feedback',
    'method': 'post',
    data: data,
  })
}

// 新增公告
export function noticeAdd (data) {
  return request({
    'url': '/system/notice/',
    'method': 'post',
    data: data,
  })
}
// 退座
export function cancelPlace (data) {
  return request({
    'url': '/system/reservation/cancelPlace',
    'method': 'post',
    data: data,
  })
}
// 离开座位
export function leaveSeat (data) {
  return request({
    'url': '/system/reservation/leaveSeat',
    'method': 'post',
    data: data,
  })
}

// 意见反馈列表
export function feedbackList (data) {
  return request({
    'url': '/system/feedback/list',
    'method': 'get',
    data: data,
  })
}

// 学生列表
export function userList (data) {
  return request({
    'url': '/system/user/list',
    'method': 'get',
    data: data,
  })
}


// 新增学生账号
export function registerStudent (data) {
  return request({
    'url': '/registerStudent',
    'method': 'post',
    data: data,
  })
}

// 修改学生密码
export function resetPwd (data) {
  return request({
    'url': '/system/user/resetPwd',
    'method': 'put',
    data: data,
  })
}

// 删除学生
export function delUserIds (userIds) {
  return request({
    'url': `/system/user/${userIds}`,
    'method': 'delete',
  })
}
// 删除座位
export function seatDel (id) {
  return request({
    'url': `/system/seat/${id}`,
    'method': 'delete',
  })
}

// 新增座位
export function seatAdd (data) {
  return request({
    'url': `/system/seat`,
    'method': 'post',
    data: data,
  })
}

// 修改座位
export function PutSeat (data) {
  return request({
    'url': `/system/seat`,
    'method': 'put',
    data: data,
  })
}

