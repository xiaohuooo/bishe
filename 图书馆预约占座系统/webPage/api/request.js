
import config from './config'
import { getToken } from './auth'
import errorCode from './errorCode'
import { toast, showConfirm, tansParams } from './common'

let timeout = 30000
const baseUrl = config.baseUrl


const request = config => {

  // 是否需要设置 token
  const isToken = (config.headers || {}).isToken === false
  config.header = config.header || {}
  if (getToken() && !isToken) {
    config.header['Authorization'] = getToken()
    // config.header['Authorization'] = getToken()
  }
  // get请求映射params参数
  if (config.params) {
    let url = config.url + '?' + tansParams(config.params)
    url = url.slice(0, -1)
    config.url = url
  }
  return new Promise((resolve, reject) => {
    uni.request({
      method: config.method || 'get',
      timeout: config.timeout || timeout,
      url: config.baseUrl || baseUrl + config.url,
      data: config.data,
      header: config.header,
      dataType: 'string'
    }).then(response => {


      let { data, errMsg } = response
      if (errMsg !== 'request:ok') {
        toast('后端接口连接异常')
        reject('后端接口连接异常')
        return
      }
      data = JSON.parse(data)



      if (data.code == 401) {
        // 提示登录过期,返回到登录页
        showConfirm('登录已过期,请重新登录').then(() => {
          uni.navigateTo({
            url: '/pages/login/index'
          })
        })
      }

      resolve(data)
    })
      .catch(error => {
        console.log(error)
        let { errMsg } = error
        if (errMsg === 'Network Error') {
          errMsg = '后端接口连接异常'
        } else if (errMsg.includes('timeout')) {
          errMsg = '系统接口请求超时'
        } else if (errMsg.includes('Request failed with status code')) {
          errMsg = '系统接口' + errMsg.substr(errMsg.length - 3) + '异常'
        } else {
          errMsg = errorCode[errMsg] || errorCode['default']
        }
        toast(errMsg)
        reject(error)
      })
  })
}

export default request
