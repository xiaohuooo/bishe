// 应用全局配置
module.exports = {
  baseUrl: "http://172.24.87.203",
  // 应用信息
  appInfo: {
    // 应用名称
    name: "USDT",
    // 应用版本
    version: "0.0.1",
    // 应用logo
    logo: "",
    // 官方网站
    site_url: "",
    // 政策协议
    agreements: [
      {
        title: "隐私政策",
        url: "",
      },
      {
        title: "用户服务协议",
        url: "",
      },
    ],
  },
};
