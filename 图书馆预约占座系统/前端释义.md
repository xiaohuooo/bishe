|   App.vue  App起步文件
|   index.html
|   main.js  vue注册main.js文件
|   manifest.json  uniapp配置文件
|   pages.json     路由配置文件
|   uni.promisify.adaptor.js
|   uni.scss   uniapp全局样式文件
|   
+---.hbuilderx
|       launch.json 
|       
+---api
|       api.js  封装接口请求
|       auth.js 封装设置cookie,取cookie,删除cookie
|       common.js 封装消息提示弹窗
|       config.js app 配置文件,同时配置后端地址
|       errorCode.js  接口不同错误返回错误码的提示信息
|       request.js   路由守卫文件,可在请求前后分别对数据进行处理
|       
+---pages
|   +---administratorEntry  管理员页面文件  权限判断,通过getinfo接口返回的user对象内的admin字段进行判断是否管理员
|   |   |   index.vue   管理员首页
|   |   |   
|   |   \---page
|   |           announ.vue  发布公告
|   |           announcementDetails.vue  查看用户反馈的详情
|   |           feedbackList.vue  查看用户意见反馈
|   |           seatManagement.vue  座位管理 新增 删除 标记故障 标记正常
|   |           studentManagement.vue  学生管理 增加学生,修改密码,删除学生
|   |           
|   +---announcement
|   |   |   index.vue 用户查看公告列表
|   |   |   
|   |   \---page
|   |           announcementDetails.vue  公告详情
|   |           
|   +---head
|   |       index.vue  统一封装的上方返回头部组件
|   |       
|   +---home
|   |       index.vue  //首页 ,负责主要功能跳转
|   |       
|   +---index
|   |       index.vue  默认页,未使用
|   |       
|   +---login
|   |       index.vue  登录页,账号密码登录
|   |       
|   +---makeAnAppointment
|   |       index.vue 预约座位,可选座位,预约时间段,
|   |       
|   +---opinionFeedback
|   |       index.vue 意见反馈提交至管理员
|   |       
|   \---reservationManagement
|           index.vue  预约管理页面,查看当日预约
|           page.vue    预约管理页面,查看自身所有预约记录
|           
\---static   静态资源目录
        back.png
        fankui.png
        gonggao.png
        gonggao2.png
        guanli.png
        library-centre.jpg
        logo.png
        muwen.jpg
        yuyue.png
        zuoyibai.png
        zuoyilan.png
        
