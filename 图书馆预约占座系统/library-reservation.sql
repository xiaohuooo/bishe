/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50730
 Source Host           : localhost:3306
 Source Schema         : library-reservation

 Target Server Type    : MySQL
 Target Server Version : 50730
 File Encoding         : 65001

 Date: 10/03/2024 17:10:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `tpl_web_type` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '前端模板类型（element-ui模版 element-plus模版）',
  `package_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (1, 'lr_feedback', '意见反馈', NULL, NULL, 'LrFeedback', 'crud', 'element-ui', 'com.ruoyi.system', 'system', 'feedback', '意见反馈', 'ruoyi', '0', '/', '{\"parentMenuId\":1}', 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:18:55', NULL);
INSERT INTO `gen_table` VALUES (2, 'lr_reservation', '座位预约', NULL, NULL, 'LrReservation', 'crud', 'element-ui', 'com.ruoyi.system', 'system', 'reservation', '座位预约', 'ruoyi', '0', '/', '{\"parentMenuId\":1}', 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:19:13', NULL);
INSERT INTO `gen_table` VALUES (3, 'lr_seat', '座位管理', NULL, NULL, 'LrSeat', 'crud', 'element-ui', 'com.ruoyi.system', 'system', 'seat', '座位管理', 'ruoyi', '0', '/', '{\"parentMenuId\":1}', 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:19:28', NULL);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` bigint(20) NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (1, 1, 'id', '主键', 'int(20)', 'Long', 'id', '1', '1', '0', '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:18:55');
INSERT INTO `gen_table_column` VALUES (2, 1, 'feedback_title', '意见反馈标题', 'varchar(255)', 'String', 'feedbackTitle', '0', '0', '0', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:18:55');
INSERT INTO `gen_table_column` VALUES (3, 1, 'feedback_content', '意见反馈内容', 'varchar(255)', 'String', 'feedbackContent', '0', '0', '0', '1', '1', '1', '1', 'EQ', 'editor', '', 3, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:18:55');
INSERT INTO `gen_table_column` VALUES (4, 1, 'feedback_location', '意见反馈位置信息', 'varchar(255)', 'String', 'feedbackLocation', '0', '0', '0', '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:18:55');
INSERT INTO `gen_table_column` VALUES (5, 1, 'feedback_handle', '意见反馈是否被处理（默认0、1为已处理）', 'char(2)', 'String', 'feedbackHandle', '0', '0', '0', '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:18:55');
INSERT INTO `gen_table_column` VALUES (6, 1, 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', '0', '1', NULL, NULL, NULL, 'EQ', 'input', '', 6, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:18:55');
INSERT INTO `gen_table_column` VALUES (7, 1, 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', '0', '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 7, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:18:55');
INSERT INTO `gen_table_column` VALUES (8, 1, 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', '0', '1', '1', NULL, NULL, 'EQ', 'input', '', 8, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:18:55');
INSERT INTO `gen_table_column` VALUES (9, 1, 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', '0', '1', '1', NULL, NULL, 'EQ', 'datetime', '', 9, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:18:55');
INSERT INTO `gen_table_column` VALUES (10, 1, 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', '0', '1', '1', '1', NULL, 'EQ', 'textarea', '', 10, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:18:55');
INSERT INTO `gen_table_column` VALUES (11, 1, 'del_flag', '删除标志（0代表存在 2代表删除）', 'char(1)', 'String', 'delFlag', '0', '0', '0', '1', NULL, NULL, NULL, 'EQ', 'input', '', 11, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:18:55');
INSERT INTO `gen_table_column` VALUES (12, 2, 'reservation_id', '预订id', 'int(20)', 'Long', 'reservationId', '1', '1', '0', '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:19:13');
INSERT INTO `gen_table_column` VALUES (13, 2, 'student_id', '用户id', 'int(20)', 'Long', 'studentId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:19:13');
INSERT INTO `gen_table_column` VALUES (14, 2, 'seat_id', '座位id', 'int(20)', 'Long', 'seatId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:19:13');
INSERT INTO `gen_table_column` VALUES (15, 2, 'reservation_date', '预定日期', 'datetime', 'Date', 'reservationDate', '0', '0', '0', '1', '1', '1', '1', 'EQ', 'datetime', '', 4, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:19:13');
INSERT INTO `gen_table_column` VALUES (16, 2, 'time_slot', '1上午2下午3晚上', 'int(2)', 'Integer', 'timeSlot', '0', '0', '0', '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:19:13');
INSERT INTO `gen_table_column` VALUES (17, 2, 'is_active', '是否入座 0为未入座 1为已入座', 'char(2)', 'String', 'isActive', '0', '0', '0', '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:19:13');
INSERT INTO `gen_table_column` VALUES (18, 2, 'is_cancal', '是否取消预约 0为未取消 1为已取消', 'char(2)', 'String', 'isCancal', '0', '0', '0', '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:19:13');
INSERT INTO `gen_table_column` VALUES (19, 2, 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', '0', '1', NULL, NULL, NULL, 'EQ', 'input', '', 8, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:19:13');
INSERT INTO `gen_table_column` VALUES (20, 2, 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', '0', '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 9, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:19:13');
INSERT INTO `gen_table_column` VALUES (21, 2, 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', '0', '1', '1', NULL, NULL, 'EQ', 'input', '', 10, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:19:13');
INSERT INTO `gen_table_column` VALUES (22, 2, 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', '0', '1', '1', NULL, NULL, 'EQ', 'datetime', '', 11, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:19:13');
INSERT INTO `gen_table_column` VALUES (23, 2, 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', '0', '1', '1', '1', NULL, 'EQ', 'textarea', '', 12, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:19:13');
INSERT INTO `gen_table_column` VALUES (24, 2, 'del_flag', '删除标志（0代表存在 2代表删除）', 'char(1)', 'String', 'delFlag', '0', '0', '0', '1', NULL, NULL, NULL, 'EQ', 'input', '', 13, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:19:13');
INSERT INTO `gen_table_column` VALUES (25, 3, 'seat_id', '主键', 'int(20)', 'Long', 'seatId', '1', '1', '0', '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:19:28');
INSERT INTO `gen_table_column` VALUES (26, 3, 'seat_location', '座位信息', 'varchar(255)', 'String', 'seatLocation', '0', '0', '0', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:19:28');
INSERT INTO `gen_table_column` VALUES (27, 3, 'seat_status', '0为可用，1为故障不可用', 'char(2)', 'String', 'seatStatus', '0', '0', '0', '1', '1', '1', '1', 'EQ', 'radio', '', 3, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:19:28');
INSERT INTO `gen_table_column` VALUES (28, 3, 'seat_number', '座位编号', 'varchar(255)', 'String', 'seatNumber', '0', '0', '0', '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:19:28');
INSERT INTO `gen_table_column` VALUES (29, 3, 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', '0', '1', NULL, NULL, NULL, 'EQ', 'input', '', 5, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:19:28');
INSERT INTO `gen_table_column` VALUES (30, 3, 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', '0', '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 6, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:19:28');
INSERT INTO `gen_table_column` VALUES (31, 3, 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', '0', '1', '1', NULL, NULL, 'EQ', 'input', '', 7, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:19:28');
INSERT INTO `gen_table_column` VALUES (32, 3, 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', '0', '1', '1', NULL, NULL, 'EQ', 'datetime', '', 8, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:19:28');
INSERT INTO `gen_table_column` VALUES (33, 3, 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', '0', '1', '1', '1', NULL, 'EQ', 'textarea', '', 9, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:19:28');
INSERT INTO `gen_table_column` VALUES (34, 3, 'del_flag', '删除标志（0代表存在 2代表删除）', 'char(1)', 'String', 'delFlag', '0', '0', '0', '1', NULL, NULL, NULL, 'EQ', 'input', '', 10, 'admin', '2024-03-07 20:17:54', '', '2024-03-07 20:19:28');

-- ----------------------------
-- Table structure for lr_feedback
-- ----------------------------
DROP TABLE IF EXISTS `lr_feedback`;
CREATE TABLE `lr_feedback`  (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `feedback_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '意见反馈标题',
  `feedback_content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '意见反馈内容',
  `feedback_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '意见反馈位置信息',
  `feedback_handle` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '意见反馈是否被处理（默认0、1为已处理）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of lr_feedback
-- ----------------------------
INSERT INTO `lr_feedback` VALUES (4, NULL, '666666', NULL, '0', '', '2024-03-08 15:53:42', '', NULL, NULL, '0');
INSERT INTO `lr_feedback` VALUES (5, NULL, '8888888888888', NULL, '0', '', '2024-03-08 15:54:36', '', NULL, NULL, '0');
INSERT INTO `lr_feedback` VALUES (6, NULL, '8555555555555555', NULL, '0', '', '2024-03-08 15:54:54', '', NULL, NULL, '0');
INSERT INTO `lr_feedback` VALUES (7, NULL, '尊敬的管理员，我希望在图书馆增加一些关于编程和技术方面的书籍，以便满足对这方面知识的需求。感谢您的考虑！', NULL, '0', '', '2024-03-08 17:14:11', '', NULL, NULL, '0');
INSERT INTO `lr_feedback` VALUES (8, NULL, '亲爱的管理员，我希望图书馆能够增加一些关于创业和商业管理的书籍，这样我可以更好地学习和了解相关知识。谢谢您！', NULL, '0', '', '2024-03-08 17:14:19', '', NULL, NULL, '0');
INSERT INTO `lr_feedback` VALUES (9, NULL, '亲爱的管理员，我希望图书馆能够增加更多的座位和学习空间，因为在繁忙的时间段，很难找到一个安静的地方专心学习。希望您能考虑我的建议，感谢您的努力！', NULL, '0', '', '2024-03-08 17:14:27', '', NULL, NULL, '0');
INSERT INTO `lr_feedback` VALUES (10, NULL, '亲爱的管理员，我希望图书馆能够增加更多的座位和学习空间，因为在繁忙的时间段，很难找到一个安静的地方专心学习。希望您能考虑我的建议，感谢您的努力！', NULL, '0', '', '2024-03-08 17:14:34', '', NULL, NULL, '0');

-- ----------------------------
-- Table structure for lr_reservation
-- ----------------------------
DROP TABLE IF EXISTS `lr_reservation`;
CREATE TABLE `lr_reservation`  (
  `reservation_id` int(20) NOT NULL AUTO_INCREMENT COMMENT '预订id',
  `student_id` int(20) NOT NULL COMMENT '用户id',
  `seat_id` int(20) NOT NULL COMMENT '座位id',
  `reservation_date` datetime NULL DEFAULT NULL COMMENT '预定日期',
  `time_slot` int(2) NULL DEFAULT NULL COMMENT '1上午2下午3晚上',
  `is_active` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '是否入座 0为未入座 1为已入座 2为退座',
  `is_cancal` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '是否取消预约 0为未取消 1为已取消',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  PRIMARY KEY (`reservation_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of lr_reservation
-- ----------------------------
INSERT INTO `lr_reservation` VALUES (9, 100, 6, '2024-03-08 00:00:00', 2, '1', '0', '', '2024-03-07 22:06:38', '', '2024-03-09 16:22:51', NULL, '0');
INSERT INTO `lr_reservation` VALUES (10, 1, 9, '2024-03-07 00:00:00', 3, '0', '0', '', '2024-03-07 22:20:31', '', NULL, NULL, '0');
INSERT INTO `lr_reservation` VALUES (11, 100, 3, '2024-03-09 00:00:00', 1, '1', '0', '', '2024-03-08 12:54:17', '', '2024-03-09 08:51:25', NULL, '0');
INSERT INTO `lr_reservation` VALUES (13, 100, 5, '2024-03-09 00:00:00', 2, '0', '1', '', '2024-03-08 13:00:52', '', '2024-03-09 09:01:47', NULL, '0');
INSERT INTO `lr_reservation` VALUES (14, 100, 4, '2024-03-09 00:00:00', 3, '0', '1', '', '2024-03-08 13:05:15', '', '2024-03-09 09:05:42', NULL, '0');
INSERT INTO `lr_reservation` VALUES (15, 100, 4, '2024-03-09 00:00:00', 2, '1', '0', '', '2024-03-08 13:05:23', '', '2024-03-09 09:10:16', NULL, '0');
INSERT INTO `lr_reservation` VALUES (16, 100, 4, '2024-03-09 00:00:00', 1, '1', '0', '', '2024-03-08 13:05:27', '', '2024-03-09 09:10:43', NULL, '0');
INSERT INTO `lr_reservation` VALUES (17, 100, 3, '2024-03-09 00:00:00', 3, '2', '1', '', '2024-03-08 16:06:02', '', '2024-03-09 16:34:34', NULL, '0');
INSERT INTO `lr_reservation` VALUES (18, 100, 3, NULL, NULL, '0', '1', '', '2024-03-09 09:53:45', '', '2024-03-09 16:30:09', NULL, '0');
INSERT INTO `lr_reservation` VALUES (19, 100, 3, NULL, NULL, '0', '1', '', '2024-03-09 09:53:57', '', '2024-03-09 16:30:28', NULL, '0');
INSERT INTO `lr_reservation` VALUES (20, 100, 6, '2024-03-10 00:00:00', 2, '0', '1', '', '2024-03-09 10:27:04', '', '2024-03-09 16:34:37', NULL, '0');
INSERT INTO `lr_reservation` VALUES (21, 100, 3, '2024-03-10 00:00:00', 1, '0', '0', '', '2024-03-09 10:27:13', '', NULL, NULL, '0');
INSERT INTO `lr_reservation` VALUES (22, 100, 5, '2024-03-10 00:00:00', 1, '0', '0', '', '2024-03-09 10:27:24', '', NULL, NULL, '0');
INSERT INTO `lr_reservation` VALUES (23, 100, 3, '2024-03-10 00:00:00', 3, '0', '0', '', '2024-03-09 10:27:30', '', NULL, NULL, '0');
INSERT INTO `lr_reservation` VALUES (24, 100, 6, '2024-03-10 00:00:00', 1, '0', '0', '', '2024-03-09 13:45:26', '', NULL, NULL, '0');
INSERT INTO `lr_reservation` VALUES (25, 100, 7, '2024-03-10 00:00:00', 1, '0', '0', '', '2024-03-09 13:46:38', '', NULL, NULL, '0');
INSERT INTO `lr_reservation` VALUES (26, 100, 8, '2024-03-10 00:00:00', 1, '0', '0', '', '2024-03-09 16:30:42', '', NULL, NULL, '0');
INSERT INTO `lr_reservation` VALUES (27, 100, 9, '2024-03-10 00:00:00', 1, '0', '0', '', '2024-03-09 16:30:46', '', NULL, NULL, '0');
INSERT INTO `lr_reservation` VALUES (28, 100, 10, '2024-03-10 00:00:00', 1, '0', '0', '', '2024-03-09 16:34:47', '', NULL, NULL, '0');
INSERT INTO `lr_reservation` VALUES (29, 100, 11, '2024-03-10 00:00:00', 2, '0', '0', '', '2024-03-09 16:35:17', '', NULL, NULL, '0');
INSERT INTO `lr_reservation` VALUES (30, 100, 12, '2024-03-10 00:00:00', 2, '0', '0', '', '2024-03-09 16:42:50', '', NULL, NULL, '0');
INSERT INTO `lr_reservation` VALUES (31, 108, 11, '2024-03-10 00:00:00', 1, '0', '1', '', '2024-03-09 17:07:40', '', '2024-03-09 17:07:50', NULL, '0');
INSERT INTO `lr_reservation` VALUES (32, 108, 12, '2024-03-10 00:00:00', 3, '2', '0', '', '2024-03-09 17:08:33', '', '2024-03-10 15:50:41', NULL, '0');
INSERT INTO `lr_reservation` VALUES (33, 108, 13, '2024-03-10 00:00:00', 2, '2', '0', '', '2024-03-09 17:12:04', '', '2024-03-10 15:51:08', NULL, '0');
INSERT INTO `lr_reservation` VALUES (34, 108, 13, '2024-03-10 00:00:00', 1, '1', '0', '', '2024-03-09 17:24:18', '', '2024-03-10 15:10:37', NULL, '0');
INSERT INTO `lr_reservation` VALUES (35, 108, 3, '2024-03-11 00:00:00', 1, '0', '1', '', '2024-03-10 15:59:29', '', '2024-03-10 16:01:44', NULL, '0');
INSERT INTO `lr_reservation` VALUES (36, 108, 5, '2024-03-11 00:00:00', 1, '0', '0', '', '2024-03-10 15:59:31', '', NULL, NULL, '0');

-- ----------------------------
-- Table structure for lr_seat
-- ----------------------------
DROP TABLE IF EXISTS `lr_seat`;
CREATE TABLE `lr_seat`  (
  `seat_id` int(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `seat_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '座位信息',
  `seat_status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '0为可用，1为故障不可用',
  `seat_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '座位编号',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  PRIMARY KEY (`seat_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 46 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of lr_seat
-- ----------------------------
INSERT INTO `lr_seat` VALUES (3, '自习室A区', '0', '01', '', '2024-03-07 20:34:51', '', '2024-03-10 15:23:25', '121', '0');
INSERT INTO `lr_seat` VALUES (4, '自习室B区', '0', '02', '', '2024-03-07 20:35:05', '', NULL, NULL, '0');
INSERT INTO `lr_seat` VALUES (5, '自习室A区', '0', '02', '', '2024-03-07 20:35:22', '', '2024-03-10 15:23:27', NULL, '0');
INSERT INTO `lr_seat` VALUES (6, '自习室A区', '1', '05', '', '2024-03-07 20:35:29', '', '2024-03-10 16:03:27', NULL, '0');
INSERT INTO `lr_seat` VALUES (7, '自习室A区', '0', '03', '', '2024-03-07 20:35:22', '', NULL, NULL, '0');
INSERT INTO `lr_seat` VALUES (8, '自习室A区', '0', '04', '', '2024-03-07 20:35:29', '', NULL, NULL, '0');
INSERT INTO `lr_seat` VALUES (9, '自习室A区', '0', '06', '', '2024-03-07 20:35:22', '', NULL, NULL, '0');
INSERT INTO `lr_seat` VALUES (10, '自习室A区', '0', '07', '', '2024-03-07 20:35:29', '', NULL, NULL, '0');
INSERT INTO `lr_seat` VALUES (11, '自习室A区', '0', '08', '', '2024-03-07 20:35:22', '', NULL, NULL, '0');
INSERT INTO `lr_seat` VALUES (12, '自习室A区', '0', '09', '', '2024-03-07 20:35:29', '', NULL, NULL, '0');
INSERT INTO `lr_seat` VALUES (13, '自习室A区', '0', '10', '', '2024-03-07 20:35:22', '', NULL, NULL, '0');
INSERT INTO `lr_seat` VALUES (14, '自习室A区', '0', '11', '', '2024-03-07 20:35:29', '', NULL, NULL, '0');
INSERT INTO `lr_seat` VALUES (15, '自习室A区', '0', '12', '', '2024-03-07 20:35:22', '', NULL, NULL, '0');
INSERT INTO `lr_seat` VALUES (16, '自习室A区', '0', '13', '', '2024-03-07 20:35:29', '', NULL, NULL, '2');
INSERT INTO `lr_seat` VALUES (17, NULL, '0', '05', '', '2024-03-07 21:20:35', '', NULL, NULL, '0');
INSERT INTO `lr_seat` VALUES (18, NULL, '0', '05', '', '2024-03-07 21:20:59', '', NULL, NULL, '0');
INSERT INTO `lr_seat` VALUES (19, NULL, '0', '05', '', '2024-03-07 21:26:26', '', NULL, NULL, '0');
INSERT INTO `lr_seat` VALUES (20, NULL, '0', '01', '', '2024-03-07 21:27:05', '', NULL, '1', '0');
INSERT INTO `lr_seat` VALUES (21, NULL, '0', '05', '', '2024-03-07 21:28:05', '', NULL, NULL, '0');
INSERT INTO `lr_seat` VALUES (22, NULL, '0', '02', '', '2024-03-07 21:28:17', '', NULL, NULL, '0');
INSERT INTO `lr_seat` VALUES (23, NULL, '0', '02', '', '2024-03-07 21:35:20', '', NULL, NULL, '0');
INSERT INTO `lr_seat` VALUES (24, NULL, '0', '01', '', '2024-03-07 21:35:26', '', NULL, '1', '0');
INSERT INTO `lr_seat` VALUES (25, NULL, '0', '01', '', '2024-03-07 21:37:32', '', NULL, '1', '0');
INSERT INTO `lr_seat` VALUES (26, NULL, '0', '01', '', '2024-03-07 21:41:31', '', NULL, '1', '0');
INSERT INTO `lr_seat` VALUES (27, NULL, '0', '01', '', '2024-03-07 21:42:07', '', NULL, '1', '0');
INSERT INTO `lr_seat` VALUES (28, NULL, '0', '01', '', '2024-03-07 21:43:00', '', NULL, '1', '0');
INSERT INTO `lr_seat` VALUES (29, NULL, '0', '01', '', '2024-03-07 21:43:36', '', NULL, '2', '0');
INSERT INTO `lr_seat` VALUES (30, NULL, '0', '01', '', '2024-03-07 21:45:15', '', NULL, NULL, '0');
INSERT INTO `lr_seat` VALUES (31, NULL, '0', '02', '', '2024-03-07 22:01:35', '', NULL, NULL, '0');
INSERT INTO `lr_seat` VALUES (32, NULL, '0', '02', '', '2024-03-07 22:02:31', '', NULL, NULL, '0');
INSERT INTO `lr_seat` VALUES (33, NULL, '0', '02', '', '2024-03-07 22:02:39', '', NULL, NULL, '0');
INSERT INTO `lr_seat` VALUES (34, NULL, '0', NULL, '', '2024-03-07 22:03:57', '', NULL, NULL, '0');
INSERT INTO `lr_seat` VALUES (35, '自习室A区', '0', '123456', '', '2024-03-09 11:26:17', '', NULL, NULL, '2');
INSERT INTO `lr_seat` VALUES (36, '自习室A区', '0', '13', '', '2024-03-10 15:00:54', '', NULL, NULL, '0');
INSERT INTO `lr_seat` VALUES (37, '自习室A区', '0', '15', '', '2024-03-10 15:01:29', '', NULL, NULL, '0');
INSERT INTO `lr_seat` VALUES (38, '自习室A区', '0', '16', '', '2024-03-10 15:01:48', '', NULL, NULL, '0');
INSERT INTO `lr_seat` VALUES (39, '自习室B区', '0', '03', '', '2024-03-10 15:01:55', '', '2024-03-10 15:23:35', NULL, '0');
INSERT INTO `lr_seat` VALUES (40, '自习室B区', '0', '04', '', '2024-03-10 15:01:59', '', NULL, NULL, '0');
INSERT INTO `lr_seat` VALUES (41, '自习室B区', '0', '05', '', '2024-03-10 15:02:01', '', NULL, NULL, '0');
INSERT INTO `lr_seat` VALUES (42, '自习室B区', '0', '06', '', '2024-03-10 15:02:04', '', NULL, NULL, '0');
INSERT INTO `lr_seat` VALUES (43, '自习室B区', '0', '09', '', '2024-03-10 15:02:10', '', NULL, NULL, '0');
INSERT INTO `lr_seat` VALUES (44, '自习室C区', '0', 'test', '', '2024-03-10 15:02:17', '', NULL, NULL, '0');
INSERT INTO `lr_seat` VALUES (45, '自习室C区', '0', 'test1', '', '2024-03-10 15:02:22', '', NULL, NULL, '0');

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `blob_data` blob NULL COMMENT '存放持久化Trigger对象',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'Blob类型的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `calendar_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '日历名称',
  `calendar` blob NOT NULL COMMENT '存放持久化calendar对象',
  PRIMARY KEY (`sched_name`, `calendar_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '日历信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `cron_expression` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'cron表达式',
  `time_zone_id` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '时区',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'Cron类型的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `entry_id` varchar(95) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度器实例id',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度器实例名',
  `fired_time` bigint(13) NOT NULL COMMENT '触发的时间',
  `sched_time` bigint(13) NOT NULL COMMENT '定时器制定的时间',
  `priority` int(11) NOT NULL COMMENT '优先级',
  `state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '状态',
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '任务名称',
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '任务组名',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否并发',
  `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否接受恢复执行',
  PRIMARY KEY (`sched_name`, `entry_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '已触发的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务组名',
  `description` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '相关介绍',
  `job_class_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '执行任务类名称',
  `is_durable` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否持久化',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否并发',
  `is_update_data` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否更新数据',
  `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否接受恢复执行',
  `job_data` blob NULL COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '任务详细信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `lock_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '悲观锁名称',
  PRIMARY KEY (`sched_name`, `lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '存储的悲观锁信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  PRIMARY KEY (`sched_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '暂停的触发器表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '实例名称',
  `last_checkin_time` bigint(13) NOT NULL COMMENT '上次检查时间',
  `checkin_interval` bigint(13) NOT NULL COMMENT '检查间隔时间',
  PRIMARY KEY (`sched_name`, `instance_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '调度器状态表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `repeat_count` bigint(7) NOT NULL COMMENT '重复的次数统计',
  `repeat_interval` bigint(12) NOT NULL COMMENT '重复的间隔时间',
  `times_triggered` bigint(10) NOT NULL COMMENT '已经触发的次数',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '简单触发器的信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `str_prop_1` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第一个参数',
  `str_prop_2` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第二个参数',
  `str_prop_3` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第三个参数',
  `int_prop_1` int(11) NULL DEFAULT NULL COMMENT 'int类型的trigger的第一个参数',
  `int_prop_2` int(11) NULL DEFAULT NULL COMMENT 'int类型的trigger的第二个参数',
  `long_prop_1` bigint(20) NULL DEFAULT NULL COMMENT 'long类型的trigger的第一个参数',
  `long_prop_2` bigint(20) NULL DEFAULT NULL COMMENT 'long类型的trigger的第二个参数',
  `dec_prop_1` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第一个参数',
  `dec_prop_2` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第二个参数',
  `bool_prop_1` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第一个参数',
  `bool_prop_2` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第二个参数',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '同步机制的行锁表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '触发器的名字',
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '触发器所属组的名字',
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_job_details表job_name的外键',
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'qrtz_job_details表job_group的外键',
  `description` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '相关介绍',
  `next_fire_time` bigint(13) NULL DEFAULT NULL COMMENT '上一次触发时间（毫秒）',
  `prev_fire_time` bigint(13) NULL DEFAULT NULL COMMENT '下一次触发时间（默认为-1表示不触发）',
  `priority` int(11) NULL DEFAULT NULL COMMENT '优先级',
  `trigger_state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '触发器状态',
  `trigger_type` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '触发器的类型',
  `start_time` bigint(13) NOT NULL COMMENT '开始时间',
  `end_time` bigint(13) NULL DEFAULT NULL COMMENT '结束时间',
  `calendar_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '日程表名称',
  `misfire_instr` smallint(2) NULL DEFAULT NULL COMMENT '补偿执行的策略',
  `job_data` blob NULL COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  INDEX `sched_name`(`sched_name`, `job_name`, `job_group`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '触发器详细信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2024-03-07 20:14:37', '', NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2024-03-07 20:14:37', '', NULL, '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2024-03-07 20:14:37', '', NULL, '深色主题theme-dark，浅色主题theme-light');
INSERT INTO `sys_config` VALUES (4, '账号自助-验证码开关', 'sys.account.captchaEnabled', 'false', 'Y', 'admin', '2024-03-07 20:14:37', '', NULL, '是否开启验证码功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES (5, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'true', 'Y', 'admin', '2024-03-07 20:14:37', '', NULL, '是否开启注册用户功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES (6, '用户登录-黑名单列表', 'sys.login.blackIPList', '', 'Y', 'admin', '2024-03-07 20:14:37', '', NULL, '设置登录IP黑名单限制，多个匹配项以;分隔，支持匹配（*通配、网段）');
INSERT INTO `sys_config` VALUES (12, '学生管理-学生初始化信誉分功能', 'sys.account.initScore', '10', 'Y', 'admin', '2024-03-05 18:58:05', '', NULL, '学生初始化信誉分为多少');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 200 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '若依科技', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-03-07 20:14:36', '', NULL);
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '深圳总公司', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-03-07 20:14:36', '', NULL);
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '长沙分公司', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-03-07 20:14:36', '', NULL);
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-03-07 20:14:36', '', NULL);
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-03-07 20:14:36', '', NULL);
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-03-07 20:14:36', '', NULL);
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-03-07 20:14:36', '', NULL);
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-03-07 20:14:36', '', NULL);
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-03-07 20:14:36', '', NULL);
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2024-03-07 20:14:36', '', NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 99, '其他', '0', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '其他操作');
INSERT INTO `sys_dict_data` VALUES (19, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (20, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (21, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (22, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (23, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (24, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (25, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (26, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (27, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (28, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (29, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '停用状态');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '登录状态列表');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0 0 0 * * ? ', '3', '1', '0', 'admin', '2024-03-07 20:14:38', 'admin', '2024-03-10 16:13:51', '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0 0 0,6,12,18 * * ? ', '3', '1', '0', 'admin', '2024-03-07 20:14:38', 'admin', '2024-03-10 16:14:03', '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2024-03-07 20:14:38', '', NULL, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------
INSERT INTO `sys_job_log` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '系统默认（无参） 总共耗时：13毫秒', '0', '', '2024-03-10 16:13:40');

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE,
  INDEX `idx_sys_logininfor_s`(`status`) USING BTREE,
  INDEX `idx_sys_logininfor_lt`(`login_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 178 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES (100, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-07 20:16:11');
INSERT INTO `sys_logininfor` VALUES (101, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '0', '退出成功', '2024-03-07 20:16:45');
INSERT INTO `sys_logininfor` VALUES (102, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-07 20:16:48');
INSERT INTO `sys_logininfor` VALUES (103, '2024030705', '172.24.87.235', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '1', '用户不存在/密码错误', '2024-03-07 20:27:53');
INSERT INTO `sys_logininfor` VALUES (104, 'admin', '172.24.87.235', '内网IP', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-07 20:28:59');
INSERT INTO `sys_logininfor` VALUES (105, '2024030705', '172.24.87.235', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2024-03-07 20:29:35');
INSERT INTO `sys_logininfor` VALUES (106, '2024030705', '172.24.87.235', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2024-03-07 20:31:07');
INSERT INTO `sys_logininfor` VALUES (107, '2024030705', '172.24.87.235', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2024-03-07 20:33:05');
INSERT INTO `sys_logininfor` VALUES (108, '2024030705', '172.24.87.235', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2024-03-07 21:13:10');
INSERT INTO `sys_logininfor` VALUES (109, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-07 21:19:35');
INSERT INTO `sys_logininfor` VALUES (110, 'admin', '172.24.87.235', '内网IP', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-07 21:21:35');
INSERT INTO `sys_logininfor` VALUES (111, 'admin', '172.24.87.235', '内网IP', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-07 21:57:17');
INSERT INTO `sys_logininfor` VALUES (112, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '0', '退出成功', '2024-03-07 22:49:40');
INSERT INTO `sys_logininfor` VALUES (113, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-07 22:49:43');
INSERT INTO `sys_logininfor` VALUES (114, 'admin1', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '0', '注册成功', '2024-03-07 22:50:02');
INSERT INTO `sys_logininfor` VALUES (115, 'admin6', '154.64.226.174', 'XX XX', 'Unknown', 'Unknown', '0', '注册成功', '2024-03-07 23:01:16');
INSERT INTO `sys_logininfor` VALUES (116, 'admin4', '154.64.226.174', 'XX XX', 'Unknown', 'Unknown', '0', '注册成功', '2024-03-07 23:03:56');
INSERT INTO `sys_logininfor` VALUES (117, '2024030705', '172.24.87.101', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '1', '验证码已失效', '2024-03-08 11:49:03');
INSERT INTO `sys_logininfor` VALUES (118, '2024030705', '172.24.87.101', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '1', '验证码已失效', '2024-03-08 11:49:05');
INSERT INTO `sys_logininfor` VALUES (119, '2024030705', '172.24.87.101', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2024-03-08 11:50:30');
INSERT INTO `sys_logininfor` VALUES (120, '2024030705', '172.24.87.101', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2024-03-08 12:41:49');
INSERT INTO `sys_logininfor` VALUES (121, '2024030705', '172.24.87.101', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2024-03-08 12:43:05');
INSERT INTO `sys_logininfor` VALUES (122, '2024030705', '172.24.87.101', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2024-03-08 14:43:56');
INSERT INTO `sys_logininfor` VALUES (123, '2024030705', '172.24.87.101', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2024-03-08 14:49:06');
INSERT INTO `sys_logininfor` VALUES (124, '2024030705', '172.24.87.101', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2024-03-08 15:22:54');
INSERT INTO `sys_logininfor` VALUES (125, '2024030705', '172.24.87.101', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2024-03-08 16:27:16');
INSERT INTO `sys_logininfor` VALUES (126, '2024030705', '172.24.87.101', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2024-03-08 16:32:35');
INSERT INTO `sys_logininfor` VALUES (127, '2024030705', '172.24.87.101', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2024-03-08 16:32:36');
INSERT INTO `sys_logininfor` VALUES (128, 'admin', '172.24.87.101', '内网IP', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-08 16:46:25');
INSERT INTO `sys_logininfor` VALUES (129, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-08 17:26:46');
INSERT INTO `sys_logininfor` VALUES (130, '2024030705', '172.24.87.101', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2024-03-08 17:50:43');
INSERT INTO `sys_logininfor` VALUES (131, '2024030705', '172.24.87.101', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2024-03-09 08:35:05');
INSERT INTO `sys_logininfor` VALUES (132, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-09 09:25:10');
INSERT INTO `sys_logininfor` VALUES (133, '2024030705', '172.24.87.101', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2024-03-09 09:39:44');
INSERT INTO `sys_logininfor` VALUES (134, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '0', '退出成功', '2024-03-09 09:47:38');
INSERT INTO `sys_logininfor` VALUES (135, '2024030705', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '1', '密码输入错误1次', '2024-03-09 09:47:45');
INSERT INTO `sys_logininfor` VALUES (136, '2024030705', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '1', '用户不存在/密码错误', '2024-03-09 09:47:45');
INSERT INTO `sys_logininfor` VALUES (137, 'admin4', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '1', '密码输入错误1次', '2024-03-09 09:47:55');
INSERT INTO `sys_logininfor` VALUES (138, 'admin4', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '1', '用户不存在/密码错误', '2024-03-09 09:47:55');
INSERT INTO `sys_logininfor` VALUES (139, 'admin4', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '1', '密码输入错误2次', '2024-03-09 09:48:00');
INSERT INTO `sys_logininfor` VALUES (140, 'admin4', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '1', '用户不存在/密码错误', '2024-03-09 09:48:00');
INSERT INTO `sys_logininfor` VALUES (141, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '1', '用户不存在/密码错误', '2024-03-09 09:48:09');
INSERT INTO `sys_logininfor` VALUES (142, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '1', '密码输入错误1次', '2024-03-09 09:48:09');
INSERT INTO `sys_logininfor` VALUES (143, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '1', '用户不存在/密码错误', '2024-03-09 09:48:11');
INSERT INTO `sys_logininfor` VALUES (144, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '1', '密码输入错误2次', '2024-03-09 09:48:11');
INSERT INTO `sys_logininfor` VALUES (145, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-09 09:48:15');
INSERT INTO `sys_logininfor` VALUES (146, '2024030705', '172.24.87.101', '内网IP', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-09 09:50:16');
INSERT INTO `sys_logininfor` VALUES (147, '2024030705', '172.24.87.101', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2024-03-09 09:52:04');
INSERT INTO `sys_logininfor` VALUES (148, 'admin4', '172.24.87.101', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '1', '密码输入错误1次', '2024-03-09 10:28:13');
INSERT INTO `sys_logininfor` VALUES (149, 'admin4', '172.24.87.101', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '1', '用户不存在/密码错误', '2024-03-09 10:28:13');
INSERT INTO `sys_logininfor` VALUES (150, 'admin4', '172.24.87.101', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '1', '密码输入错误2次', '2024-03-09 10:28:33');
INSERT INTO `sys_logininfor` VALUES (151, 'admin4', '172.24.87.101', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '1', '用户不存在/密码错误', '2024-03-09 10:28:33');
INSERT INTO `sys_logininfor` VALUES (152, '2024030705', '172.24.87.101', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2024-03-09 10:28:40');
INSERT INTO `sys_logininfor` VALUES (153, '123456', '172.24.87.101', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '注册成功', '2024-03-09 10:35:44');
INSERT INTO `sys_logininfor` VALUES (154, 'admin', '172.24.87.101', '内网IP', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-09 10:36:20');
INSERT INTO `sys_logininfor` VALUES (155, '666666', '172.24.87.101', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '注册成功', '2024-03-09 10:37:04');
INSERT INTO `sys_logininfor` VALUES (156, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-09 10:41:39');
INSERT INTO `sys_logininfor` VALUES (157, 'test', '172.24.87.101', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '注册成功', '2024-03-09 10:43:53');
INSERT INTO `sys_logininfor` VALUES (158, '2024030705', '172.24.87.199', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2024-03-09 13:17:42');
INSERT INTO `sys_logininfor` VALUES (159, '2024030705', '172.24.87.101', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2024-03-09 13:24:35');
INSERT INTO `sys_logininfor` VALUES (160, '2024030705', '172.24.87.199', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2024-03-09 13:44:46');
INSERT INTO `sys_logininfor` VALUES (161, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-09 14:02:56');
INSERT INTO `sys_logininfor` VALUES (162, '2024030705', '172.24.87.199', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2024-03-09 14:58:42');
INSERT INTO `sys_logininfor` VALUES (163, '2024030705', '172.24.87.101', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2024-03-09 16:10:59');
INSERT INTO `sys_logininfor` VALUES (164, 'admin', '172.24.87.101', '内网IP', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-09 16:57:02');
INSERT INTO `sys_logininfor` VALUES (165, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-09 17:05:07');
INSERT INTO `sys_logininfor` VALUES (166, '666', '172.24.87.101', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '注册成功', '2024-03-09 17:06:10');
INSERT INTO `sys_logininfor` VALUES (167, '666666', '172.24.87.101', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '注册成功', '2024-03-09 17:06:35');
INSERT INTO `sys_logininfor` VALUES (168, '666666', '172.24.87.101', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2024-03-09 17:07:10');
INSERT INTO `sys_logininfor` VALUES (169, '2024030705', '172.24.87.235', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2024-03-10 14:41:46');
INSERT INTO `sys_logininfor` VALUES (170, '666666', '172.24.87.235', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2024-03-10 14:57:24');
INSERT INTO `sys_logininfor` VALUES (171, '666666', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-10 15:32:58');
INSERT INTO `sys_logininfor` VALUES (172, '2024030705', '172.24.87.235', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2024-03-10 15:38:20');
INSERT INTO `sys_logininfor` VALUES (173, '666666', '172.24.87.235', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2024-03-10 15:38:32');
INSERT INTO `sys_logininfor` VALUES (174, '666666', '172.24.87.235', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2024-03-10 16:09:10');
INSERT INTO `sys_logininfor` VALUES (175, 'admin', '172.24.87.235', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2024-03-10 16:09:26');
INSERT INTO `sys_logininfor` VALUES (176, 'admin', '172.24.87.235', '内网IP', 'Mobile Safari', 'Mac OS X (iPhone)', '0', '登录成功', '2024-03-10 16:10:13');
INSERT INTO `sys_logininfor` VALUES (177, 'admin', '127.0.0.1', '内网IP', 'Chrome 12', 'Windows 10', '0', '登录成功', '2024-03-10 16:12:12');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '组件路径',
  `query` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路由参数',
  `is_frame` int(1) NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `is_cache` int(1) NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2018 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 1, 'system', NULL, '', 1, 0, 'M', '0', '0', '', 'system', 'admin', '2024-03-07 20:14:37', '', NULL, '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 2, 'monitor', NULL, '', 1, 0, 'M', '0', '0', '', 'monitor', 'admin', '2024-03-07 20:14:37', '', NULL, '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 3, 'tool', NULL, '', 1, 0, 'M', '0', '0', '', 'tool', 'admin', '2024-03-07 20:14:37', '', NULL, '系统工具目录');
INSERT INTO `sys_menu` VALUES (4, '若依官网', 0, 4, 'http://ruoyi.vip', NULL, '', 0, 0, 'M', '0', '0', '', 'guide', 'admin', '2024-03-07 20:14:37', '', NULL, '若依官网地址');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', '', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2024-03-07 20:14:37', '', NULL, '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', '', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2024-03-07 20:14:37', '', NULL, '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', '', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2024-03-07 20:14:37', '', NULL, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', '', 1, 0, 'C', '0', '0', 'system:dept:list', 'tree', 'admin', '2024-03-07 20:14:37', '', NULL, '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', '', 1, 0, 'C', '0', '0', 'system:post:list', 'post', 'admin', '2024-03-07 20:14:37', '', NULL, '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', '', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2024-03-07 20:14:37', '', NULL, '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', '', 1, 0, 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2024-03-07 20:14:37', '', NULL, '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, 'notice', 'system/notice/index', '', 1, 0, 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2024-03-07 20:14:37', '', NULL, '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, 'log', '', '', 1, 0, 'M', '0', '0', '', 'log', 'admin', '2024-03-07 20:14:37', '', NULL, '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, 'online', 'monitor/online/index', '', 1, 0, 'C', '0', '0', 'monitor:online:list', 'online', 'admin', '2024-03-07 20:14:37', '', NULL, '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, 'job', 'monitor/job/index', '', 1, 0, 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2024-03-07 20:14:37', '', NULL, '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, 'druid', 'monitor/druid/index', '', 1, 0, 'C', '0', '0', 'monitor:druid:list', 'druid', 'admin', '2024-03-07 20:14:37', '', NULL, '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 2, 4, 'server', 'monitor/server/index', '', 1, 0, 'C', '0', '0', 'monitor:server:list', 'server', 'admin', '2024-03-07 20:14:37', '', NULL, '服务监控菜单');
INSERT INTO `sys_menu` VALUES (113, '缓存监控', 2, 5, 'cache', 'monitor/cache/index', '', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis', 'admin', '2024-03-07 20:14:37', '', NULL, '缓存监控菜单');
INSERT INTO `sys_menu` VALUES (114, '缓存列表', 2, 6, 'cacheList', 'monitor/cache/list', '', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis-list', 'admin', '2024-03-07 20:14:37', '', NULL, '缓存列表菜单');
INSERT INTO `sys_menu` VALUES (115, '表单构建', 3, 1, 'build', 'tool/build/index', '', 1, 0, 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2024-03-07 20:14:37', '', NULL, '表单构建菜单');
INSERT INTO `sys_menu` VALUES (116, '代码生成', 3, 2, 'gen', 'tool/gen/index', '', 1, 0, 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2024-03-07 20:14:37', '', NULL, '代码生成菜单');
INSERT INTO `sys_menu` VALUES (117, '系统接口', 3, 3, 'swagger', 'tool/swagger/index', '', 1, 0, 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', '2024-03-07 20:14:37', '', NULL, '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, 'operlog', 'monitor/operlog/index', '', 1, 0, 'C', '0', '0', 'monitor:operlog:list', 'form', 'admin', '2024-03-07 20:14:37', '', NULL, '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, 'logininfor', 'monitor/logininfor/index', '', 1, 0, 'C', '0', '0', 'monitor:logininfor:list', 'logininfor', 'admin', '2024-03-07 20:14:37', '', NULL, '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1000, '用户查询', 100, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1001, '用户新增', 100, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1002, '用户修改', 100, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1003, '用户删除', 100, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1004, '用户导出', 100, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1005, '用户导入', 100, 6, '', '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1006, '重置密码', 100, 7, '', '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1007, '角色查询', 101, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1008, '角色新增', 101, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1009, '角色修改', 101, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1010, '角色删除', 101, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1011, '角色导出', 101, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1012, '菜单查询', 102, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1013, '菜单新增', 102, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1014, '菜单修改', 102, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1015, '菜单删除', 102, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1016, '部门查询', 103, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1017, '部门新增', 103, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1018, '部门修改', 103, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1019, '部门删除', 103, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1020, '岗位查询', 104, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:post:query', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1021, '岗位新增', 104, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:post:add', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1022, '岗位修改', 104, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:post:edit', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1023, '岗位删除', 104, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:post:remove', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1024, '岗位导出', 104, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:post:export', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1025, '字典查询', 105, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:query', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1026, '字典新增', 105, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:add', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1027, '字典修改', 105, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1028, '字典删除', 105, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1029, '字典导出', 105, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:export', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1030, '参数查询', 106, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:query', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1031, '参数新增', 106, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:add', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1032, '参数修改', 106, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:edit', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1033, '参数删除', 106, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:remove', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1034, '参数导出', 106, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:export', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1035, '公告查询', 107, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1036, '公告新增', 107, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1037, '公告修改', 107, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1038, '公告删除', 107, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1039, '操作查询', 500, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:query', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1040, '操作删除', 500, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:remove', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1041, '日志导出', 500, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:export', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1042, '登录查询', 501, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:query', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1043, '登录删除', 501, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:remove', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1044, '日志导出', 501, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:export', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1045, '账户解锁', 501, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:unlock', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1046, '在线查询', 109, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:query', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1047, '批量强退', 109, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1048, '单条强退', 109, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1049, '任务查询', 110, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1050, '任务新增', 110, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1051, '任务修改', 110, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1052, '任务删除', 110, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1053, '状态修改', 110, 5, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1054, '任务导出', 110, 6, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1055, '生成查询', 116, 1, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1056, '生成修改', 116, 2, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1057, '生成删除', 116, 3, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1058, '导入代码', 116, 4, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1059, '预览代码', 116, 5, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1060, '生成代码', 116, 6, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2000, '意见反馈', 1, 1, 'feedback', 'system/feedback/index', NULL, 1, 0, 'C', '0', '0', 'system:feedback:list', '#', 'admin', '2024-03-07 20:29:22', 'admin', '2024-03-07 20:32:25', '意见反馈菜单');
INSERT INTO `sys_menu` VALUES (2001, '意见反馈查询', 2000, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:feedback:query', '#', 'admin', '2024-03-07 20:29:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2002, '意见反馈新增', 2000, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:feedback:add', '#', 'admin', '2024-03-07 20:29:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2003, '意见反馈修改', 2000, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:feedback:edit', '#', 'admin', '2024-03-07 20:29:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2004, '意见反馈删除', 2000, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:feedback:remove', '#', 'admin', '2024-03-07 20:29:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2005, '意见反馈导出', 2000, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:feedback:export', '#', 'admin', '2024-03-07 20:29:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2006, '座位预约', 1, 1, 'reservation', 'system/reservation/index', NULL, 1, 0, 'C', '0', '0', 'system:reservation:list', '#', 'admin', '2024-03-07 20:29:22', '', NULL, '座位预约菜单');
INSERT INTO `sys_menu` VALUES (2007, '座位预约查询', 2006, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:reservation:query', '#', 'admin', '2024-03-07 20:29:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2008, '座位预约新增', 2006, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:reservation:add', '#', 'admin', '2024-03-07 20:29:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2009, '座位预约修改', 2006, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:reservation:edit', '#', 'admin', '2024-03-07 20:29:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2010, '座位预约删除', 2006, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:reservation:remove', '#', 'admin', '2024-03-07 20:29:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2011, '座位预约导出', 2006, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:reservation:export', '#', 'admin', '2024-03-07 20:29:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2012, '座位管理', 1, 1, 'seat', 'system/seat/index', NULL, 1, 0, 'C', '0', '0', 'system:seat:list', '#', 'admin', '2024-03-07 20:29:22', '', NULL, '座位管理菜单');
INSERT INTO `sys_menu` VALUES (2013, '座位管理查询', 2012, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:seat:query', '#', 'admin', '2024-03-07 20:29:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2014, '座位管理新增', 2012, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:seat:add', '#', 'admin', '2024-03-07 20:29:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2015, '座位管理修改', 2012, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:seat:edit', '#', 'admin', '2024-03-07 20:29:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2016, '座位管理删除', 2012, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:seat:remove', '#', 'admin', '2024-03-07 20:29:23', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2017, '座位管理导出', 2012, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:seat:export', '#', 'admin', '2024-03-07 20:29:23', '', NULL, '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '通知公告表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (11, '标题', '2', 0xE58685E5AEB9, '0', '2024030705', '2024-03-08 17:18:02', '', NULL, NULL);
INSERT INTO `sys_notice` VALUES (12, '2024年公告', '2', 0xE4BAB2E788B1E79A84E794A8E688B7EFBC8CE6849FE8B0A2E682A8E4B880E79BB4E4BBA5E69DA5E5AFB9E68891E4BBACE59BBEE4B9A6E99885E8AFBBE5AEA4E79A84E694AFE68C81E5928CE4BDBFE794A8EFBC81E68891E4BBACE99D9EE5B8B8E9878DE8A786E682A8E79A84E6848FE8A781E5928CE5BBBAE8AEAEEFBC8CE4BBA5E4BEBFE4B88DE696ADE694B9E8BF9BE5928CE68F90E58D87E794A8E688B7E4BD93E9AA8CE38082E5A682E69E9CE682A8E59CA8E4BDBFE794A8E8BF87E7A88BE4B8ADE98187E588B0E4BBBBE4BD95E997AEE9A298E68896E99499E8AFAFEFBC8CE8AFB7E58F8AE697B6E59091E68891E4BBACE58F8DE9A688E38082E68891E4BBACE4BC9AE5B0BDE5BFABE8A7A3E586B3E5B9B6E59091E682A8E68F90E4BE9BE6BBA1E6848FE79A84E7AD94, '0', '2024030705', '2024-03-08 17:18:44', '', NULL, NULL);
INSERT INTO `sys_notice` VALUES (13, '公告3', '2', 0xE4BAB2E788B1E79A84E794A8E688B7EFBC8CE5AFB9E4BA8EE59BBEE4B9A6E9A686E79A84E69C8DE58AA1E5928CE8AEBEE696BDEFBC8CE682A8E698AFE590A6E69C89E4BBBBE4BD95E694B9E8BF9BE6848FE8A781E68896E5BBBAE8AEAEEFBC9FE68891E4BBACE6ACA2E8BF8EE682A8E59091E68891E4BBACE68F90E4BE9BE58F8DE9A688EFBC8CE4BBA5E5B8AEE58AA9E68891E4BBACE694B9E8BF9BE5928CE68F90E58D87E69C8DE58AA1E8B4A8E9878FE38082E682A8E58FAFE4BBA5E9809AE8BF87E982AEE7AEB1E68896E5BA94E794A8E7A88BE5BA8FE58685E79A84E88194E7B3BBE68891E4BBACE9A1B5E99DA2E4B88EE68891E4BBACE58F96E5BE97E88194E7B3BBEFBC8CE68891E4BBACE69C9FE5BE85E590ACE588B0E682A8E5AE9DE8B4B5E79A84E6848FE8A781, '0', '2024030705', '2024-03-08 17:19:45', '', NULL, NULL);
INSERT INTO `sys_notice` VALUES (14, '公告5', '2', 0xE5B08AE695ACE79A84E794A8E688B7EFBC8CE5A682E69E9CE682A8E59CA8E4BDBFE794A8E8BF87E7A88BE4B8ADE98187E588B0E4BA86E794A8E688B7E7958CE99DA2E4B88AE79A84E99ABEE4BBA5E79086E8A7A3E68896E4B88DE4BEBFE4BDBFE794A8E79A84E59CB0E696B9EFBC8CE8AFB7E58F8AE697B6E59091E68891E4BBACE58F8DE9A688E38082E682A8E79A84E58F8DE9A688E5AFB9E4BA8EE68891E4BBACE694B9E8BF9BE794A8E688B7E4BD93E9AA8CE99D9EE5B8B8E9878DE8A681E38082E6849FE8B0A2E682A8E79A84E694AFE68C81E5928CE9858DE59088EFBC81, '0', '2024030705', '2024-03-08 17:19:59', '', NULL, NULL);
INSERT INTO `sys_notice` VALUES (15, '标题', '1', 0x3C703E32313334E9A29D3C2F703E, '0', 'admin', '2024-03-08 17:27:07', '', NULL, NULL);
INSERT INTO `sys_notice` VALUES (16, '123', '2', 0x343536, '0', 'admin', '2024-03-10 16:10:38', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime NULL DEFAULT NULL COMMENT '操作时间',
  `cost_time` bigint(20) NULL DEFAULT 0 COMMENT '消耗时间',
  PRIMARY KEY (`oper_id`) USING BTREE,
  INDEX `idx_sys_oper_log_bt`(`business_type`) USING BTREE,
  INDEX `idx_sys_oper_log_s`(`status`) USING BTREE,
  INDEX `idx_sys_oper_log_ot`(`oper_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 311 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (100, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":\"lr_seat,lr_reservation,lr_feedback\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 20:17:54', 114);
INSERT INTO `sys_oper_log` VALUES (101, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', '研发部门', '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"feedback\",\"className\":\"LrFeedback\",\"columns\":[{\"capJavaField\":\"Id\",\"columnComment\":\"主键\",\"columnId\":1,\"columnName\":\"id\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"createTime\":\"2024-03-07 20:17:54\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"isRequired\":\"0\",\"javaField\":\"id\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"FeedbackTitle\",\"columnComment\":\"意见反馈标题\",\"columnId\":2,\"columnName\":\"feedback_title\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2024-03-07 20:17:54\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"0\",\"javaField\":\"feedbackTitle\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"FeedbackContent\",\"columnComment\":\"意见反馈内容\",\"columnId\":3,\"columnName\":\"feedback_content\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2024-03-07 20:17:54\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"editor\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"0\",\"javaField\":\"feedbackContent\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":1,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"FeedbackLocation\",\"columnComment\":\"意见反馈位置信息\",\"columnId\":4,\"columnName\":\"feedback_location\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2024-03-07 20:17:54\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 20:18:55', 37);
INSERT INTO `sys_oper_log` VALUES (102, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', '研发部门', '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"reservation\",\"className\":\"LrReservation\",\"columns\":[{\"capJavaField\":\"ReservationId\",\"columnComment\":\"预订id\",\"columnId\":12,\"columnName\":\"reservation_id\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"createTime\":\"2024-03-07 20:17:54\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"isRequired\":\"0\",\"javaField\":\"reservationId\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"StudentId\",\"columnComment\":\"用户id\",\"columnId\":13,\"columnName\":\"student_id\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"createTime\":\"2024-03-07 20:17:54\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"studentId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":2,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"SeatId\",\"columnComment\":\"座位id\",\"columnId\":14,\"columnName\":\"seat_id\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"createTime\":\"2024-03-07 20:17:54\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"1\",\"javaField\":\"seatId\",\"javaType\":\"Long\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":true,\"sort\":3,\"superColumn\":false,\"tableId\":2,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"ReservationDate\",\"columnComment\":\"预定日期\",\"columnId\":15,\"columnName\":\"reservation_date\",\"columnType\":\"datetime\",\"createBy\":\"admin\",\"createTime\":\"2024-03-07 20:17:54\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"datetime\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 20:19:13', 41);
INSERT INTO `sys_oper_log` VALUES (103, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', '研发部门', '/tool/gen', '127.0.0.1', '内网IP', '{\"businessName\":\"seat\",\"className\":\"LrSeat\",\"columns\":[{\"capJavaField\":\"SeatId\",\"columnComment\":\"主键\",\"columnId\":25,\"columnName\":\"seat_id\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"createTime\":\"2024-03-07 20:17:54\",\"dictType\":\"\",\"edit\":false,\"htmlType\":\"input\",\"increment\":true,\"insert\":true,\"isIncrement\":\"1\",\"isInsert\":\"1\",\"isPk\":\"1\",\"isRequired\":\"0\",\"javaField\":\"seatId\",\"javaType\":\"Long\",\"list\":false,\"params\":{},\"pk\":true,\"query\":false,\"queryType\":\"EQ\",\"required\":false,\"sort\":1,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"SeatLocation\",\"columnComment\":\"座位信息\",\"columnId\":26,\"columnName\":\"seat_location\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2024-03-07 20:17:54\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"0\",\"javaField\":\"seatLocation\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":2,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"SeatStatus\",\"columnComment\":\"0为可用，1为故障不可用\",\"columnId\":27,\"columnName\":\"seat_status\",\"columnType\":\"char(2)\",\"createBy\":\"admin\",\"createTime\":\"2024-03-07 20:17:54\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"radio\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"isQuery\":\"1\",\"isRequired\":\"0\",\"javaField\":\"seatStatus\",\"javaType\":\"String\",\"list\":true,\"params\":{},\"pk\":false,\"query\":true,\"queryType\":\"EQ\",\"required\":false,\"sort\":3,\"superColumn\":false,\"tableId\":3,\"updateBy\":\"\",\"usableColumn\":false},{\"capJavaField\":\"SeatNumber\",\"columnComment\":\"座位编号\",\"columnId\":28,\"columnName\":\"seat_number\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"createTime\":\"2024-03-07 20:17:54\",\"dictType\":\"\",\"edit\":true,\"htmlType\":\"input\",\"increment\":false,\"insert\":true,\"isEdit\":\"1\",\"isIncrement\":\"0\",\"isInsert\":\"1\",\"isList\":\"1\",\"isPk\":\"0\",\"is', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 20:19:28', 30);
INSERT INTO `sys_oper_log` VALUES (104, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', '研发部门', '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{\"tables\":\"lr_feedback,lr_reservation,lr_seat\"}', NULL, 0, NULL, '2024-03-07 20:19:32', 198);
INSERT INTO `sys_oper_log` VALUES (105, '用户管理', 1, 'com.ruoyi.web.controller.system.SysUserController.add()', 'POST', 1, 'admin', '研发部门', '/system/user', '172.24.87.235', '内网IP', '{\"admin\":false,\"createBy\":\"admin\",\"nickName\":\"2024030705\",\"params\":{},\"postIds\":[],\"roleIds\":[],\"status\":\"0\",\"userId\":100,\"userName\":\"2024030705\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 20:29:32', 104);
INSERT INTO `sys_oper_log` VALUES (106, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/menu', '127.0.0.1', '内网IP', '{\"children\":[],\"component\":\"system/feedback/index\",\"createTime\":\"2024-03-07 20:29:22\",\"icon\":\"#\",\"isCache\":\"0\",\"isFrame\":\"1\",\"menuId\":2000,\"menuName\":\"意见反馈\",\"menuType\":\"C\",\"orderNum\":1,\"params\":{},\"parentId\":1,\"path\":\"feedback\",\"perms\":\"system:feedback:list\",\"status\":\"0\",\"updateBy\":\"admin\",\"visible\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 20:32:25', 16);
INSERT INTO `sys_oper_log` VALUES (107, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, 'admin', '研发部门', '/system/seat', '127.0.0.1', '内网IP', '{\"createTime\":\"2024-03-07 20:34:51\",\"params\":{},\"remark\":\"1\",\"seatId\":3,\"seatLocation\":\"自习室A区\",\"seatNumber\":\"01\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 20:34:51', 29);
INSERT INTO `sys_oper_log` VALUES (108, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, 'admin', '研发部门', '/system/seat', '127.0.0.1', '内网IP', '{\"createTime\":\"2024-03-07 20:35:04\",\"params\":{},\"seatId\":4,\"seatLocation\":\"自习室B区\",\"seatNumber\":\"02\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 20:35:04', 18);
INSERT INTO `sys_oper_log` VALUES (109, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, 'admin', '研发部门', '/system/seat', '127.0.0.1', '内网IP', '{\"createTime\":\"2024-03-07 20:35:21\",\"params\":{},\"seatId\":5,\"seatLocation\":\"自习室A区\",\"seatNumber\":\"02\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 20:35:22', 18);
INSERT INTO `sys_oper_log` VALUES (110, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, 'admin', '研发部门', '/system/seat', '127.0.0.1', '内网IP', '{\"createTime\":\"2024-03-07 20:35:28\",\"params\":{},\"seatId\":6,\"seatLocation\":\"自习室A区\",\"seatNumber\":\"05\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 20:35:28', 19);
INSERT INTO `sys_oper_log` VALUES (111, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, 'admin', '研发部门', '/system/reservation', '127.0.0.1', '内网IP', '{\"createTime\":\"2024-03-07 20:35:44\",\"params\":{},\"reservationDate\":\"2024-03-08\",\"reservationId\":3,\"seatId\":3,\"studentId\":10}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 20:35:44', 23);
INSERT INTO `sys_oper_log` VALUES (112, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, 'admin', '研发部门', '/system/reservation', '127.0.0.1', '内网IP', '{\"createTime\":\"2024-03-07 20:36:30\",\"params\":{},\"reservationDate\":\"2024-03-08\",\"reservationId\":4,\"seatId\":4,\"studentId\":15}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 20:36:30', 19);
INSERT INTO `sys_oper_log` VALUES (113, '座位预约', 2, 'com.ruoyi.web.controller.system.LrReservationController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/reservation', '127.0.0.1', '内网IP', '{\"createBy\":\"\",\"createTime\":\"2024-03-07 20:35:45\",\"delFlag\":\"0\",\"isActive\":\"0\",\"isCancal\":\"0\",\"params\":{},\"reservationDate\":\"2024-03-08\",\"reservationId\":3,\"seatId\":3,\"studentId\":10,\"timeSlot\":1,\"updateBy\":\"\",\"updateTime\":\"2024-03-07 20:48:57\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 20:48:57', 19);
INSERT INTO `sys_oper_log` VALUES (114, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, 'admin', '研发部门', '/system/reservation/reservation', '154.64.226.174', 'XX XX', '{\"createTime\":\"2024-03-07 21:19:44\",\"params\":{},\"remark\":\"111\",\"reservationDate\":\"2024-03-07\",\"seatId\":1,\"timeSlot\":1}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'student_id\' doesn\'t have a default value\r\n### The error may exist in file [D:\\project\\code\\libraryReservation\\Server\\RuoYi-Vue-v3.8.7\\ruoyi-system\\target\\classes\\mapper\\system\\LrReservationMapper.xml]\r\n### The error may involve com.ruoyi.system.mapper.LrReservationMapper.insertLrReservation-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into lr_reservation          ( seat_id,             reservation_date,             time_slot,                                                    create_time,                                       remark )           values ( ?,             ?,             ?,                                                    ?,                                       ? )\r\n### Cause: java.sql.SQLException: Field \'student_id\' doesn\'t have a default value\n; Field \'student_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'student_id\' doesn\'t have a default value', '2024-03-07 21:19:45', 60);
INSERT INTO `sys_oper_log` VALUES (115, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, 'admin', '研发部门', '/system/reservation/reservation', '154.64.226.174', 'XX XX', '{\"createTime\":\"2024-03-07 21:20:06\",\"params\":{},\"remark\":\"111\",\"reservationDate\":\"2024-03-07\",\"reservationId\":5,\"seatId\":1,\"studentId\":11,\"timeSlot\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 21:20:06', 10);
INSERT INTO `sys_oper_log` VALUES (116, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, '2024030705', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-07 21:20:34\",\"params\":{},\"seatId\":17,\"seatNumber\":\"05\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 21:20:34', 21);
INSERT INTO `sys_oper_log` VALUES (117, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, '2024030705', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-07 21:20:59\",\"params\":{},\"seatId\":18,\"seatNumber\":\"05\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 21:20:59', 17);
INSERT INTO `sys_oper_log` VALUES (118, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, '2024030705', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-07 21:26:25\",\"params\":{},\"seatId\":19,\"seatNumber\":\"05\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 21:26:25', 16);
INSERT INTO `sys_oper_log` VALUES (119, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, '2024030705', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-07 21:27:05\",\"params\":{},\"remark\":\"1\",\"seatId\":20,\"seatNumber\":\"01\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 21:27:05', 8);
INSERT INTO `sys_oper_log` VALUES (120, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, '2024030705', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-07 21:28:04\",\"params\":{},\"seatId\":21,\"seatNumber\":\"05\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 21:28:04', 15);
INSERT INTO `sys_oper_log` VALUES (121, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, '2024030705', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-07 21:28:16\",\"params\":{},\"seatId\":22,\"seatNumber\":\"02\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 21:28:16', 15);
INSERT INTO `sys_oper_log` VALUES (122, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, '2024030705', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-07 21:35:19\",\"params\":{},\"seatId\":23,\"seatNumber\":\"02\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 21:35:19', 15);
INSERT INTO `sys_oper_log` VALUES (123, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, '2024030705', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-07 21:35:25\",\"params\":{},\"remark\":\"1\",\"seatId\":24,\"seatNumber\":\"01\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 21:35:25', 15);
INSERT INTO `sys_oper_log` VALUES (124, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, '2024030705', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-07 21:37:31\",\"params\":{},\"remark\":\"1\",\"seatId\":25,\"seatNumber\":\"01\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 21:37:31', 15);
INSERT INTO `sys_oper_log` VALUES (125, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, '2024030705', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-07 21:41:30\",\"params\":{},\"remark\":\"1\",\"seatId\":26,\"seatNumber\":\"01\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 21:41:30', 6);
INSERT INTO `sys_oper_log` VALUES (126, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, '2024030705', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-07 21:42:06\",\"params\":{},\"remark\":\"1\",\"seatId\":27,\"seatNumber\":\"01\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 21:42:06', 7);
INSERT INTO `sys_oper_log` VALUES (127, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, '2024030705', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-07 21:43:00\",\"params\":{},\"remark\":\"1\",\"seatId\":28,\"seatNumber\":\"01\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 21:43:00', 7);
INSERT INTO `sys_oper_log` VALUES (128, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, '2024030705', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-07 21:43:35\",\"params\":{},\"remark\":\"2\",\"seatId\":29,\"seatNumber\":\"01\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 21:43:35', 16);
INSERT INTO `sys_oper_log` VALUES (129, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, 'admin', '研发部门', '/system/reservation/reservation', '154.64.226.174', 'XX XX', '{\"createTime\":\"2024-03-07 21:44:41\",\"params\":{},\"reservationDate\":\"2024-03-08\",\"reservationId\":6,\"seatId\":9,\"studentId\":11,\"timeSlot\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 21:44:41', 17);
INSERT INTO `sys_oper_log` VALUES (130, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, '2024030705', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-07 21:45:15\",\"params\":{},\"seatId\":30,\"seatNumber\":\"01\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 21:45:15', 7);
INSERT INTO `sys_oper_log` VALUES (131, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '2024030705', NULL, '/system/reservation/reservation', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-07 21:45:49\",\"params\":{},\"reservationDate\":\"2024-03-08\",\"reservationId\":7,\"seatId\":3,\"studentId\":100,\"timeSlot\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 21:45:49', 14);
INSERT INTO `sys_oper_log` VALUES (132, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, '2024030705', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-07 22:01:34\",\"params\":{},\"seatId\":31,\"seatNumber\":\"02\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 22:01:34', 7);
INSERT INTO `sys_oper_log` VALUES (133, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, '2024030705', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-07 22:02:31\",\"params\":{},\"seatId\":32,\"seatNumber\":\"02\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 22:02:31', 15);
INSERT INTO `sys_oper_log` VALUES (134, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, '2024030705', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-07 22:02:39\",\"params\":{},\"seatId\":33,\"seatNumber\":\"02\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 22:02:39', 6);
INSERT INTO `sys_oper_log` VALUES (135, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, '2024030705', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-07 22:03:57\",\"params\":{},\"seatId\":34}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 22:03:57', 15);
INSERT INTO `sys_oper_log` VALUES (136, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '2024030705', NULL, '/system/reservation/reservation', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-07 22:04:58\",\"params\":{},\"reservationDate\":\"2024-03-08\",\"reservationId\":8,\"seatId\":5,\"studentId\":100,\"timeSlot\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 22:04:58', 6);
INSERT INTO `sys_oper_log` VALUES (137, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '2024030705', NULL, '/system/reservation/reservation', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-07 22:06:37\",\"params\":{},\"reservationDate\":\"2024-03-08\",\"reservationId\":9,\"seatId\":6,\"studentId\":100,\"timeSlot\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 22:06:37', 6);
INSERT INTO `sys_oper_log` VALUES (138, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, 'admin', '研发部门', '/system/reservation/reservation', '154.64.226.174', 'XX XX', '{\"createTime\":\"2024-03-07 22:20:31\",\"params\":{},\"reservationDate\":\"2024-03-07\",\"reservationId\":10,\"seatId\":9,\"studentId\":100,\"timeSlot\":3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-07 22:20:31', 36);
INSERT INTO `sys_oper_log` VALUES (139, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '2024030705', NULL, '/system/reservation/reservation', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-08 12:51:32\",\"params\":{},\"reservationDate\":\"2024-03-09\",\"seatId\":3,\"timeSlot\":1}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'student_id\' doesn\'t have a default value\r\n### The error may exist in file [D:\\project\\code\\libraryReservation\\Server\\RuoYi-Vue-v3.8.7\\ruoyi-system\\target\\classes\\mapper\\system\\LrReservationMapper.xml]\r\n### The error may involve com.ruoyi.system.mapper.LrReservationMapper.insertLrReservation-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into lr_reservation          ( seat_id,             reservation_date,             time_slot,                                                    create_time )           values ( ?,             ?,             ?,                                                    ? )\r\n### Cause: java.sql.SQLException: Field \'student_id\' doesn\'t have a default value\n; Field \'student_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'student_id\' doesn\'t have a default value', '2024-03-08 12:51:32', 52);
INSERT INTO `sys_oper_log` VALUES (140, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '2024030705', NULL, '/system/reservation/reservation', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-08 12:53:27\",\"params\":{},\"reservationDate\":\"2024-03-09\",\"seatId\":3,\"timeSlot\":1}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'student_id\' doesn\'t have a default value\r\n### The error may exist in file [D:\\project\\code\\libraryReservation\\Server\\RuoYi-Vue-v3.8.7\\ruoyi-system\\target\\classes\\mapper\\system\\LrReservationMapper.xml]\r\n### The error may involve com.ruoyi.system.mapper.LrReservationMapper.insertLrReservation-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into lr_reservation          ( seat_id,             reservation_date,             time_slot,                                                    create_time )           values ( ?,             ?,             ?,                                                    ? )\r\n### Cause: java.sql.SQLException: Field \'student_id\' doesn\'t have a default value\n; Field \'student_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'student_id\' doesn\'t have a default value', '2024-03-08 12:53:27', 2);
INSERT INTO `sys_oper_log` VALUES (141, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '2024030705', NULL, '/system/reservation/reservation', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-08 12:54:17\",\"params\":{},\"reservationDate\":\"2024-03-09\",\"reservationId\":11,\"seatId\":3,\"studentId\":100,\"timeSlot\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-08 12:54:17', 17);
INSERT INTO `sys_oper_log` VALUES (142, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '2024030705', NULL, '/system/reservation/reservation', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-08 12:56:48\",\"params\":{},\"reservationId\":12,\"seatId\":5,\"studentId\":100}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-08 12:56:48', 15);
INSERT INTO `sys_oper_log` VALUES (143, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '2024030705', NULL, '/system/reservation/reservation', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-08 13:00:51\",\"params\":{},\"reservationDate\":\"2024-03-09\",\"reservationId\":13,\"seatId\":5,\"studentId\":100,\"timeSlot\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-08 13:00:51', 16);
INSERT INTO `sys_oper_log` VALUES (144, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '2024030705', NULL, '/system/reservation/reservation', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-08 13:05:14\",\"params\":{},\"reservationDate\":\"2024-03-09\",\"reservationId\":14,\"seatId\":4,\"studentId\":100,\"timeSlot\":3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-08 13:05:14', 7);
INSERT INTO `sys_oper_log` VALUES (145, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '2024030705', NULL, '/system/reservation/reservation', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-08 13:05:22\",\"params\":{},\"reservationDate\":\"2024-03-09\",\"reservationId\":15,\"seatId\":4,\"studentId\":100,\"timeSlot\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-08 13:05:22', 16);
INSERT INTO `sys_oper_log` VALUES (146, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '2024030705', NULL, '/system/reservation/reservation', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-08 13:05:27\",\"params\":{},\"reservationDate\":\"2024-03-09\",\"reservationId\":16,\"seatId\":4,\"studentId\":100,\"timeSlot\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-08 13:05:27', 16);
INSERT INTO `sys_oper_log` VALUES (147, '入座', 2, 'com.ruoyi.web.controller.system.LrReservationController.takePlace()', 'POST', 1, '2024030705', NULL, '/system/reservation/takePlace', '172.24.87.101', '内网IP', '{\"params\":{},\"reservationId\":9}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-08 14:56:38', 10);
INSERT INTO `sys_oper_log` VALUES (148, '入座', 2, 'com.ruoyi.web.controller.system.LrReservationController.takePlace()', 'POST', 1, '2024030705', NULL, '/system/reservation/takePlace', '172.24.87.101', '内网IP', '{\"params\":{},\"reservationId\":9}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-08 15:36:46', 25);
INSERT INTO `sys_oper_log` VALUES (149, '入座', 2, 'com.ruoyi.web.controller.system.LrReservationController.takePlace()', 'POST', 1, '2024030705', NULL, '/system/reservation/takePlace', '172.24.87.101', '内网IP', '{\"params\":{},\"reservationId\":9}', NULL, 1, 'nested exception is org.apache.ibatis.binding.BindingException: Parameter \'reservationId\' not found. Available parameters are [avatar, userName, param1, param2]', '2024-03-08 15:43:19', 14);
INSERT INTO `sys_oper_log` VALUES (150, '意见反馈', 1, 'com.ruoyi.web.controller.system.LrFeedbackController.add()', 'POST', 1, '2024030705', NULL, '/system/feedback', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-08 15:53:41\",\"feedbackContent\":\"666666\",\"id\":4,\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-08 15:53:41', 31);
INSERT INTO `sys_oper_log` VALUES (151, '意见反馈', 1, 'com.ruoyi.web.controller.system.LrFeedbackController.add()', 'POST', 1, '2024030705', NULL, '/system/feedback', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-08 15:54:36\",\"feedbackContent\":\"8888888888888\",\"id\":5,\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-08 15:54:36', 10);
INSERT INTO `sys_oper_log` VALUES (152, '意见反馈', 1, 'com.ruoyi.web.controller.system.LrFeedbackController.add()', 'POST', 1, '2024030705', NULL, '/system/feedback', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-08 15:54:53\",\"feedbackContent\":\"8555555555555555\",\"id\":6,\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-08 15:54:53', 12);
INSERT INTO `sys_oper_log` VALUES (153, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '2024030705', NULL, '/system/reservation/reservation', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-08 16:06:02\",\"params\":{},\"reservationDate\":\"2024-03-09\",\"reservationId\":17,\"seatId\":3,\"studentId\":100,\"timeSlot\":3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-08 16:06:02', 14);
INSERT INTO `sys_oper_log` VALUES (154, '入座', 2, 'com.ruoyi.web.controller.system.LrReservationController.takePlace()', 'POST', 1, '2024030705', NULL, '/system/reservation/takePlace', '172.24.87.101', '内网IP', '{\"params\":{},\"reservationId\":9}', NULL, 1, 'nested exception is org.apache.ibatis.binding.BindingException: Parameter \'reservationId\' not found. Available parameters are [avatar, userName, param1, param2]', '2024-03-08 16:31:25', 12);
INSERT INTO `sys_oper_log` VALUES (155, '意见反馈', 1, 'com.ruoyi.web.controller.system.LrFeedbackController.add()', 'POST', 1, '2024030705', NULL, '/system/feedback', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-08 17:14:10\",\"feedbackContent\":\"尊敬的管理员，我希望在图书馆增加一些关于编程和技术方面的书籍，以便满足对这方面知识的需求。感谢您的考虑！\",\"id\":7,\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-08 17:14:10', 26);
INSERT INTO `sys_oper_log` VALUES (156, '意见反馈', 1, 'com.ruoyi.web.controller.system.LrFeedbackController.add()', 'POST', 1, '2024030705', NULL, '/system/feedback', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-08 17:14:18\",\"feedbackContent\":\"亲爱的管理员，我希望图书馆能够增加一些关于创业和商业管理的书籍，这样我可以更好地学习和了解相关知识。谢谢您！\",\"id\":8,\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-08 17:14:18', 16);
INSERT INTO `sys_oper_log` VALUES (157, '意见反馈', 1, 'com.ruoyi.web.controller.system.LrFeedbackController.add()', 'POST', 1, '2024030705', NULL, '/system/feedback', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-08 17:14:26\",\"feedbackContent\":\"亲爱的管理员，我希望图书馆能够增加更多的座位和学习空间，因为在繁忙的时间段，很难找到一个安静的地方专心学习。希望您能考虑我的建议，感谢您的努力！\",\"id\":9,\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-08 17:14:26', 16);
INSERT INTO `sys_oper_log` VALUES (158, '意见反馈', 1, 'com.ruoyi.web.controller.system.LrFeedbackController.add()', 'POST', 1, '2024030705', NULL, '/system/feedback', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-08 17:14:33\",\"feedbackContent\":\"亲爱的管理员，我希望图书馆能够增加更多的座位和学习空间，因为在繁忙的时间段，很难找到一个安静的地方专心学习。希望您能考虑我的建议，感谢您的努力！\",\"id\":10,\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-08 17:14:33', 17);
INSERT INTO `sys_oper_log` VALUES (159, '通知公告', 1, 'com.ruoyi.web.controller.system.SysNoticeController.add()', 'POST', 1, 'admin', '研发部门', '/system/notice', '172.24.87.101', '内网IP', '{\"createBy\":\"admin\",\"noticeContent\":\"<p>公告内容</p>\",\"noticeTitle\":\"公告标题\",\"noticeType\":\"2\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-08 17:15:24', 19);
INSERT INTO `sys_oper_log` VALUES (160, '通知公告', 1, 'com.ruoyi.web.controller.system.SysNoticeController.add()', 'POST', 1, '2024030705', NULL, '/system/notice/', '172.24.87.101', '内网IP', '{\"createBy\":\"2024030705\",\"noticeContent\":\"内容\",\"noticeTitle\":\"标题\",\"noticeType\":\"2\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-08 17:18:02', 17);
INSERT INTO `sys_oper_log` VALUES (161, '通知公告', 1, 'com.ruoyi.web.controller.system.SysNoticeController.add()', 'POST', 1, '2024030705', NULL, '/system/notice/', '172.24.87.101', '内网IP', '{\"createBy\":\"2024030705\",\"noticeContent\":\"亲爱的用户，感谢您一直以来对我们图书阅读室的支持和使用！我们非常重视您的意见和建议，以便不断改进和提升用户体验。如果您在使用过程中遇到任何问题或错误，请及时向我们反馈。我们会尽快解决并向您提供满意的答\",\"noticeTitle\":\"2024年公告\",\"noticeType\":\"2\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-08 17:18:44', 8);
INSERT INTO `sys_oper_log` VALUES (162, '通知公告', 1, 'com.ruoyi.web.controller.system.SysNoticeController.add()', 'POST', 1, '2024030705', NULL, '/system/notice/', '172.24.87.101', '内网IP', '{\"createBy\":\"2024030705\",\"noticeContent\":\"尊敬的用户，您是否希望看到更多的图书种类或图书推荐？我们非常愿意听取您的意见和建议，以满足您的阅读需求。请通过邮箱或应用程序内的联系我们页面向我们提供您的建议，我们会认真考虑并尽量满足您的需求。\",\"noticeTitle\":\"公告\",\"params\":{}}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'notice_type\' doesn\'t have a default value\r\n### The error may exist in file [D:\\project\\code\\libraryReservation\\Server\\RuoYi-Vue-v3.8.7\\ruoyi-system\\target\\classes\\mapper\\system\\SysNoticeMapper.xml]\r\n### The error may involve com.ruoyi.system.mapper.SysNoticeMapper.insertNotice-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into sys_notice (     notice_title,            notice_content,                  create_by,      create_time    )values(     ?,            ?,                  ?,      sysdate()   )\r\n### Cause: java.sql.SQLException: Field \'notice_type\' doesn\'t have a default value\n; Field \'notice_type\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'notice_type\' doesn\'t have a default value', '2024-03-08 17:18:54', 50);
INSERT INTO `sys_oper_log` VALUES (163, '通知公告', 1, 'com.ruoyi.web.controller.system.SysNoticeController.add()', 'POST', 1, '2024030705', NULL, '/system/notice/', '172.24.87.101', '内网IP', '{\"createBy\":\"2024030705\",\"noticeContent\":\"亲爱的用户，对于图书馆的服务和设施，您是否有任何改进意见或建议？我们欢迎您向我们提供反馈，以帮助我们改进和提升服务质量。您可以通过邮箱或应用程序内的联系我们页面与我们取得联系，我们期待听到您宝贵的意见\",\"noticeTitle\":\"公告2\",\"params\":{}}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'notice_type\' doesn\'t have a default value\r\n### The error may exist in file [D:\\project\\code\\libraryReservation\\Server\\RuoYi-Vue-v3.8.7\\ruoyi-system\\target\\classes\\mapper\\system\\SysNoticeMapper.xml]\r\n### The error may involve com.ruoyi.system.mapper.SysNoticeMapper.insertNotice-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into sys_notice (     notice_title,            notice_content,                  create_by,      create_time    )values(     ?,            ?,                  ?,      sysdate()   )\r\n### Cause: java.sql.SQLException: Field \'notice_type\' doesn\'t have a default value\n; Field \'notice_type\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'notice_type\' doesn\'t have a default value', '2024-03-08 17:19:06', 3);
INSERT INTO `sys_oper_log` VALUES (164, '通知公告', 1, 'com.ruoyi.web.controller.system.SysNoticeController.add()', 'POST', 1, '2024030705', NULL, '/system/notice/', '172.24.87.101', '内网IP', '{\"createBy\":\"2024030705\",\"noticeContent\":\"亲爱的用户，对于图书馆的服务和设施，您是否有任何改进意见或建议？我们欢迎您向我们提供反馈，以帮助我们改进和提升服务质量。您可以通过邮箱或应用程序内的联系我们页面与我们取得联系，我们期待听到您宝贵的意见\",\"noticeTitle\":\"公告3\",\"noticeType\":\"2\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-08 17:19:45', 16);
INSERT INTO `sys_oper_log` VALUES (165, '通知公告', 1, 'com.ruoyi.web.controller.system.SysNoticeController.add()', 'POST', 1, '2024030705', NULL, '/system/notice/', '172.24.87.101', '内网IP', '{\"createBy\":\"2024030705\",\"noticeContent\":\"尊敬的用户，如果您在使用过程中遇到了用户界面上的难以理解或不便使用的地方，请及时向我们反馈。您的反馈对于我们改进用户体验非常重要。感谢您的支持和配合！\",\"noticeTitle\":\"公告5\",\"noticeType\":\"2\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-08 17:19:59', 16);
INSERT INTO `sys_oper_log` VALUES (166, '通知公告', 3, 'com.ruoyi.web.controller.system.SysNoticeController.remove()', 'DELETE', 1, 'admin', '研发部门', '/system/notice/2', '172.24.87.101', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-08 17:20:30', 19);
INSERT INTO `sys_oper_log` VALUES (167, '通知公告', 3, 'com.ruoyi.web.controller.system.SysNoticeController.remove()', 'DELETE', 1, 'admin', '研发部门', '/system/notice/1', '172.24.87.101', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-08 17:20:33', 17);
INSERT INTO `sys_oper_log` VALUES (168, '通知公告', 3, 'com.ruoyi.web.controller.system.SysNoticeController.remove()', 'DELETE', 1, 'admin', '研发部门', '/system/notice/10', '172.24.87.101', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-08 17:20:56', 7);
INSERT INTO `sys_oper_log` VALUES (169, '通知公告', 1, 'com.ruoyi.web.controller.system.SysNoticeController.add()', 'POST', 1, 'admin', '研发部门', '/system/notice', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"noticeContent\":\"<p>2134额</p>\",\"noticeTitle\":\"标题\",\"noticeType\":\"1\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-08 17:27:07', 7);
INSERT INTO `sys_oper_log` VALUES (170, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/user', '127.0.0.1', '内网IP', '{\"admin\":false,\"avatar\":\"\",\"createBy\":\"admin\",\"createTime\":\"2024-03-07 20:29:32\",\"delFlag\":\"0\",\"deptId\":100,\"email\":\"archyqx@gmail.com\",\"loginDate\":\"2024-03-08 16:32:36\",\"loginIp\":\"172.24.87.101\",\"nickName\":\"2024030705\",\"params\":{},\"phonenumber\":\"18829539116\",\"postIds\":[1],\"remark\":\"111\",\"roleIds\":[2],\"roles\":[],\"sex\":\"0\",\"status\":\"0\",\"updateBy\":\"admin\",\"userId\":100,\"userName\":\"2024030705\",\"userType\":\"00\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-08 17:32:01', 25);
INSERT INTO `sys_oper_log` VALUES (171, '入座', 2, 'com.ruoyi.web.controller.system.LrReservationController.takePlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/takePlace', '172.24.87.101', '内网IP', '{\"params\":{},\"reservationId\":11}', NULL, 1, 'nested exception is org.apache.ibatis.binding.BindingException: Parameter \'reservationId\' not found. Available parameters are [avatar, userName, param1, param2]', '2024-03-09 08:35:27', 21);
INSERT INTO `sys_oper_log` VALUES (172, '入座', 2, 'com.ruoyi.web.controller.system.LrReservationController.takePlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/takePlace', '172.24.87.101', '内网IP', '{\"params\":{},\"reservationId\":13}', NULL, 1, 'nested exception is org.apache.ibatis.binding.BindingException: Parameter \'reservationId\' not found. Available parameters are [avatar, userName, param1, param2]', '2024-03-09 08:36:54', 4);
INSERT INTO `sys_oper_log` VALUES (173, '入座', 2, 'com.ruoyi.web.controller.system.LrReservationController.takePlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/takePlace', '172.24.87.101', '内网IP', '{\"params\":{},\"reservationId\":11}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 08:51:24', 38);
INSERT INTO `sys_oper_log` VALUES (174, '取消预约', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelReservation()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelReservation', '172.24.87.101', '内网IP', '{\"params\":{},\"reservationId\":13}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 09:01:47', 21);
INSERT INTO `sys_oper_log` VALUES (175, '取消预约', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelReservation()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelReservation', '172.24.87.101', '内网IP', '{\"params\":{},\"reservationId\":14}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 09:05:42', 18);
INSERT INTO `sys_oper_log` VALUES (176, '入座', 2, 'com.ruoyi.web.controller.system.LrReservationController.takePlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/takePlace', '172.24.87.101', '内网IP', '{\"params\":{},\"reservationId\":15}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 09:10:15', 37);
INSERT INTO `sys_oper_log` VALUES (177, '入座', 2, 'com.ruoyi.web.controller.system.LrReservationController.takePlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/takePlace', '172.24.87.101', '内网IP', '{\"params\":{},\"reservationId\":16}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 09:10:43', 17);
INSERT INTO `sys_oper_log` VALUES (178, '入座', 2, 'com.ruoyi.web.controller.system.LrReservationController.takePlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/takePlace', '172.24.87.101', '内网IP', '{\"params\":{},\"reservationId\":17}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 09:16:04', 27);
INSERT INTO `sys_oper_log` VALUES (179, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.resetPwd()', 'PUT', 1, 'admin', '研发部门', '/system/user/resetPwd', '127.0.0.1', '内网IP', '{\"admin\":false,\"params\":{},\"updateBy\":\"admin\",\"userId\":101}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 09:26:18', 76);
INSERT INTO `sys_oper_log` VALUES (180, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/user', '127.0.0.1', '内网IP', '{\"admin\":false,\"avatar\":\"\",\"createBy\":\"\",\"createTime\":\"2024-03-07 23:01:16\",\"delFlag\":\"0\",\"email\":\"archyqx@gmail.com\",\"loginIp\":\"\",\"nickName\":\"admin6\",\"params\":{},\"phonenumber\":\"18829539116\",\"postIds\":[],\"roleIds\":[],\"roles\":[],\"score\":10,\"sex\":\"0\",\"status\":\"0\",\"userId\":102,\"userName\":\"admin6\",\"userType\":\"00\"}', '{\"msg\":\"修改用户\'admin6\'失败，手机号码已存在\",\"code\":500}', 0, NULL, '2024-03-09 09:28:16', 5);
INSERT INTO `sys_oper_log` VALUES (181, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/user', '127.0.0.1', '内网IP', '{\"admin\":false,\"avatar\":\"\",\"createBy\":\"\",\"createTime\":\"2024-03-07 23:01:16\",\"delFlag\":\"0\",\"email\":\"archyqx@gmail.com\",\"loginIp\":\"\",\"nickName\":\"admin6\",\"params\":{},\"phonenumber\":\"18829539116\",\"postIds\":[],\"roleIds\":[],\"roles\":[],\"score\":10,\"sex\":\"0\",\"status\":\"0\",\"userId\":102,\"userName\":\"admin6\",\"userType\":\"00\"}', '{\"msg\":\"修改用户\'admin6\'失败，手机号码已存在\",\"code\":500}', 0, NULL, '2024-03-09 09:28:30', 5);
INSERT INTO `sys_oper_log` VALUES (182, '离开座位', 2, 'com.ruoyi.web.controller.system.LrReservationController.leaveSeat()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/leaveSeat', '172.24.87.101', '内网IP', '{\"params\":{}}', NULL, 1, '', '2024-03-09 09:35:35', 11);
INSERT INTO `sys_oper_log` VALUES (183, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelPlace', '172.24.87.101', '内网IP', '{\"params\":{}}', NULL, 1, '', '2024-03-09 09:36:04', 3);
INSERT INTO `sys_oper_log` VALUES (184, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelPlace', '172.24.87.101', '内网IP', '{\"params\":{}}', NULL, 1, '', '2024-03-09 09:38:03', 3);
INSERT INTO `sys_oper_log` VALUES (185, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelPlace', '172.24.87.101', '内网IP', '{\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 09:40:20', 42);
INSERT INTO `sys_oper_log` VALUES (186, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelPlace', '172.24.87.101', '内网IP', '{\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 09:41:11', 20);
INSERT INTO `sys_oper_log` VALUES (187, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelPlace', '172.24.87.101', '内网IP', '{\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 09:41:19', 23);
INSERT INTO `sys_oper_log` VALUES (188, '离开座位', 2, 'com.ruoyi.web.controller.system.LrReservationController.leaveSeat()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/leaveSeat', '172.24.87.101', '内网IP', '{\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 09:41:44', 22);
INSERT INTO `sys_oper_log` VALUES (189, '离开座位', 2, 'com.ruoyi.web.controller.system.LrReservationController.leaveSeat()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/leaveSeat', '172.24.87.101', '内网IP', '{\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 09:41:51', 14);
INSERT INTO `sys_oper_log` VALUES (190, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelPlace', '172.24.87.101', '内网IP', '{\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 09:44:47', 36);
INSERT INTO `sys_oper_log` VALUES (191, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelPlace', '172.24.87.101', '内网IP', '{\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 09:44:52', 23);
INSERT INTO `sys_oper_log` VALUES (192, '离开座位', 2, 'com.ruoyi.web.controller.system.LrReservationController.leaveSeat()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/leaveSeat', '172.24.87.101', '内网IP', '{\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 09:45:01', 16);
INSERT INTO `sys_oper_log` VALUES (193, '离开座位', 2, 'com.ruoyi.web.controller.system.LrReservationController.leaveSeat()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/leaveSeat', '172.24.87.101', '内网IP', '{\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 09:45:03', 27);
INSERT INTO `sys_oper_log` VALUES (194, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelPlace', '172.24.87.101', '内网IP', '{\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 09:46:21', 54);
INSERT INTO `sys_oper_log` VALUES (195, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelPlace', '172.24.87.101', '内网IP', '{\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 09:46:39', 24);
INSERT INTO `sys_oper_log` VALUES (196, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelPlace', '172.24.87.101', '内网IP', '{\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 09:46:40', 25);
INSERT INTO `sys_oper_log` VALUES (197, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelPlace', '172.24.87.101', '内网IP', '{\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 09:46:42', 23);
INSERT INTO `sys_oper_log` VALUES (198, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelPlace', '172.24.87.101', '内网IP', '{\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 09:46:43', 15);
INSERT INTO `sys_oper_log` VALUES (199, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelPlace', '172.24.87.101', '内网IP', '{\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 09:46:44', 16);
INSERT INTO `sys_oper_log` VALUES (200, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, 'admin', '研发部门', '/system/reservation/cancelPlace', '119.184.124.3', 'XX XX', '{\"params\":{}}', '{\"msg\":\"当前无法退座\",\"code\":500}', 0, NULL, '2024-03-09 09:48:46', 6233);
INSERT INTO `sys_oper_log` VALUES (201, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelPlace', '172.24.87.101', '内网IP', '{\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 09:49:38', 26);
INSERT INTO `sys_oper_log` VALUES (202, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelPlace', '172.24.87.101', '内网IP', '{\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 09:49:42', 21);
INSERT INTO `sys_oper_log` VALUES (203, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelPlace', '172.24.87.101', '内网IP', '{\"params\":{}}', '{\"msg\":\"当前无法退座\",\"code\":500}', 0, NULL, '2024-03-09 09:50:43', 19401);
INSERT INTO `sys_oper_log` VALUES (204, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelPlace', '172.24.87.101', '内网IP', '{\"params\":{}}', '{\"msg\":\"当前无法退座\",\"code\":500}', 0, NULL, '2024-03-09 09:50:43', 120);
INSERT INTO `sys_oper_log` VALUES (205, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelPlace', '172.24.87.101', '内网IP', '{\"params\":{}}', '{\"msg\":\"当前无法退座\",\"code\":500}', 0, NULL, '2024-03-09 09:50:58', 4);
INSERT INTO `sys_oper_log` VALUES (206, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelPlace', '172.24.87.101', '内网IP', '{\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 09:51:37', 36);
INSERT INTO `sys_oper_log` VALUES (207, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelPlace', '172.24.87.101', '内网IP', '{\"params\":{}}', '{\"msg\":\"当前无法退座\",\"code\":500}', 0, NULL, '2024-03-09 09:52:12', 0);
INSERT INTO `sys_oper_log` VALUES (208, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelPlace', '172.24.87.101', '内网IP', '{\"params\":{}}', '{\"msg\":\"当前无法退座\",\"code\":500}', 0, NULL, '2024-03-09 09:52:54', 0);
INSERT INTO `sys_oper_log` VALUES (209, '离开座位', 2, 'com.ruoyi.web.controller.system.LrReservationController.leaveSeat()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/leaveSeat', '172.24.87.101', '内网IP', '{\"params\":{}}', '{\"msg\":\"当前无法退座\",\"code\":500}', 0, NULL, '2024-03-09 09:52:58', 1);
INSERT INTO `sys_oper_log` VALUES (210, '离开座位', 2, 'com.ruoyi.web.controller.system.LrReservationController.leaveSeat()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/leaveSeat', '172.24.87.101', '内网IP', '{\"params\":{}}', '{\"msg\":\"当前无法退座\",\"code\":500}', 0, NULL, '2024-03-09 09:53:03', 0);
INSERT INTO `sys_oper_log` VALUES (211, '离开座位', 2, 'com.ruoyi.web.controller.system.LrReservationController.leaveSeat()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/leaveSeat', '172.24.87.101', '内网IP', '{\"params\":{}}', '{\"msg\":\"当前无法退座\",\"code\":500}', 0, NULL, '2024-03-09 09:53:10', 1);
INSERT INTO `sys_oper_log` VALUES (212, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/reservation', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-09 09:53:45\",\"params\":{},\"reservationId\":18,\"seatId\":3,\"studentId\":100}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 09:53:45', 19);
INSERT INTO `sys_oper_log` VALUES (213, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/reservation', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-09 09:53:57\",\"params\":{},\"reservationId\":19,\"seatId\":3,\"studentId\":100}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 09:53:57', 17);
INSERT INTO `sys_oper_log` VALUES (214, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/reservation', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-09 10:27:03\",\"params\":{},\"reservationDate\":\"2024-03-10\",\"reservationId\":20,\"seatId\":6,\"studentId\":100,\"timeSlot\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 10:27:03', 8);
INSERT INTO `sys_oper_log` VALUES (215, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/reservation', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-09 10:27:13\",\"params\":{},\"reservationDate\":\"2024-03-10\",\"reservationId\":21,\"seatId\":3,\"studentId\":100,\"timeSlot\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 10:27:13', 7);
INSERT INTO `sys_oper_log` VALUES (216, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/reservation', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-09 10:27:24\",\"params\":{},\"reservationDate\":\"2024-03-10\",\"reservationId\":22,\"seatId\":5,\"studentId\":100,\"timeSlot\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 10:27:24', 8);
INSERT INTO `sys_oper_log` VALUES (217, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/reservation', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-09 10:27:30\",\"params\":{},\"reservationDate\":\"2024-03-10\",\"reservationId\":23,\"seatId\":3,\"studentId\":100,\"timeSlot\":3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 10:27:30', 18);
INSERT INTO `sys_oper_log` VALUES (218, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.resetPwd()', 'PUT', 1, '2024030705', '若依科技', '/system/user/resetPwd', '172.24.87.101', '内网IP', '{\"admin\":false,\"params\":{},\"updateBy\":\"2024030705\"}', '{\"msg\":\"操作失败\",\"code\":500}', 0, NULL, '2024-03-09 10:55:24', 92);
INSERT INTO `sys_oper_log` VALUES (219, '用户管理', 3, 'com.ruoyi.web.controller.system.SysUserController.remove()', 'DELETE', 1, '2024030705', '若依科技', '/system/user/106', '172.24.87.101', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 11:00:24', 31);
INSERT INTO `sys_oper_log` VALUES (220, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.resetPwd()', 'PUT', 1, '2024030705', '若依科技', '/system/user/resetPwd', '172.24.87.101', '内网IP', '{\"admin\":false,\"params\":{},\"updateBy\":\"2024030705\",\"userId\":105}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 11:00:33', 97);
INSERT INTO `sys_oper_log` VALUES (221, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/reservation', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-09 11:25:13\",\"params\":{},\"studentId\":100}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'seat_id\' doesn\'t have a default value\r\n### The error may exist in file [D:\\project\\code\\libraryReservation\\Server\\RuoYi-Vue-v3.8.7\\ruoyi-system\\target\\classes\\mapper\\system\\LrReservationMapper.xml]\r\n### The error may involve com.ruoyi.system.mapper.LrReservationMapper.insertLrReservation-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into lr_reservation          ( student_id,                                                                                           create_time )           values ( ?,                                                                                           ? )\r\n### Cause: java.sql.SQLException: Field \'seat_id\' doesn\'t have a default value\n; Field \'seat_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'seat_id\' doesn\'t have a default value', '2024-03-09 11:25:13', 69);
INSERT INTO `sys_oper_log` VALUES (222, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, '2024030705', '若依科技', '/system/seat', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-09 11:26:17\",\"params\":{},\"seatId\":35,\"seatLocation\":\"自习室A区\",\"seatNumber\":\"123456\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 11:26:17', 19);
INSERT INTO `sys_oper_log` VALUES (223, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/reservation', '172.24.87.199', '内网IP', '{\"createTime\":\"2024-03-09 13:45:26\",\"params\":{},\"reservationDate\":\"2024-03-10\",\"reservationId\":24,\"seatId\":6,\"studentId\":100,\"timeSlot\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 13:45:26', 13);
INSERT INTO `sys_oper_log` VALUES (224, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelPlace', '172.24.87.199', '内网IP', '{\"params\":{}}', '{\"msg\":\"当前无法退座\",\"code\":500}', 0, NULL, '2024-03-09 13:46:03', 2);
INSERT INTO `sys_oper_log` VALUES (225, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/reservation', '172.24.87.199', '内网IP', '{\"createTime\":\"2024-03-09 13:46:37\",\"params\":{},\"reservationDate\":\"2024-03-10\",\"reservationId\":25,\"seatId\":7,\"studentId\":100,\"timeSlot\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 13:46:37', 17);
INSERT INTO `sys_oper_log` VALUES (226, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelPlace', '172.24.87.199', '内网IP', '{\"params\":{}}', '{\"msg\":\"当前无法退座\",\"code\":500}', 0, NULL, '2024-03-09 13:47:30', 0);
INSERT INTO `sys_oper_log` VALUES (227, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelPlace', '172.24.87.199', '内网IP', '{\"params\":{}}', '{\"msg\":\"当前无法退座\",\"code\":500}', 0, NULL, '2024-03-09 13:47:31', 0);
INSERT INTO `sys_oper_log` VALUES (228, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelPlace', '172.24.87.199', '内网IP', '{\"params\":{}}', '{\"msg\":\"当前无法退座\",\"code\":500}', 0, NULL, '2024-03-09 13:47:32', 1);
INSERT INTO `sys_oper_log` VALUES (229, '入座', 2, 'com.ruoyi.web.controller.system.LrReservationController.takePlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/takePlace', '172.24.87.101', '内网IP', '{\"params\":{},\"reservationId\":9}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 16:22:50', 21);
INSERT INTO `sys_oper_log` VALUES (230, '取消预约', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelReservation()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelReservation', '172.24.87.101', '内网IP', '{\"params\":{},\"reservationId\":18}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 16:30:08', 18);
INSERT INTO `sys_oper_log` VALUES (231, '取消预约', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelReservation()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelReservation', '172.24.87.101', '内网IP', '{\"params\":{},\"reservationId\":19}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 16:30:28', 24);
INSERT INTO `sys_oper_log` VALUES (232, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/reservation', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-09 16:30:41\",\"params\":{},\"reservationDate\":\"2024-03-10\",\"reservationId\":26,\"seatId\":8,\"studentId\":100,\"timeSlot\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 16:30:41', 15);
INSERT INTO `sys_oper_log` VALUES (233, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/reservation', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-09 16:30:45\",\"params\":{},\"reservationDate\":\"2024-03-10\",\"reservationId\":27,\"seatId\":9,\"studentId\":100,\"timeSlot\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 16:30:45', 15);
INSERT INTO `sys_oper_log` VALUES (234, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelPlace', '172.24.87.101', '内网IP', '{\"params\":{}}', '{\"msg\":\"当前无法退座\",\"code\":500}', 0, NULL, '2024-03-09 16:33:49', 0);
INSERT INTO `sys_oper_log` VALUES (235, '离开座位', 2, 'com.ruoyi.web.controller.system.LrReservationController.leaveSeat()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/leaveSeat', '172.24.87.101', '内网IP', '{\"params\":{}}', '{\"msg\":\"当前无法退座\",\"code\":500}', 0, NULL, '2024-03-09 16:34:00', 0);
INSERT INTO `sys_oper_log` VALUES (236, '取消预约', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelReservation()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelReservation', '172.24.87.101', '内网IP', '{\"params\":{},\"reservationId\":17}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 16:34:33', 18);
INSERT INTO `sys_oper_log` VALUES (237, '取消预约', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelReservation()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/cancelReservation', '172.24.87.101', '内网IP', '{\"params\":{},\"reservationId\":20}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 16:34:37', 18);
INSERT INTO `sys_oper_log` VALUES (238, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/reservation', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-09 16:34:47\",\"params\":{},\"reservationDate\":\"2024-03-10\",\"reservationId\":28,\"seatId\":10,\"studentId\":100,\"timeSlot\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 16:34:47', 9);
INSERT INTO `sys_oper_log` VALUES (239, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/reservation', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-09 16:35:16\",\"params\":{},\"reservationDate\":\"2024-03-10\",\"reservationId\":29,\"seatId\":11,\"studentId\":100,\"timeSlot\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 16:35:16', 8);
INSERT INTO `sys_oper_log` VALUES (240, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/reservation', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-09 16:42:49\",\"params\":{},\"reservationDate\":\"2024-03-10\",\"reservationId\":30,\"seatId\":12,\"studentId\":100,\"timeSlot\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 16:42:49', 17);
INSERT INTO `sys_oper_log` VALUES (241, '座位管理', 3, 'com.ruoyi.web.controller.system.LrSeatController.remove()', 'DELETE', 1, '2024030705', '若依科技', '/system/seat/35', '172.24.87.101', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 16:55:29', 25);
INSERT INTO `sys_oper_log` VALUES (242, '座位管理', 2, 'com.ruoyi.web.controller.system.LrSeatController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/seat', '172.24.87.101', '内网IP', '{\"createBy\":\"\",\"createTime\":\"2024-03-07 20:34:51\",\"delFlag\":\"03\",\"params\":{},\"remark\":\"12\",\"seatId\":3,\"seatLocation\":\"自习室A区\",\"seatNumber\":\"01\",\"seatStatus\":\"0\",\"updateBy\":\"\",\"updateTime\":\"2024-03-09 16:57:58\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 16:57:58', 16);
INSERT INTO `sys_oper_log` VALUES (243, '座位管理', 2, 'com.ruoyi.web.controller.system.LrSeatController.edit()', 'PUT', 1, 'admin', '研发部门', '/system/seat', '127.0.0.1', '内网IP', '{\"createBy\":\"\",\"createTime\":\"2024-03-07 20:34:51\",\"delFlag\":\"0\",\"params\":{},\"remark\":\"121\",\"seatId\":3,\"seatLocation\":\"自习室A区\",\"seatNumber\":\"01\",\"seatStatus\":\"0\",\"updateBy\":\"\",\"updateTime\":\"2024-03-09 17:05:30\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 17:05:30', 24);
INSERT INTO `sys_oper_log` VALUES (244, '用户管理', 3, 'com.ruoyi.web.controller.system.SysUserController.remove()', 'DELETE', 1, '2024030705', '若依科技', '/system/user/107', '172.24.87.101', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 17:06:30', 35);
INSERT INTO `sys_oper_log` VALUES (245, '用户管理', 3, 'com.ruoyi.web.controller.system.SysUserController.remove()', 'DELETE', 1, '2024030705', '若依科技', '/system/user/105', '172.24.87.101', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 17:06:32', 11);
INSERT INTO `sys_oper_log` VALUES (246, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '666666', NULL, '/system/reservation/reservation', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-09 17:07:40\",\"params\":{},\"reservationDate\":\"2024-03-10\",\"reservationId\":31,\"seatId\":11,\"studentId\":108,\"timeSlot\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 17:07:40', 13);
INSERT INTO `sys_oper_log` VALUES (247, '取消预约', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelReservation()', 'POST', 1, '666666', NULL, '/system/reservation/cancelReservation', '172.24.87.101', '内网IP', '{\"params\":{},\"reservationId\":31}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 17:07:49', 19);
INSERT INTO `sys_oper_log` VALUES (248, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '666666', NULL, '/system/reservation/reservation', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-09 17:08:33\",\"params\":{},\"reservationDate\":\"2024-03-10\",\"reservationId\":32,\"seatId\":12,\"studentId\":108,\"timeSlot\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 17:08:33', 15);
INSERT INTO `sys_oper_log` VALUES (249, '取消预约', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelReservation()', 'POST', 1, '666666', NULL, '/system/reservation/cancelReservation', '172.24.87.101', '内网IP', '{\"params\":{},\"reservationId\":32}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 17:08:41', 21);
INSERT INTO `sys_oper_log` VALUES (250, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '666666', NULL, '/system/reservation/reservation', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-09 17:12:04\",\"params\":{},\"reservationDate\":\"2024-03-10\",\"reservationId\":33,\"seatId\":13,\"studentId\":108,\"timeSlot\":2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 17:12:04', 17);
INSERT INTO `sys_oper_log` VALUES (251, '取消预约', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelReservation()', 'POST', 1, '666666', NULL, '/system/reservation/cancelReservation', '172.24.87.101', '内网IP', '{\"params\":{},\"reservationId\":33}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 17:12:10', 11);
INSERT INTO `sys_oper_log` VALUES (252, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '666666', NULL, '/system/reservation/reservation', '172.24.87.101', '内网IP', '{\"createTime\":\"2024-03-09 17:24:18\",\"params\":{},\"reservationDate\":\"2024-03-10\",\"reservationId\":34,\"seatId\":13,\"studentId\":108,\"timeSlot\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-09 17:24:18', 8);
INSERT INTO `sys_oper_log` VALUES (253, '返回座位', 2, 'com.ruoyi.web.controller.system.LrReservationController.returnSeat()', 'POST', 1, '2024030705', '若依科技', '/system/reservation/returnSeat', '172.24.87.235', '内网IP', '{\"params\":{},\"seatId\":5}', NULL, 1, '', '2024-03-10 14:43:12', 3);
INSERT INTO `sys_oper_log` VALUES (254, '返回座位', 2, 'com.ruoyi.web.controller.system.LrReservationController.returnSeat()', 'POST', 1, '666666', NULL, '/system/reservation/returnSeat', '172.24.87.235', '内网IP', '{\"params\":{},\"seatId\":13}', NULL, 1, '', '2024-03-10 14:57:53', 9);
INSERT INTO `sys_oper_log` VALUES (255, '返回座位', 2, 'com.ruoyi.web.controller.system.LrReservationController.returnSeat()', 'POST', 1, '666666', NULL, '/system/reservation/returnSeat', '172.24.87.235', '内网IP', '{\"params\":{},\"seatId\":13}', NULL, 1, '', '2024-03-10 14:58:02', 4);
INSERT INTO `sys_oper_log` VALUES (256, '座位管理', 3, 'com.ruoyi.web.controller.system.LrSeatController.remove()', 'DELETE', 1, '666666', NULL, '/system/seat/16', '172.24.87.235', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:00:44', 19);
INSERT INTO `sys_oper_log` VALUES (257, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, '666666', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-10 15:00:54\",\"params\":{},\"seatId\":36,\"seatLocation\":\"自习室A区\",\"seatNumber\":\"13\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:00:54', 22);
INSERT INTO `sys_oper_log` VALUES (258, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, '666666', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-10 15:01:28\",\"params\":{},\"seatId\":37,\"seatLocation\":\"自习室A区\",\"seatNumber\":\"15\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:01:28', 19);
INSERT INTO `sys_oper_log` VALUES (259, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, '666666', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-10 15:01:47\",\"params\":{},\"seatId\":38,\"seatLocation\":\"自习室A区\",\"seatNumber\":\"16\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:01:47', 15);
INSERT INTO `sys_oper_log` VALUES (260, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, '666666', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-10 15:01:55\",\"params\":{},\"seatId\":39,\"seatLocation\":\"自习室B区\",\"seatNumber\":\"03\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:01:55', 17);
INSERT INTO `sys_oper_log` VALUES (261, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, '666666', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-10 15:01:58\",\"params\":{},\"seatId\":40,\"seatLocation\":\"自习室B区\",\"seatNumber\":\"04\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:01:58', 7);
INSERT INTO `sys_oper_log` VALUES (262, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, '666666', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-10 15:02:01\",\"params\":{},\"seatId\":41,\"seatLocation\":\"自习室B区\",\"seatNumber\":\"05\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:02:01', 8);
INSERT INTO `sys_oper_log` VALUES (263, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, '666666', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-10 15:02:04\",\"params\":{},\"seatId\":42,\"seatLocation\":\"自习室B区\",\"seatNumber\":\"06\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:02:04', 7);
INSERT INTO `sys_oper_log` VALUES (264, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, '666666', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-10 15:02:09\",\"params\":{},\"seatId\":43,\"seatLocation\":\"自习室B区\",\"seatNumber\":\"09\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:02:10', 16);
INSERT INTO `sys_oper_log` VALUES (265, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, '666666', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-10 15:02:16\",\"params\":{},\"seatId\":44,\"seatLocation\":\"自习室C区\",\"seatNumber\":\"test\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:02:16', 17);
INSERT INTO `sys_oper_log` VALUES (266, '座位管理', 1, 'com.ruoyi.web.controller.system.LrSeatController.add()', 'POST', 1, '666666', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-10 15:02:22\",\"params\":{},\"seatId\":45,\"seatLocation\":\"自习室C区\",\"seatNumber\":\"test1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:02:22', 17);
INSERT INTO `sys_oper_log` VALUES (267, '返回座位', 2, 'com.ruoyi.web.controller.system.LrReservationController.returnSeat()', 'POST', 1, '666666', NULL, '/system/reservation/returnSeat', '172.24.87.235', '内网IP', '{\"params\":{},\"seatId\":13}', NULL, 1, '', '2024-03-10 15:05:32', 18);
INSERT INTO `sys_oper_log` VALUES (268, '返回座位', 2, 'com.ruoyi.web.controller.system.LrReservationController.returnSeat()', 'POST', 1, '666666', NULL, '/system/reservation/returnSeat', '172.24.87.235', '内网IP', '{\"params\":{},\"seatId\":13}', NULL, 1, '', '2024-03-10 15:09:04', 3);
INSERT INTO `sys_oper_log` VALUES (269, '返回座位', 2, 'com.ruoyi.web.controller.system.LrReservationController.returnSeat()', 'POST', 1, '666666', NULL, '/system/reservation/returnSeat', '172.24.87.235', '内网IP', '{\"params\":{},\"seatId\":13}', NULL, 1, '', '2024-03-10 15:09:45', 5);
INSERT INTO `sys_oper_log` VALUES (270, '入座', 2, 'com.ruoyi.web.controller.system.LrReservationController.takePlace()', 'POST', 1, '666666', NULL, '/system/reservation/takePlace', '172.24.87.235', '内网IP', '{\"params\":{},\"reservationId\":34}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:10:37', 28);
INSERT INTO `sys_oper_log` VALUES (271, '座位管理', 2, 'com.ruoyi.web.controller.system.LrSeatController.edit()', 'PUT', 1, '666666', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"params\":{},\"seatId\":3,\"seatLocation\":\"自习室A区\",\"seatNumber\":\"01\",\"updateTime\":\"2024-03-10 15:22:42\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:22:42', 19);
INSERT INTO `sys_oper_log` VALUES (272, '座位管理', 2, 'com.ruoyi.web.controller.system.LrSeatController.edit()', 'PUT', 1, '666666', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"params\":{},\"seatId\":3,\"seatLocation\":\"自习室A区\",\"seatNumber\":\"01\",\"seatStatus\":\"1\",\"updateTime\":\"2024-03-10 15:23:17\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:23:17', 18);
INSERT INTO `sys_oper_log` VALUES (273, '座位管理', 2, 'com.ruoyi.web.controller.system.LrSeatController.edit()', 'PUT', 1, '666666', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"params\":{},\"seatId\":3,\"seatLocation\":\"自习室A区\",\"seatNumber\":\"01\",\"seatStatus\":\"0\",\"updateTime\":\"2024-03-10 15:23:21\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:23:21', 14);
INSERT INTO `sys_oper_log` VALUES (274, '座位管理', 2, 'com.ruoyi.web.controller.system.LrSeatController.edit()', 'PUT', 1, '666666', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"params\":{},\"seatId\":3,\"seatLocation\":\"自习室A区\",\"seatNumber\":\"01\",\"seatStatus\":\"1\",\"updateTime\":\"2024-03-10 15:23:22\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:23:22', 16);
INSERT INTO `sys_oper_log` VALUES (275, '座位管理', 2, 'com.ruoyi.web.controller.system.LrSeatController.edit()', 'PUT', 1, '666666', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"params\":{},\"seatId\":3,\"seatLocation\":\"自习室A区\",\"seatNumber\":\"01\",\"seatStatus\":\"0\",\"updateTime\":\"2024-03-10 15:23:23\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:23:23', 15);
INSERT INTO `sys_oper_log` VALUES (276, '座位管理', 2, 'com.ruoyi.web.controller.system.LrSeatController.edit()', 'PUT', 1, '666666', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"params\":{},\"seatId\":3,\"seatLocation\":\"自习室A区\",\"seatNumber\":\"01\",\"seatStatus\":\"1\",\"updateTime\":\"2024-03-10 15:23:23\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:23:23', 21);
INSERT INTO `sys_oper_log` VALUES (277, '座位管理', 2, 'com.ruoyi.web.controller.system.LrSeatController.edit()', 'PUT', 1, '666666', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"params\":{},\"seatId\":3,\"seatLocation\":\"自习室A区\",\"seatNumber\":\"01\",\"seatStatus\":\"0\",\"updateTime\":\"2024-03-10 15:23:24\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:23:24', 8);
INSERT INTO `sys_oper_log` VALUES (278, '座位管理', 2, 'com.ruoyi.web.controller.system.LrSeatController.edit()', 'PUT', 1, '666666', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"params\":{},\"seatId\":3,\"seatLocation\":\"自习室A区\",\"seatNumber\":\"01\",\"seatStatus\":\"1\",\"updateTime\":\"2024-03-10 15:23:24\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:23:24', 7);
INSERT INTO `sys_oper_log` VALUES (279, '座位管理', 2, 'com.ruoyi.web.controller.system.LrSeatController.edit()', 'PUT', 1, '666666', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"params\":{},\"seatId\":3,\"seatLocation\":\"自习室A区\",\"seatNumber\":\"01\",\"seatStatus\":\"0\",\"updateTime\":\"2024-03-10 15:23:25\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:23:25', 15);
INSERT INTO `sys_oper_log` VALUES (280, '座位管理', 2, 'com.ruoyi.web.controller.system.LrSeatController.edit()', 'PUT', 1, '666666', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"params\":{},\"seatId\":5,\"seatLocation\":\"自习室A区\",\"seatNumber\":\"02\",\"seatStatus\":\"1\",\"updateTime\":\"2024-03-10 15:23:26\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:23:26', 8);
INSERT INTO `sys_oper_log` VALUES (281, '座位管理', 2, 'com.ruoyi.web.controller.system.LrSeatController.edit()', 'PUT', 1, '666666', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"params\":{},\"seatId\":5,\"seatLocation\":\"自习室A区\",\"seatNumber\":\"02\",\"seatStatus\":\"0\",\"updateTime\":\"2024-03-10 15:23:27\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:23:27', 18);
INSERT INTO `sys_oper_log` VALUES (282, '座位管理', 2, 'com.ruoyi.web.controller.system.LrSeatController.edit()', 'PUT', 1, '666666', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"params\":{},\"seatId\":39,\"seatLocation\":\"自习室B区\",\"seatNumber\":\"03\",\"seatStatus\":\"1\",\"updateTime\":\"2024-03-10 15:23:33\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:23:33', 16);
INSERT INTO `sys_oper_log` VALUES (283, '座位管理', 2, 'com.ruoyi.web.controller.system.LrSeatController.edit()', 'PUT', 1, '666666', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"params\":{},\"seatId\":39,\"seatLocation\":\"自习室B区\",\"seatNumber\":\"03\",\"seatStatus\":\"0\",\"updateTime\":\"2024-03-10 15:23:33\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:23:33', 15);
INSERT INTO `sys_oper_log` VALUES (284, '座位管理', 2, 'com.ruoyi.web.controller.system.LrSeatController.edit()', 'PUT', 1, '666666', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"params\":{},\"seatId\":39,\"seatLocation\":\"自习室B区\",\"seatNumber\":\"03\",\"seatStatus\":\"1\",\"updateTime\":\"2024-03-10 15:23:34\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:23:34', 7);
INSERT INTO `sys_oper_log` VALUES (285, '座位管理', 2, 'com.ruoyi.web.controller.system.LrSeatController.edit()', 'PUT', 1, '666666', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"params\":{},\"seatId\":39,\"seatLocation\":\"自习室B区\",\"seatNumber\":\"03\",\"seatStatus\":\"0\",\"updateTime\":\"2024-03-10 15:23:34\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:23:34', 8);
INSERT INTO `sys_oper_log` VALUES (286, '入座', 2, 'com.ruoyi.web.controller.system.LrReservationController.takePlace()', 'POST', 1, '666666', NULL, '/system/reservation/takePlace', '172.24.87.235', '内网IP', '{\"params\":{},\"reservationId\":32}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:28:39', 25);
INSERT INTO `sys_oper_log` VALUES (287, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '666666', NULL, '/system/reservation/cancelPlace', '172.24.87.235', '内网IP', '{\"params\":{}}', '{\"msg\":\"当前无法退座\",\"code\":500}', 0, NULL, '2024-03-10 15:30:41', 1);
INSERT INTO `sys_oper_log` VALUES (288, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '666666', NULL, '/system/reservation/cancelPlace', '172.24.87.235', '内网IP', '{\"params\":{}}', '{\"msg\":\"当前无法退座\",\"code\":500}', 0, NULL, '2024-03-10 15:34:00', 28708);
INSERT INTO `sys_oper_log` VALUES (289, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '666666', NULL, '/system/reservation/cancelPlace', '172.24.87.235', '内网IP', '{\"params\":{}}', '{\"msg\":\"当前无法退座\",\"code\":500}', 0, NULL, '2024-03-10 15:35:17', 49738);
INSERT INTO `sys_oper_log` VALUES (290, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '666666', NULL, '/system/reservation/cancelPlace', '172.24.87.235', '内网IP', '{\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:39:04', 50);
INSERT INTO `sys_oper_log` VALUES (291, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '666666', NULL, '/system/reservation/cancelPlace', '172.24.87.235', '内网IP', '{\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:39:10', 38);
INSERT INTO `sys_oper_log` VALUES (292, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '666666', NULL, '/system/reservation/cancelPlace', '172.24.87.235', '内网IP', '{\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:39:16', 24);
INSERT INTO `sys_oper_log` VALUES (293, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '666666', NULL, '/system/reservation/cancelPlace', '172.24.87.235', '内网IP', '{\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:39:32', 10);
INSERT INTO `sys_oper_log` VALUES (294, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '666666', NULL, '/system/reservation/cancelPlace', '172.24.87.235', '内网IP', '{\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:40:35', 29509);
INSERT INTO `sys_oper_log` VALUES (295, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '666666', NULL, '/system/reservation/cancelPlace', '172.24.87.235', '内网IP', '{\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:50:41', 36);
INSERT INTO `sys_oper_log` VALUES (296, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '666666', NULL, '/system/reservation/cancelPlace', '172.24.87.235', '内网IP', '{\"params\":{}}', '{\"msg\":\"当前无法退座\",\"code\":500}', 0, NULL, '2024-03-10 15:50:44', 0);
INSERT INTO `sys_oper_log` VALUES (297, '入座', 2, 'com.ruoyi.web.controller.system.LrReservationController.takePlace()', 'POST', 1, '666666', NULL, '/system/reservation/takePlace', '172.24.87.235', '内网IP', '{\"params\":{},\"reservationId\":33}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:51:04', 26);
INSERT INTO `sys_oper_log` VALUES (298, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '666666', NULL, '/system/reservation/cancelPlace', '172.24.87.235', '内网IP', '{\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:51:07', 19);
INSERT INTO `sys_oper_log` VALUES (299, '退座', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelPlace()', 'POST', 1, '666666', NULL, '/system/reservation/cancelPlace', '172.24.87.235', '内网IP', '{\"params\":{}}', '{\"msg\":\"当前无法退座\",\"code\":500}', 0, NULL, '2024-03-10 15:51:10', 0);
INSERT INTO `sys_oper_log` VALUES (300, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '666666', NULL, '/system/reservation/reservation', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-10 15:59:28\",\"params\":{},\"reservationDate\":\"2024-03-11\",\"reservationId\":35,\"seatId\":3,\"studentId\":108,\"timeSlot\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:59:28', 17);
INSERT INTO `sys_oper_log` VALUES (301, '座位预约', 1, 'com.ruoyi.web.controller.system.LrReservationController.add()', 'POST', 1, '666666', NULL, '/system/reservation/reservation', '172.24.87.235', '内网IP', '{\"createTime\":\"2024-03-10 15:59:31\",\"params\":{},\"reservationDate\":\"2024-03-11\",\"reservationId\":36,\"seatId\":5,\"studentId\":108,\"timeSlot\":1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 15:59:31', 15);
INSERT INTO `sys_oper_log` VALUES (302, '取消预约', 2, 'com.ruoyi.web.controller.system.LrReservationController.cancelReservation()', 'POST', 1, '666666', NULL, '/system/reservation/cancelReservation', '172.24.87.235', '内网IP', '{\"params\":{},\"reservationId\":35}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 16:01:44', 18);
INSERT INTO `sys_oper_log` VALUES (303, '座位管理', 2, 'com.ruoyi.web.controller.system.LrSeatController.edit()', 'PUT', 1, '666666', NULL, '/system/seat', '172.24.87.235', '内网IP', '{\"params\":{},\"seatId\":6,\"seatLocation\":\"自习室A区\",\"seatNumber\":\"05\",\"seatStatus\":\"1\",\"updateTime\":\"2024-03-10 16:03:26\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 16:03:26', 19);
INSERT INTO `sys_oper_log` VALUES (304, '离开座位', 2, 'com.ruoyi.web.controller.system.LrReservationController.leaveSeat()', 'POST', 1, '666666', NULL, '/system/reservation/leaveSeat', '172.24.87.235', '内网IP', '{\"params\":{}}', '{\"msg\":\"当前无法离座\",\"code\":500}', 0, NULL, '2024-03-10 16:07:32', 0);
INSERT INTO `sys_oper_log` VALUES (305, '通知公告', 1, 'com.ruoyi.web.controller.system.SysNoticeController.add()', 'POST', 1, 'admin', '研发部门', '/system/notice/', '172.24.87.235', '内网IP', '{\"createBy\":\"admin\",\"noticeContent\":\"456\",\"noticeTitle\":\"123\",\"noticeType\":\"2\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 16:10:38', 18);
INSERT INTO `sys_oper_log` VALUES (306, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.changeStatus()', 'PUT', 1, 'admin', '研发部门', '/monitor/job/changeStatus', '127.0.0.1', '内网IP', '{\"jobId\":1,\"misfirePolicy\":\"0\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 16:13:36', 34);
INSERT INTO `sys_oper_log` VALUES (307, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.edit()', 'PUT', 1, 'admin', '研发部门', '/monitor/job', '127.0.0.1', '内网IP', '{\"concurrent\":\"1\",\"createBy\":\"admin\",\"createTime\":\"2024-03-07 20:14:38\",\"cronExpression\":\"0 0 0,6,12,18 * * ? \",\"invokeTarget\":\"ryTask.ryNoParams\",\"jobGroup\":\"DEFAULT\",\"jobId\":1,\"jobName\":\"系统默认（无参）\",\"misfirePolicy\":\"3\",\"nextValidTime\":\"2024-03-10 18:00:00\",\"params\":{},\"remark\":\"\",\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 16:13:41', 9);
INSERT INTO `sys_oper_log` VALUES (308, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.edit()', 'PUT', 1, 'admin', '研发部门', '/monitor/job', '127.0.0.1', '内网IP', '{\"concurrent\":\"1\",\"createBy\":\"admin\",\"createTime\":\"2024-03-07 20:14:38\",\"cronExpression\":\"0 0 0 * * ? \",\"invokeTarget\":\"ryTask.ryNoParams\",\"jobGroup\":\"DEFAULT\",\"jobId\":1,\"jobName\":\"系统默认（无参）\",\"misfirePolicy\":\"3\",\"nextValidTime\":\"2024-03-11 00:00:00\",\"params\":{},\"remark\":\"\",\"status\":\"0\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 16:13:51', 19);
INSERT INTO `sys_oper_log` VALUES (309, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.edit()', 'PUT', 1, 'admin', '研发部门', '/monitor/job', '127.0.0.1', '内网IP', '{\"concurrent\":\"1\",\"createBy\":\"admin\",\"createTime\":\"2024-03-07 20:14:38\",\"cronExpression\":\"0 0 0,6,12,18 * * ? \",\"invokeTarget\":\"ryTask.ryParams(\'ry\')\",\"jobGroup\":\"DEFAULT\",\"jobId\":2,\"jobName\":\"系统默认（有参）\",\"misfirePolicy\":\"3\",\"nextValidTime\":\"2024-03-10 18:00:00\",\"params\":{},\"remark\":\"\",\"status\":\"1\",\"updateBy\":\"admin\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 16:13:58', 9);
INSERT INTO `sys_oper_log` VALUES (310, '定时任务', 2, 'com.ruoyi.quartz.controller.SysJobController.changeStatus()', 'PUT', 1, 'admin', '研发部门', '/monitor/job/changeStatus', '127.0.0.1', '内网IP', '{\"jobId\":2,\"misfirePolicy\":\"0\",\"params\":{},\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2024-03-10 16:14:03', 22);

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2024-03-07 20:14:37', '', NULL, '');
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2024-03-07 20:14:37', '', NULL, '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', 1, 1, '0', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '超级管理员');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 2, '2', 1, 1, '0', '0', 'admin', '2024-03-07 20:14:37', '', NULL, '普通角色');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 100);
INSERT INTO `sys_role_dept` VALUES (2, 101);
INSERT INTO `sys_role_dept` VALUES (2, 105);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 3);
INSERT INTO `sys_role_menu` VALUES (2, 4);
INSERT INTO `sys_role_menu` VALUES (2, 100);
INSERT INTO `sys_role_menu` VALUES (2, 101);
INSERT INTO `sys_role_menu` VALUES (2, 102);
INSERT INTO `sys_role_menu` VALUES (2, 103);
INSERT INTO `sys_role_menu` VALUES (2, 104);
INSERT INTO `sys_role_menu` VALUES (2, 105);
INSERT INTO `sys_role_menu` VALUES (2, 106);
INSERT INTO `sys_role_menu` VALUES (2, 107);
INSERT INTO `sys_role_menu` VALUES (2, 108);
INSERT INTO `sys_role_menu` VALUES (2, 109);
INSERT INTO `sys_role_menu` VALUES (2, 110);
INSERT INTO `sys_role_menu` VALUES (2, 111);
INSERT INTO `sys_role_menu` VALUES (2, 112);
INSERT INTO `sys_role_menu` VALUES (2, 113);
INSERT INTO `sys_role_menu` VALUES (2, 114);
INSERT INTO `sys_role_menu` VALUES (2, 115);
INSERT INTO `sys_role_menu` VALUES (2, 116);
INSERT INTO `sys_role_menu` VALUES (2, 117);
INSERT INTO `sys_role_menu` VALUES (2, 500);
INSERT INTO `sys_role_menu` VALUES (2, 501);
INSERT INTO `sys_role_menu` VALUES (2, 1000);
INSERT INTO `sys_role_menu` VALUES (2, 1001);
INSERT INTO `sys_role_menu` VALUES (2, 1002);
INSERT INTO `sys_role_menu` VALUES (2, 1003);
INSERT INTO `sys_role_menu` VALUES (2, 1004);
INSERT INTO `sys_role_menu` VALUES (2, 1005);
INSERT INTO `sys_role_menu` VALUES (2, 1006);
INSERT INTO `sys_role_menu` VALUES (2, 1007);
INSERT INTO `sys_role_menu` VALUES (2, 1008);
INSERT INTO `sys_role_menu` VALUES (2, 1009);
INSERT INTO `sys_role_menu` VALUES (2, 1010);
INSERT INTO `sys_role_menu` VALUES (2, 1011);
INSERT INTO `sys_role_menu` VALUES (2, 1012);
INSERT INTO `sys_role_menu` VALUES (2, 1013);
INSERT INTO `sys_role_menu` VALUES (2, 1014);
INSERT INTO `sys_role_menu` VALUES (2, 1015);
INSERT INTO `sys_role_menu` VALUES (2, 1016);
INSERT INTO `sys_role_menu` VALUES (2, 1017);
INSERT INTO `sys_role_menu` VALUES (2, 1018);
INSERT INTO `sys_role_menu` VALUES (2, 1019);
INSERT INTO `sys_role_menu` VALUES (2, 1020);
INSERT INTO `sys_role_menu` VALUES (2, 1021);
INSERT INTO `sys_role_menu` VALUES (2, 1022);
INSERT INTO `sys_role_menu` VALUES (2, 1023);
INSERT INTO `sys_role_menu` VALUES (2, 1024);
INSERT INTO `sys_role_menu` VALUES (2, 1025);
INSERT INTO `sys_role_menu` VALUES (2, 1026);
INSERT INTO `sys_role_menu` VALUES (2, 1027);
INSERT INTO `sys_role_menu` VALUES (2, 1028);
INSERT INTO `sys_role_menu` VALUES (2, 1029);
INSERT INTO `sys_role_menu` VALUES (2, 1030);
INSERT INTO `sys_role_menu` VALUES (2, 1031);
INSERT INTO `sys_role_menu` VALUES (2, 1032);
INSERT INTO `sys_role_menu` VALUES (2, 1033);
INSERT INTO `sys_role_menu` VALUES (2, 1034);
INSERT INTO `sys_role_menu` VALUES (2, 1035);
INSERT INTO `sys_role_menu` VALUES (2, 1036);
INSERT INTO `sys_role_menu` VALUES (2, 1037);
INSERT INTO `sys_role_menu` VALUES (2, 1038);
INSERT INTO `sys_role_menu` VALUES (2, 1039);
INSERT INTO `sys_role_menu` VALUES (2, 1040);
INSERT INTO `sys_role_menu` VALUES (2, 1041);
INSERT INTO `sys_role_menu` VALUES (2, 1042);
INSERT INTO `sys_role_menu` VALUES (2, 1043);
INSERT INTO `sys_role_menu` VALUES (2, 1044);
INSERT INTO `sys_role_menu` VALUES (2, 1045);
INSERT INTO `sys_role_menu` VALUES (2, 1046);
INSERT INTO `sys_role_menu` VALUES (2, 1047);
INSERT INTO `sys_role_menu` VALUES (2, 1048);
INSERT INTO `sys_role_menu` VALUES (2, 1049);
INSERT INTO `sys_role_menu` VALUES (2, 1050);
INSERT INTO `sys_role_menu` VALUES (2, 1051);
INSERT INTO `sys_role_menu` VALUES (2, 1052);
INSERT INTO `sys_role_menu` VALUES (2, 1053);
INSERT INTO `sys_role_menu` VALUES (2, 1054);
INSERT INTO `sys_role_menu` VALUES (2, 1055);
INSERT INTO `sys_role_menu` VALUES (2, 1056);
INSERT INTO `sys_role_menu` VALUES (2, 1057);
INSERT INTO `sys_role_menu` VALUES (2, 1058);
INSERT INTO `sys_role_menu` VALUES (2, 1059);
INSERT INTO `sys_role_menu` VALUES (2, 1060);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '密码',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `score` int(11) NULL DEFAULT NULL COMMENT '信誉分数',
  `reservation_id` int(20) NULL DEFAULT NULL COMMENT '预约Id',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 109 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 103, 'admin', '若依', '00', 'ry@163.com', '15888888888', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2024-03-10 16:12:12', 'admin', '2024-03-07 20:14:36', '', '2024-03-10 16:12:12', '管理员', NULL, NULL);
INSERT INTO `sys_user` VALUES (2, 105, 'ry', '若依', '00', 'ry@qq.com', '15666666666', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2024-03-07 20:14:36', 'admin', '2024-03-07 20:14:36', '', NULL, '测试员', NULL, NULL);
INSERT INTO `sys_user` VALUES (100, 100, '2024030705', '2024030705', '11', 'archyqx@gmail.com', '18829539116', '0', '', '$2a$10$/ZpBTJhLxb9XkZq7.mYNGOYO6PQIt7KwANgHM2BotyAt041Io8F8W', '0', '0', '172.24.87.235', '2024-03-10 15:38:20', 'admin', '2024-03-07 20:29:32', 'admin', '2024-03-10 15:38:20', '111', 10, 9);
INSERT INTO `sys_user` VALUES (101, NULL, 'admin1', 'admin1', '00', '', '', '0', '', '$2a$10$T2BWoJhvcW7Kef2XHLVVH.lQJxVCZUEWch499D3jknQyssyaFTFZy', '0', '0', '', NULL, '', '2024-03-07 22:50:02', 'admin', '2024-03-09 09:26:18', NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES (102, NULL, 'admin6', 'admin6', '00', '', '', '0', '', '$2a$10$hsiFM2Ij3m0ImNsOItst2uGeC.JpnQr2aVo5/KAW.DXnwJK8UK4BS', '0', '0', '', NULL, '', '2024-03-07 23:01:16', '', NULL, NULL, 10, NULL);
INSERT INTO `sys_user` VALUES (103, NULL, 'admin4', 'admin4', '11', '', '', '0', '', '$2a$10$LQCbuIuKt4nszhfzE1QTOeE.Oq/hO.OXJSxUFJcQmDYvA.QEmnOAC', '0', '0', '', NULL, '', '2024-03-07 23:03:56', '', NULL, NULL, 10, NULL);
INSERT INTO `sys_user` VALUES (104, NULL, '123456', '123456', '11', '', '', '0', '', '$2a$10$nVH.pYJBUrPjM1US2kgDgemqS87asjQV8BOmLoSEb9MD4FYP7zXyC', '0', '0', '', NULL, '', '2024-03-09 10:35:44', '', NULL, NULL, 10, NULL);
INSERT INTO `sys_user` VALUES (105, NULL, '666666', '666666', '11', '', '', '0', '', '$2a$10$sCPznDWalwnhtjC8R/mZOu0HqVDk25IuR../41TsKe80ReNHvWcKq', '0', '2', '', NULL, '', '2024-03-09 10:37:04', '2024030705', '2024-03-09 11:00:33', NULL, 10, NULL);
INSERT INTO `sys_user` VALUES (106, NULL, 'test', 'test', '11', '', '', '0', '', '$2a$10$wiVFScYY0iPlM80zWu4QLO92XoM70MzF3tDZAv7nFOswARQzBgM2e', '0', '2', '', NULL, '', '2024-03-09 10:43:53', '', NULL, NULL, 10, NULL);
INSERT INTO `sys_user` VALUES (107, NULL, '666', '666', '11', '', '', '0', '', '$2a$10$rhkjat6wqIZd6LEoY06IeOkYac5z9HrK5fCnrgHKLo.puTPC.KBe6', '0', '2', '', NULL, '', '2024-03-09 17:06:10', '', NULL, NULL, 10, NULL);
INSERT INTO `sys_user` VALUES (108, NULL, '666666', '666666', '11', '', '', '0', '', '$2a$10$16yCgK/TKWYMt7JuRuKkl.7SIhGL8IMvXNXKHHTId6lokhKly7LnW', '0', '0', '172.24.87.235', '2024-03-10 16:09:11', '', '2024-03-09 17:06:35', '', '2024-03-10 16:09:10', NULL, 10, NULL);

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (2, 2);
INSERT INTO `sys_user_post` VALUES (100, 1);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);
INSERT INTO `sys_user_role` VALUES (100, 2);

SET FOREIGN_KEY_CHECKS = 1;
