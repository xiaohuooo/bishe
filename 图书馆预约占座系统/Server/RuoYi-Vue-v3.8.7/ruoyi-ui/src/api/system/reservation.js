import request from '@/utils/request'

// 查询座位预约列表
export function listReservation(query) {
  return request({
    url: '/system/reservation/list',
    method: 'get',
    params: query
  })
}

// 查询座位预约详细
export function getReservation(reservationId) {
  return request({
    url: '/system/reservation/' + reservationId,
    method: 'get'
  })
}

// 新增座位预约
export function addReservation(data) {
  return request({
    url: '/system/reservation',
    method: 'post',
    data: data
  })
}

// 修改座位预约
export function updateReservation(data) {
  return request({
    url: '/system/reservation',
    method: 'put',
    data: data
  })
}

// 删除座位预约
export function delReservation(reservationId) {
  return request({
    url: '/system/reservation/' + reservationId,
    method: 'delete'
  })
}
