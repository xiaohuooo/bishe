package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 座位预约对象 lr_reservation
 * 
 * @author ruoyi
 * @date 2024-03-07
 */
public class LrReservation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 预订id */
    private Long reservationId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long studentId;

    /** 座位id */
    @Excel(name = "座位id")
    private Long seatId;

    /** 预定日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "预定日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date reservationDate;

    /** 1上午2下午3晚上 */
    @Excel(name = "1上午2下午3晚上")
    private Integer timeSlot;

    /** 是否入座 0为未入座 1为已入座 2为退座 */
    @Excel(name = "是否入座 0为未入座 1为已入座 2为退座")
    private String isActive;

    /** 是否取消预约 0为未取消 1为已取消 */
    @Excel(name = "是否取消预约 0为未取消 1为已取消")
    private String isCancal;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setReservationId(Long reservationId) 
    {
        this.reservationId = reservationId;
    }

    public Long getReservationId() 
    {
        return reservationId;
    }
    public void setStudentId(Long studentId) 
    {
        this.studentId = studentId;
    }

    public Long getStudentId() 
    {
        return studentId;
    }
    public void setSeatId(Long seatId) 
    {
        this.seatId = seatId;
    }

    public Long getSeatId() 
    {
        return seatId;
    }
    public void setReservationDate(Date reservationDate) 
    {
        this.reservationDate = reservationDate;
    }

    public Date getReservationDate() 
    {
        return reservationDate;
    }
    public void setTimeSlot(Integer timeSlot) 
    {
        this.timeSlot = timeSlot;
    }

    public Integer getTimeSlot() 
    {
        return timeSlot;
    }
    public void setIsActive(String isActive) 
    {
        this.isActive = isActive;
    }

    public String getIsActive() 
    {
        return isActive;
    }
    public void setIsCancal(String isCancal) 
    {
        this.isCancal = isCancal;
    }

    public String getIsCancal() 
    {
        return isCancal;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("reservationId", getReservationId())
            .append("studentId", getStudentId())
            .append("seatId", getSeatId())
            .append("reservationDate", getReservationDate())
            .append("timeSlot", getTimeSlot())
            .append("isActive", getIsActive())
            .append("isCancal", getIsCancal())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
