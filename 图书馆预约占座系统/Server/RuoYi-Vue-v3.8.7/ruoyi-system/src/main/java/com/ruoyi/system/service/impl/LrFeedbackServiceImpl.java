package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.LrFeedbackMapper;
import com.ruoyi.system.domain.LrFeedback;
import com.ruoyi.system.service.ILrFeedbackService;

/**
 * 意见反馈Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-03-07
 */
@Service
public class LrFeedbackServiceImpl implements ILrFeedbackService 
{
    @Autowired
    private LrFeedbackMapper lrFeedbackMapper;

    /**
     * 查询意见反馈
     * 
     * @param id 意见反馈主键
     * @return 意见反馈
     */
    @Override
    public LrFeedback selectLrFeedbackById(Long id)
    {
        return lrFeedbackMapper.selectLrFeedbackById(id);
    }

    /**
     * 查询意见反馈列表
     * 
     * @param lrFeedback 意见反馈
     * @return 意见反馈
     */
    @Override
    public List<LrFeedback> selectLrFeedbackList(LrFeedback lrFeedback)
    {
        return lrFeedbackMapper.selectLrFeedbackList(lrFeedback);
    }

    /**
     * 新增意见反馈
     * 
     * @param lrFeedback 意见反馈
     * @return 结果
     */
    @Override
    public int insertLrFeedback(LrFeedback lrFeedback)
    {
        lrFeedback.setCreateTime(DateUtils.getNowDate());
        return lrFeedbackMapper.insertLrFeedback(lrFeedback);
    }

    /**
     * 修改意见反馈
     * 
     * @param lrFeedback 意见反馈
     * @return 结果
     */
    @Override
    public int updateLrFeedback(LrFeedback lrFeedback)
    {
        lrFeedback.setUpdateTime(DateUtils.getNowDate());
        return lrFeedbackMapper.updateLrFeedback(lrFeedback);
    }

    /**
     * 批量删除意见反馈
     * 
     * @param ids 需要删除的意见反馈主键
     * @return 结果
     */
    @Override
    public int deleteLrFeedbackByIds(Long[] ids)
    {
        return lrFeedbackMapper.deleteLrFeedbackByIds(ids);
    }

    /**
     * 删除意见反馈信息
     * 
     * @param id 意见反馈主键
     * @return 结果
     */
    @Override
    public int deleteLrFeedbackById(Long id)
    {
        return lrFeedbackMapper.deleteLrFeedbackById(id);
    }
}
