package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.LrSeat;

/**
 * 座位管理Service接口
 * 
 * @author ruoyi
 * @date 2024-03-07
 */
public interface ILrSeatService 
{
    /**
     * 查询座位管理
     * 
     * @param seatId 座位管理主键
     * @return 座位管理
     */
    public LrSeat selectLrSeatBySeatId(Long seatId);

    /**
     * 查询座位管理列表
     * 
     * @param lrSeat 座位管理
     * @return 座位管理集合
     */
    public List<LrSeat> selectLrSeatList(LrSeat lrSeat);

    /**
     * 查询座位管理列表
     *
     * @param lrSeat 座位管理
     * @return 座位管理集合
     */
    public List<LrSeat> selectLrSeatReservationList(LrSeat lrSeat);

    /**
     * 新增座位管理
     * 
     * @param lrSeat 座位管理
     * @return 结果
     */
    public int insertLrSeat(LrSeat lrSeat);

    /**
     * 修改座位管理
     * 
     * @param lrSeat 座位管理
     * @return 结果
     */
    public int updateLrSeat(LrSeat lrSeat);

    /**
     * 批量删除座位管理
     * 
     * @param seatIds 需要删除的座位管理主键集合
     * @return 结果
     */
    public int deleteLrSeatBySeatIds(Long[] seatIds);

    /**
     * 删除座位管理信息
     * 
     * @param seatId 座位管理主键
     * @return 结果
     */
    public int deleteLrSeatBySeatId(Long seatId);
}
