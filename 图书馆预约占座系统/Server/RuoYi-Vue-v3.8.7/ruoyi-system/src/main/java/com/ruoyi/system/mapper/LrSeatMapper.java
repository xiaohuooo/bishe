package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.LrSeat;

/**
 * 座位管理Mapper接口
 * 
 * @author ruoyi
 * @date 2024-03-07
 */
public interface LrSeatMapper 
{
    /**
     * 查询座位管理
     * 
     * @param seatId 座位管理主键
     * @return 座位管理
     */
    public LrSeat selectLrSeatBySeatId(Long seatId);

    /**
     * 查询座位管理列表
     * 
     * @param lrSeat 座位管理
     * @return 座位管理集合
     */
    public List<LrSeat> selectLrSeatList(LrSeat lrSeat);

    /**
     * 查询座位管理列表
     *
     * @param lrSeat 座位管理
     * @return 座位管理集合
     */
    public List<LrSeat> selectLrSeatReservationList(LrSeat lrSeat);


    /**
     * 新增座位管理
     * 
     * @param lrSeat 座位管理
     * @return 结果
     */
    public int insertLrSeat(LrSeat lrSeat);

    /**
     * 修改座位管理
     * 
     * @param lrSeat 座位管理
     * @return 结果
     */
    public int updateLrSeat(LrSeat lrSeat);

    /**
     * 删除座位管理
     * 
     * @param seatId 座位管理主键
     * @return 结果
     */
    public int deleteLrSeatBySeatId(Long seatId);

    /**
     * 批量删除座位管理
     * 
     * @param seatIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteLrSeatBySeatIds(Long[] seatIds);
}
