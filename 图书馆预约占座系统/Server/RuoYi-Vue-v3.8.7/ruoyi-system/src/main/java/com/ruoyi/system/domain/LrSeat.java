package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 座位管理对象 lr_seat
 * 
 * @author ruoyi
 * @date 2024-03-07
 */
public class LrSeat extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long seatId;

    /** 座位信息 */
    @Excel(name = "座位信息")
    private String seatLocation;

    /** 0为可用，1为故障不可用 */
    @Excel(name = "0为可用，1为故障不可用")
    private String seatStatus;

    /** 座位编号 */
    @Excel(name = "座位编号")
    private String seatNumber;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setSeatId(Long seatId) 
    {
        this.seatId = seatId;
    }

    public Long getSeatId() 
    {
        return seatId;
    }
    public void setSeatLocation(String seatLocation) 
    {
        this.seatLocation = seatLocation;
    }

    public String getSeatLocation() 
    {
        return seatLocation;
    }
    public void setSeatStatus(String seatStatus) 
    {
        this.seatStatus = seatStatus;
    }

    public String getSeatStatus() 
    {
        return seatStatus;
    }
    public void setSeatNumber(String seatNumber) 
    {
        this.seatNumber = seatNumber;
    }

    public String getSeatNumber() 
    {
        return seatNumber;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("seatId", getSeatId())
            .append("seatLocation", getSeatLocation())
            .append("seatStatus", getSeatStatus())
            .append("seatNumber", getSeatNumber())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
