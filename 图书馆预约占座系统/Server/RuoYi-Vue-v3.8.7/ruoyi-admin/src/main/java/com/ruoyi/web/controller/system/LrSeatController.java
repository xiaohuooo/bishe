package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.LrSeat;
import com.ruoyi.system.service.ILrSeatService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 座位管理Controller
 * 
 * @author ruoyi
 * @date 2024-03-07
 */
@RestController
@RequestMapping("/system/seat")
public class LrSeatController extends BaseController
{
    @Autowired
    private ILrSeatService lrSeatService;

    /**
     * 查询座位管理列表
     */
    @GetMapping("/list")
    public TableDataInfo list(LrSeat lrSeat)
    {
        startPage();
        List<LrSeat> list = lrSeatService.selectLrSeatList(lrSeat);
        return getDataTable(list);
    }

    /**
     * 查询座位管理列表
     */
    @PostMapping("/seatList")
    @ResponseBody
    public AjaxResult seatList(LrSeat lrSeat)
    {

        List<LrSeat> reservationList = lrSeatService.selectLrSeatReservationList(lrSeat);

        List<LrSeat> list = lrSeatService.selectLrSeatList(lrSeat);
        for (LrSeat reservationSeat : reservationList) {
            for (LrSeat seat : list) {
                if (seat.getSeatId() == reservationSeat.getSeatId()) {
                    seat.setSeatStatus(String.valueOf(2));
                }
            }
        }
        return success(list);
    }

    /**
     * 导出座位管理列表
     */
    @Log(title = "座位管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, LrSeat lrSeat)
    {
        List<LrSeat> list = lrSeatService.selectLrSeatList(lrSeat);
        ExcelUtil<LrSeat> util = new ExcelUtil<LrSeat>(LrSeat.class);
        util.exportExcel(response, list, "座位管理数据");
    }

    /**
     * 获取座位管理详细信息
     */
    @GetMapping(value = "/{seatId}")
    public AjaxResult getInfo(@PathVariable("seatId") Long seatId)
    {
        return success(lrSeatService.selectLrSeatBySeatId(seatId));
    }

    /**
     * 新增座位管理
     */
    @Log(title = "座位管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody LrSeat lrSeat)
    {
        return toAjax(lrSeatService.insertLrSeat(lrSeat));
    }

    /**
     * 修改座位管理
     */
    @Log(title = "座位管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody LrSeat lrSeat)
    {
        return toAjax(lrSeatService.updateLrSeat(lrSeat));
    }

    /**
     * 删除座位管理
     */
    @Log(title = "座位管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{seatIds}")
    public AjaxResult remove(@PathVariable Long[] seatIds)
    {
        return toAjax(lrSeatService.deleteLrSeatBySeatIds(seatIds));
    }
}
