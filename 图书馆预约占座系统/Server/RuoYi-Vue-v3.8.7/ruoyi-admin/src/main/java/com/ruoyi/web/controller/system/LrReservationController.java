package com.ruoyi.web.controller.system;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.service.ISysConfigService;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.LrReservation;
import com.ruoyi.system.service.ILrReservationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 座位预约Controller
 * 
 * @author ruoyi
 * @date 2024-03-07
 */
@RestController
@RequestMapping("/system/reservation")
public class LrReservationController extends BaseController
{
    @Autowired
    private ILrReservationService lrReservationService;

    @Autowired
    private ISysUserService userService;

    @Autowired
    private ISysConfigService configService;
    @Autowired
    private TokenService tokenService;

    /**
     * 查询座位预约列表
     */
    @GetMapping("/list")
    public AjaxResult list(LrReservation lrReservation)
    {
        startPage();
        List<LrReservation> list = lrReservationService.selectLrReservationList(lrReservation);
        return success(list);
    }

    /**
     * 导出座位预约列表
     */
    @Log(title = "座位预约", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, LrReservation lrReservation)
    {
        List<LrReservation> list = lrReservationService.selectLrReservationList(lrReservation);
        ExcelUtil<LrReservation> util = new ExcelUtil<LrReservation>(LrReservation.class);
        util.exportExcel(response, list, "座位预约数据");
    }

    /**
     * 获取座位预约详细信息
     */
    @GetMapping(value = "/{reservationId}")
    public AjaxResult getInfo(@PathVariable("reservationId") Long reservationId)
    {
        return success(lrReservationService.selectLrReservationByReservationId(reservationId));
    }

    /**
     * 新增座位预约
     */
    @Log(title = "座位预约", businessType = BusinessType.INSERT)
    @PostMapping("/reservation")
    public AjaxResult add(@RequestBody LrReservation lrReservation)
    {
        return toAjax(lrReservationService.insertLrReservation(lrReservation));
    }

    /**
     * 修改座位预约
     */
    @Log(title = "座位预约", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody LrReservation lrReservation)
    {
        return toAjax(lrReservationService.updateLrReservation(lrReservation));
    }

    /**
     * 查询座位预约列表
     */
    @GetMapping("/listTod")
    public AjaxResult listTod(LrReservation lrReservation)
    {
        lrReservation.setIsActive(String.valueOf(0));
        lrReservation.setStudentId(getUserId());
        lrReservation.setReservationDate(DateUtils.getNowDate());
        List<LrReservation> list = lrReservationService.selectLrReservationList(lrReservation);
        return success(list);
    }

    /**
     * 入座
     */
    @Log(title = "入座", businessType = BusinessType.UPDATE)
    @PostMapping("/takePlace")
    public AjaxResult takePlace(@RequestBody LrReservation lrReservation)
    {
        LrReservation lrReservation1 = lrReservationService.selectLrReservationByReservationId(lrReservation.getReservationId());
        lrReservation1.setIsActive(String.valueOf(1));
        userService.updateUserReservationId(getLoginUser().getUsername(),lrReservation.getReservationId());
        getLoginUser().getUser().setReservationId(lrReservation.getReservationId());
        tokenService.setLoginUser(getLoginUser());
        return toAjax(lrReservationService.updateLrReservation(lrReservation1));
    }

    /**
     * 退座
     */
    @Log(title = "退座", businessType = BusinessType.UPDATE)
    @PostMapping("/cancelPlace")
    public AjaxResult cancelPlace( LrReservation lrReservation)
    {
        if (getLoginUser().getUser().getReservationId()==null){
            return error("当前无法退座");
        }
        LrReservation lrReservation1 = lrReservationService.selectLrReservationByReservationId(getLoginUser().getUser().getReservationId());
        lrReservation1.setIsActive(String.valueOf(2));
        userService.updateUserReservationId(getLoginUser().getUsername(),null);
        getLoginUser().getUser().setReservationId(null);
        tokenService.setLoginUser(getLoginUser());
        return toAjax(lrReservationService.updateLrReservation(lrReservation1));
    }

    /**
     * 取消预约
     */
    @Log(title = "取消预约", businessType = BusinessType.UPDATE)
    @PostMapping("/cancelReservation")
    public AjaxResult cancelReservation(@RequestBody LrReservation lrReservation)
    {
        LrReservation lrReservation1 = lrReservationService.selectLrReservationByReservationId(lrReservation.getReservationId());
        lrReservation1.setIsCancal(String.valueOf(1));
        return toAjax(lrReservationService.updateLrReservation(lrReservation1));
    }

    /**
     * 离开座位
     */
    @Log(title = "离开座位", businessType = BusinessType.UPDATE)
    @PostMapping("/leaveSeat")
    public AjaxResult leaveSeat( LrReservation lrReservation)
    {
        if (getLoginUser().getUser().getReservationId()==null){
            return error("当前无法离座");
        }
        LrReservation lrReservation1 = lrReservationService.selectLrReservationByReservationId(getLoginUser().getUser().getReservationId());
        lrReservation1.setIsActive(String.valueOf(3));
        userService.updateUserReservationId(getLoginUser().getUsername(),null);
        getLoginUser().getUser().setReservationId(null);
        tokenService.setLoginUser(getLoginUser());
        return toAjax(lrReservationService.updateLrReservation(lrReservation1));
    }

    /**
     * 返回座位
     */
    @Log(title = "返回座位", businessType = BusinessType.UPDATE)
    @PostMapping("/returnSeat")
    public AjaxResult returnSeat(@RequestBody LrReservation lrReservation)
    {

        LrReservation lrReservation2 = new LrReservation();
        lrReservation2.setStudentId(getUserId());
        lrReservation2.setReservationDate(DateUtils.getNowDate());
        lrReservation2.setSeatId(lrReservation.getSeatId());
        List<LrReservation> lrReservations = lrReservationService.selectLrReservationList(lrReservation2);

        LrReservation lrReservation1 = new LrReservation();
        if (lrReservations.size()==1){
            lrReservation1=lrReservations.get(0);
        }
//        lrReservation1 = lrReservationService.selectLrReservationByReservationId(lrReservation.getReservationId());
        lrReservation1.setIsActive(String.valueOf(1));
        LocalDateTime halfHourAgo = LocalDateTime.now().minus(30, ChronoUnit.MINUTES);
        LocalDateTime leaveSeatTime = lrReservation1.getUpdateTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        if (leaveSeatTime.isBefore(halfHourAgo)){
            Integer score = getLoginUser().getUser().getScore();
            if (StringUtils.isNotEmpty(configService.selectConfigByKey("sys.account.leaveSeatTimeOutScore"))){
                int s =Integer.parseInt(configService.selectConfigByKey("sys.account.leaveSeatTimeOutScore"));
                userService.updateUserScore(getLoginUser().getUser().getUserName(),score-s);
                getLoginUser().getUser().setScore(score-s);
                tokenService.setLoginUser(getLoginUser());
            }
            else{
                return error("当前系统没有初始化离开座位扣信誉分数！");
            }
        }
        userService.updateUserReservationId(getLoginUser().getUsername(),null);
        getLoginUser().getUser().setReservationId(null);
        tokenService.setLoginUser(getLoginUser());
        return toAjax(lrReservationService.updateLrReservation(lrReservation1));
    }


    /**
     * 删除座位预约
     */
    @Log(title = "座位预约", businessType = BusinessType.DELETE)
	@DeleteMapping("/{reservationIds}")
    public AjaxResult remove(@PathVariable Long[] reservationIds)
    {
        return toAjax(lrReservationService.deleteLrReservationByReservationIds(reservationIds));
    }
}
