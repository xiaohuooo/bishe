package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.LrFeedback;
import com.ruoyi.system.service.ILrFeedbackService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 意见反馈Controller
 * 
 * @author ruoyi
 * @date 2024-03-07
 */
@RestController
@RequestMapping("/system/feedback")
public class LrFeedbackController extends BaseController
{
    @Autowired
    private ILrFeedbackService lrFeedbackService;

    /**
     * 查询意见反馈列表
     */
    @GetMapping("/list")
    public TableDataInfo list(LrFeedback lrFeedback)
    {
        startPage();
        List<LrFeedback> list = lrFeedbackService.selectLrFeedbackList(lrFeedback);
        return getDataTable(list);
    }

    /**
     * 导出意见反馈列表
     */
    @Log(title = "意见反馈", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, LrFeedback lrFeedback)
    {
        List<LrFeedback> list = lrFeedbackService.selectLrFeedbackList(lrFeedback);
        ExcelUtil<LrFeedback> util = new ExcelUtil<LrFeedback>(LrFeedback.class);
        util.exportExcel(response, list, "意见反馈数据");
    }

    /**
     * 获取意见反馈详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(lrFeedbackService.selectLrFeedbackById(id));
    }

    /**
     * 新增意见反馈
     */
    @Log(title = "意见反馈", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody LrFeedback lrFeedback)
    {
        return toAjax(lrFeedbackService.insertLrFeedback(lrFeedback));
    }

    /**
     * 修改意见反馈
     */
    @Log(title = "意见反馈", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody LrFeedback lrFeedback)
    {
        return toAjax(lrFeedbackService.updateLrFeedback(lrFeedback));
    }

    /**
     * 删除意见反馈
     */
    @Log(title = "意见反馈", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(lrFeedbackService.deleteLrFeedbackByIds(ids));
    }
}
