package com.ruoyi.quartz.task;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.system.domain.LrReservation;
import com.ruoyi.system.mapper.LrReservationMapper;
import com.ruoyi.system.service.ISysConfigService;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.ruoyi.common.utils.StringUtils;

import static com.ruoyi.common.utils.SecurityUtils.getLoginUser;

/**
 * 定时任务调度测试
 * 
 * @author ruoyi
 */
@Component("ryTask")
public class RyTask
{

    @Autowired
    private ISysUserService userService;

    @Autowired
    private ISysConfigService configService;
    @Autowired
    private LrReservationMapper lrReservationMapper;
    @Autowired
    ISysUserService sysUserService;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private RedisCache redisCache;

    public void ryMultipleParams(String s, Boolean b, Long l, Double d, Integer i)
    {
        System.out.println(StringUtils.format("执行多参方法： 字符串类型{}，布尔类型{}，长整型{}，浮点型{}，整形{}", s, b, l, d, i));
    }

    public void ryParams(String params)
    {
        //时间段后更新对应的预约信息
        SysUser sysUser = new SysUser();
        sysUser.setUserType("11");
        sysUser.setStatus("0");
        for (SysUser user : sysUserService.selectUserList(sysUser)) {
            sysUserService.updateUserReservationId(user.getUserName(),null);
        }

        System.out.println("执行有参方法：" + params);
    }

//    public void ryNoParams()
//    {
//        System.out.println("执行无参方法");
//    }
    public void ryNoParams()
    {
        //查询全部一个月内同一个人三次预约取消
        LrReservation lrReservation = lrReservationMapper.selectLrReservationByCancelReservationCount(new LrReservation());
        if (Integer.parseInt(lrReservation.getRemark())>3){
            SysUser sysUser = userService.selectUserById(lrReservation.getStudentId());
            Integer score = sysUser.getScore();
            if (StringUtils.isNotEmpty(configService.selectConfigByKey("sys.account.leaveSeatTimeOutScore"))){
                int s =Integer.parseInt(configService.selectConfigByKey("sys.account.leaveSeatTimeOutScore"));
                userService.updateUserScore(sysUser.getUserName(),score-s);
                getLoginUser().getUser().setScore(score-s);
            }
            else{
                new Exception("当前系统没有初始化离开座位扣信誉分数！");
            }
        }
    }
}
