package com.ruoyi.web.controller.system;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.LrFeedback;
import com.ruoyi.system.service.ILrFeedbackService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 意见反馈Controller
 * 
 * @author ruoyi
 * @date 2024-03-06
 */
@Controller
@CrossOrigin
@RequestMapping("/system/feedback")
public class LrFeedbackController extends BaseController
{
    private String prefix = "system/feedback";

    @Autowired
    private ILrFeedbackService lrFeedbackService;

    @RequiresPermissions("system:feedback:view")
    @GetMapping()
    public String feedback()
    {
        return prefix + "/feedback";
    }

    /**
     * 查询意见反馈列表
     */
    @RequiresPermissions("system:feedback:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(LrFeedback lrFeedback)
    {
        startPage();
        List<LrFeedback> list = lrFeedbackService.selectLrFeedbackList(lrFeedback);
        return getDataTable(list);
    }

    /**
     * 导出意见反馈列表
     */
    @RequiresPermissions("system:feedback:export")
    @Log(title = "意见反馈", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(LrFeedback lrFeedback)
    {
        List<LrFeedback> list = lrFeedbackService.selectLrFeedbackList(lrFeedback);
        ExcelUtil<LrFeedback> util = new ExcelUtil<LrFeedback>(LrFeedback.class);
        return util.exportExcel(list, "意见反馈数据");
    }

    /**
     * 新增意见反馈
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存意见反馈
     */
    @RequiresPermissions("system:feedback:add")
    @Log(title = "意见反馈", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(LrFeedback lrFeedback)
    {
        return toAjax(lrFeedbackService.insertLrFeedback(lrFeedback));
    }

    /**
     * 修改意见反馈
     */
    @RequiresPermissions("system:feedback:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        LrFeedback lrFeedback = lrFeedbackService.selectLrFeedbackById(id);
        mmap.put("lrFeedback", lrFeedback);
        return prefix + "/edit";
    }

    /**
     * 修改保存意见反馈
     */
    @RequiresPermissions("system:feedback:edit")
    @Log(title = "意见反馈", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(LrFeedback lrFeedback)
    {
        return toAjax(lrFeedbackService.updateLrFeedback(lrFeedback));
    }

    /**
     * 删除意见反馈
     */
    @RequiresPermissions("system:feedback:remove")
    @Log(title = "意见反馈", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(lrFeedbackService.deleteLrFeedbackByIds(ids));
    }
}
