package com.ruoyi.web.controller.system;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.LrSeat;
import com.ruoyi.system.service.ILrSeatService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 座位管理Controller
 * 
 * @author ruoyi
 * @date 2024-03-06
 */
@Controller
@CrossOrigin
@RequestMapping("/system/seat")
public class LrSeatController extends BaseController
{
    private String prefix = "system/seat";

    @Autowired
    private ILrSeatService lrSeatService;

    @RequiresPermissions("system:seat:view")
    @GetMapping()
    public String seat()
    {
        return prefix + "/seat";
    }

    /**
     * 查询座位管理列表
     */
    @RequiresPermissions("system:seat:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(LrSeat lrSeat)
    {
        startPage();
        List<LrSeat> list = lrSeatService.selectLrSeatList(lrSeat);
        return getDataTable(list);
    }

    /**
     * 查询座位管理列表
     */
    @PostMapping("/seatList")
    @ResponseBody
    public TableDataInfo seatList(LrSeat lrSeat)
    {
        startPage();
        List<LrSeat> reservationList = lrSeatService.selectLrSeatReservationList(lrSeat);

        List<LrSeat> list = lrSeatService.selectLrSeatList(lrSeat);
        for (LrSeat reservationSeat : reservationList) {
            for (LrSeat seat : list) {
                if (seat.getSeatId() == reservationSeat.getSeatId()) {
                    seat.setSeatStatus(String.valueOf(2));
                }
            }
        }
        return getDataTable(list);
    }

    /**
     * 导出座位管理列表
     */
    @RequiresPermissions("system:seat:export")
    @Log(title = "座位管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(LrSeat lrSeat)
    {
        List<LrSeat> list = lrSeatService.selectLrSeatList(lrSeat);
        ExcelUtil<LrSeat> util = new ExcelUtil<LrSeat>(LrSeat.class);
        return util.exportExcel(list, "座位管理数据");
    }

    /**
     * 新增座位管理
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存座位管理
     */
    @RequiresPermissions("system:seat:add")
    @Log(title = "座位管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(LrSeat lrSeat)
    {
        return toAjax(lrSeatService.insertLrSeat(lrSeat));
    }

    /**
     * 修改座位管理
     */
    @RequiresPermissions("system:seat:edit")
    @GetMapping("/edit/{seatId}")
    public String edit(@PathVariable("seatId") Long seatId, ModelMap mmap)
    {
        LrSeat lrSeat = lrSeatService.selectLrSeatBySeatId(seatId);
        mmap.put("lrSeat", lrSeat);
        return prefix + "/edit";
    }

    /**
     * 修改保存座位管理
     */
    @RequiresPermissions("system:seat:edit")
    @Log(title = "座位管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(LrSeat lrSeat)
    {
        return toAjax(lrSeatService.updateLrSeat(lrSeat));
    }

    /**
     * 删除座位管理
     */
    @RequiresPermissions("system:seat:remove")
    @Log(title = "座位管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(lrSeatService.deleteLrSeatBySeatIds(ids));
    }
}
