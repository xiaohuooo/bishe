package com.ruoyi.web.controller.system;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.LrReservation;
import com.ruoyi.system.service.ILrReservationService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 座位预约Controller
 * 
 * @author ruoyi
 * @date 2024-03-06
 */
@Controller
@CrossOrigin
@RequestMapping("/system/reservation")
public class LrReservationController extends BaseController
{
    private String prefix = "system/reservation";

    @Autowired
    private ILrReservationService lrReservationService;

    @RequiresPermissions("system:reservation:view")
    @GetMapping()
    public String reservation()
    {
        return prefix + "/reservation";
    }

    /**
     * 查询座位预约列表
     */
    @RequiresPermissions("system:reservation:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(LrReservation lrReservation)
    {
        startPage();
        List<LrReservation> list = lrReservationService.selectLrReservationList(lrReservation);
        return getDataTable(list);
    }

    /**
     * 导出座位预约列表
     */
    @RequiresPermissions("system:reservation:export")
    @Log(title = "座位预约", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(LrReservation lrReservation)
    {
        List<LrReservation> list = lrReservationService.selectLrReservationList(lrReservation);
        ExcelUtil<LrReservation> util = new ExcelUtil<LrReservation>(LrReservation.class);
        return util.exportExcel(list, "座位预约数据");
    }

    /**
     * 新增座位预约
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存座位预约
     */
    @RequiresPermissions("system:reservation:add")
    @Log(title = "座位预约", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(LrReservation lrReservation)
    {
        return toAjax(lrReservationService.insertLrReservation(lrReservation));
    }

    /**
     * 修改座位预约
     */
    @RequiresPermissions("system:reservation:edit")
    @GetMapping("/edit/{reservationId}")
    public String edit(@PathVariable("reservationId") Long reservationId, ModelMap mmap)
    {
        LrReservation lrReservation = lrReservationService.selectLrReservationByReservationId(reservationId);
        mmap.put("lrReservation", lrReservation);
        return prefix + "/edit";
    }

    /**
     * 修改保存座位预约
     */
    @RequiresPermissions("system:reservation:edit")
    @Log(title = "座位预约", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(LrReservation lrReservation)
    {
        return toAjax(lrReservationService.updateLrReservation(lrReservation));
    }

    /**
     * 删除座位预约
     */
    @RequiresPermissions("system:reservation:remove")
    @Log(title = "座位预约", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(lrReservationService.deleteLrReservationByReservationIds(ids));
    }
}
