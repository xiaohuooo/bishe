package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.LrFeedback;

/**
 * 意见反馈Service接口
 * 
 * @author ruoyi
 * @date 2024-03-06
 */
public interface ILrFeedbackService 
{
    /**
     * 查询意见反馈
     * 
     * @param id 意见反馈主键
     * @return 意见反馈
     */
    public LrFeedback selectLrFeedbackById(Long id);

    /**
     * 查询意见反馈列表
     * 
     * @param lrFeedback 意见反馈
     * @return 意见反馈集合
     */
    public List<LrFeedback> selectLrFeedbackList(LrFeedback lrFeedback);

    /**
     * 新增意见反馈
     * 
     * @param lrFeedback 意见反馈
     * @return 结果
     */
    public int insertLrFeedback(LrFeedback lrFeedback);

    /**
     * 修改意见反馈
     * 
     * @param lrFeedback 意见反馈
     * @return 结果
     */
    public int updateLrFeedback(LrFeedback lrFeedback);

    /**
     * 批量删除意见反馈
     * 
     * @param ids 需要删除的意见反馈主键集合
     * @return 结果
     */
    public int deleteLrFeedbackByIds(String ids);

    /**
     * 删除意见反馈信息
     * 
     * @param id 意见反馈主键
     * @return 结果
     */
    public int deleteLrFeedbackById(Long id);
}
