package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 意见反馈对象 lr_feedback
 * 
 * @author ruoyi
 * @date 2024-03-06
 */
public class LrFeedback extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 意见反馈标题 */
    @Excel(name = "意见反馈标题")
    private String feedbackTitle;

    /** 意见反馈内容 */
    @Excel(name = "意见反馈内容")
    private String feedbackContent;

    /** 意见反馈位置信息 */
    @Excel(name = "意见反馈位置信息")
    private String feedbackLocation;

    /** 意见反馈是否被处理（默认0、1为已处理） */
    @Excel(name = "意见反馈是否被处理", readConverterExp = "默=认0、1为已处理")
    private String feedbackHandle;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setFeedbackTitle(String feedbackTitle) 
    {
        this.feedbackTitle = feedbackTitle;
    }

    public String getFeedbackTitle() 
    {
        return feedbackTitle;
    }
    public void setFeedbackContent(String feedbackContent) 
    {
        this.feedbackContent = feedbackContent;
    }

    public String getFeedbackContent() 
    {
        return feedbackContent;
    }
    public void setFeedbackLocation(String feedbackLocation) 
    {
        this.feedbackLocation = feedbackLocation;
    }

    public String getFeedbackLocation() 
    {
        return feedbackLocation;
    }
    public void setFeedbackHandle(String feedbackHandle) 
    {
        this.feedbackHandle = feedbackHandle;
    }

    public String getFeedbackHandle() 
    {
        return feedbackHandle;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("feedbackTitle", getFeedbackTitle())
            .append("feedbackContent", getFeedbackContent())
            .append("feedbackLocation", getFeedbackLocation())
            .append("feedbackHandle", getFeedbackHandle())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
