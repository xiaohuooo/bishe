package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.LrReservation;

/**
 * 座位预约Service接口
 * 
 * @author ruoyi
 * @date 2024-03-06
 */
public interface ILrReservationService 
{
    /**
     * 查询座位预约
     * 
     * @param reservationId 座位预约主键
     * @return 座位预约
     */
    public LrReservation selectLrReservationByReservationId(Long reservationId);

    /**
     * 查询座位预约列表
     * 
     * @param lrReservation 座位预约
     * @return 座位预约集合
     */
    public List<LrReservation> selectLrReservationList(LrReservation lrReservation);

    /**
     * 新增座位预约
     * 
     * @param lrReservation 座位预约
     * @return 结果
     */
    public int insertLrReservation(LrReservation lrReservation);

    /**
     * 修改座位预约
     * 
     * @param lrReservation 座位预约
     * @return 结果
     */
    public int updateLrReservation(LrReservation lrReservation);

    /**
     * 批量删除座位预约
     * 
     * @param reservationIds 需要删除的座位预约主键集合
     * @return 结果
     */
    public int deleteLrReservationByReservationIds(String reservationIds);

    /**
     * 删除座位预约信息
     * 
     * @param reservationId 座位预约主键
     * @return 结果
     */
    public int deleteLrReservationByReservationId(Long reservationId);
}
