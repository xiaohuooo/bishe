package com.ruoyi.system.domain.vo;

import com.ruoyi.common.annotation.Excel;

public class LrSeatVO {
    /** 主键 */
    private Long seatId;

    /** 座位信息 */
    @Excel(name = "座位信息")
    private String seatLocation;

    /** 0为可用，1为故障不可用 */
    @Excel(name = "0为可用，1为故障不可用,")
    private String seatStatus;

    /** 座位编号 */
    @Excel(name = "座位编号")
    private String seatNumber;


}
