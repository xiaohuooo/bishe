package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.LrSeatMapper;
import com.ruoyi.system.domain.LrSeat;
import com.ruoyi.system.service.ILrSeatService;
import com.ruoyi.common.core.text.Convert;

/**
 * 座位管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-03-06
 */
@Service
public class LrSeatServiceImpl implements ILrSeatService 
{
    @Autowired
    private LrSeatMapper lrSeatMapper;

    /**
     * 查询座位管理
     * 
     * @param seatId 座位管理主键
     * @return 座位管理
     */
    @Override
    public LrSeat selectLrSeatBySeatId(Long seatId)
    {
        return lrSeatMapper.selectLrSeatBySeatId(seatId);
    }

    /**
     * 查询座位管理列表
     * 
     * @param lrSeat 座位管理
     * @return 座位管理
     */
    @Override
    public List<LrSeat> selectLrSeatList(LrSeat lrSeat)
    {
        return lrSeatMapper.selectLrSeatList(lrSeat);
    }

    @Override
    public List<LrSeat> selectLrSeatReservationList(LrSeat lrSeat)
    {
        return lrSeatMapper.selectLrSeatReservationList(lrSeat);
    }

    /**
     * 新增座位管理
     * 
     * @param lrSeat 座位管理
     * @return 结果
     */
    @Override
    public int insertLrSeat(LrSeat lrSeat)
    {
        lrSeat.setCreateTime(DateUtils.getNowDate());
        return lrSeatMapper.insertLrSeat(lrSeat);
    }

    /**
     * 修改座位管理
     * 
     * @param lrSeat 座位管理
     * @return 结果
     */
    @Override
    public int updateLrSeat(LrSeat lrSeat)
    {
        lrSeat.setUpdateTime(DateUtils.getNowDate());
        return lrSeatMapper.updateLrSeat(lrSeat);
    }

    /**
     * 批量删除座位管理
     * 
     * @param seatIds 需要删除的座位管理主键
     * @return 结果
     */
    @Override
    public int deleteLrSeatBySeatIds(String seatIds)
    {
        return lrSeatMapper.deleteLrSeatBySeatIds(Convert.toStrArray(seatIds));
    }

    /**
     * 删除座位管理信息
     * 
     * @param seatId 座位管理主键
     * @return 结果
     */
    @Override
    public int deleteLrSeatBySeatId(Long seatId)
    {
        return lrSeatMapper.deleteLrSeatBySeatId(seatId);
    }
}
