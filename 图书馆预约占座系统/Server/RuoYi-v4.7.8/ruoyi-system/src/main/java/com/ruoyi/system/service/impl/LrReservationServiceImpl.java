package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.LrReservationMapper;
import com.ruoyi.system.domain.LrReservation;
import com.ruoyi.system.service.ILrReservationService;
import com.ruoyi.common.core.text.Convert;

/**
 * 座位预约Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-03-06
 */
@Service
public class LrReservationServiceImpl implements ILrReservationService 
{
    @Autowired
    private LrReservationMapper lrReservationMapper;

    /**
     * 查询座位预约
     * 
     * @param reservationId 座位预约主键
     * @return 座位预约
     */
    @Override
    public LrReservation selectLrReservationByReservationId(Long reservationId)
    {
        return lrReservationMapper.selectLrReservationByReservationId(reservationId);
    }

    /**
     * 查询座位预约列表
     * 
     * @param lrReservation 座位预约
     * @return 座位预约
     */
    @Override
    public List<LrReservation> selectLrReservationList(LrReservation lrReservation)
    {
        lrReservation.setStudentId(ShiroUtils.getUserId());
        return lrReservationMapper.selectLrReservationList(lrReservation);
    }

    /**
     * 新增座位预约
     * 
     * @param lrReservation 座位预约
     * @return 结果
     */
    @Override
    public int insertLrReservation(LrReservation lrReservation)
    {
        lrReservation.setCreateTime(DateUtils.getNowDate());
        return lrReservationMapper.insertLrReservation(lrReservation);
    }

    /**
     * 修改座位预约
     * 
     * @param lrReservation 座位预约
     * @return 结果
     */
    @Override
    public int updateLrReservation(LrReservation lrReservation)
    {
        lrReservation.setUpdateTime(DateUtils.getNowDate());
        return lrReservationMapper.updateLrReservation(lrReservation);
    }

    /**
     * 批量删除座位预约
     * 
     * @param reservationIds 需要删除的座位预约主键
     * @return 结果
     */
    @Override
    public int deleteLrReservationByReservationIds(String reservationIds)
    {
        return lrReservationMapper.deleteLrReservationByReservationIds(Convert.toStrArray(reservationIds));
    }

    /**
     * 删除座位预约信息
     * 
     * @param reservationId 座位预约主键
     * @return 结果
     */
    @Override
    public int deleteLrReservationByReservationId(Long reservationId)
    {
        return lrReservationMapper.deleteLrReservationByReservationId(reservationId);
    }
}
