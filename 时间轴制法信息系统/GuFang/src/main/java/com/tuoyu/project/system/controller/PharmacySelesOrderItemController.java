package com.tuoyu.project.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tuoyu.framework.aspectj.lang.annotation.Log;
import com.tuoyu.framework.aspectj.lang.enums.BusinessType;
import com.tuoyu.project.system.domain.PharmacySelesOrderItem;
import com.tuoyu.project.system.service.IPharmacySelesOrderItemService;
import com.tuoyu.framework.web.controller.BaseController;
import com.tuoyu.framework.web.domain.AjaxResult;
import com.tuoyu.common.utils.poi.ExcelUtil;
import com.tuoyu.framework.web.page.TableDataInfo;

/**
 * 药品销售明细Controller
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
@RestController
@RequestMapping("/system/salesItem")
public class PharmacySelesOrderItemController extends BaseController
{
    @Autowired
    private IPharmacySelesOrderItemService pharmacySelesOrderItemService;

    /**
     * 查询药品销售明细列表
     */
    @PreAuthorize("@ss.hasPermi('system:salesItem:list')")
    @GetMapping("/list")
    public TableDataInfo list(PharmacySelesOrderItem pharmacySelesOrderItem)
    {
        startPage();
        List<PharmacySelesOrderItem> list = pharmacySelesOrderItemService.selectPharmacySelesOrderItemList(pharmacySelesOrderItem);
        return getDataTable(list);
    }

    /**
     * 导出药品销售明细列表
     */
    @PreAuthorize("@ss.hasPermi('system:salesItem:export')")
    @Log(title = "药品销售明细", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PharmacySelesOrderItem pharmacySelesOrderItem)
    {
        List<PharmacySelesOrderItem> list = pharmacySelesOrderItemService.selectPharmacySelesOrderItemList(pharmacySelesOrderItem);
        ExcelUtil<PharmacySelesOrderItem> util = new ExcelUtil<PharmacySelesOrderItem>(PharmacySelesOrderItem.class);
        util.exportExcel(response, list, "药品销售明细数据");
    }

    /**
     * 获取药品销售明细详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:salesItem:query')")
    @GetMapping(value = "/{salesDetailId}")
    public AjaxResult getInfo(@PathVariable("salesDetailId") Long salesDetailId)
    {
        return success(pharmacySelesOrderItemService.selectPharmacySelesOrderItemBySalesDetailId(salesDetailId));
    }

    /**
     * 新增药品销售明细
     */
    @PreAuthorize("@ss.hasPermi('system:salesItem:add')")
    @Log(title = "药品销售明细", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PharmacySelesOrderItem pharmacySelesOrderItem)
    {
        return toAjax(pharmacySelesOrderItemService.insertPharmacySelesOrderItem(pharmacySelesOrderItem));
    }

    /**
     * 修改药品销售明细
     */
    @PreAuthorize("@ss.hasPermi('system:salesItem:edit')")
    @Log(title = "药品销售明细", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PharmacySelesOrderItem pharmacySelesOrderItem)
    {
        return toAjax(pharmacySelesOrderItemService.updatePharmacySelesOrderItem(pharmacySelesOrderItem));
    }

    /**
     * 删除药品销售明细
     */
    @PreAuthorize("@ss.hasPermi('system:salesItem:remove')")
    @Log(title = "药品销售明细", businessType = BusinessType.DELETE)
	@DeleteMapping("/{salesDetailIds}")
    public AjaxResult remove(@PathVariable Long[] salesDetailIds)
    {
        return toAjax(pharmacySelesOrderItemService.deletePharmacySelesOrderItemBySalesDetailIds(salesDetailIds));
    }
}
