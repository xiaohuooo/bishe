package com.tuoyu.project.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tuoyu.project.system.mapper.PharmacyBuyOrderMapper;
import com.tuoyu.project.system.domain.PharmacyBuyOrder;
import com.tuoyu.project.system.service.IPharmacyBuyOrderService;

/**
 * 药品采购Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
@Service
public class PharmacyBuyOrderServiceImpl implements IPharmacyBuyOrderService 
{
    @Autowired
    private PharmacyBuyOrderMapper pharmacyBuyOrderMapper;

    /**
     * 查询药品采购
     * 
     * @param purchaseOrderId 药品采购主键
     * @return 药品采购
     */
    @Override
    public PharmacyBuyOrder selectPharmacyBuyOrderByPurchaseOrderId(Long purchaseOrderId)
    {
        return pharmacyBuyOrderMapper.selectPharmacyBuyOrderByPurchaseOrderId(purchaseOrderId);
    }

    /**
     * 查询药品采购列表
     * 
     * @param pharmacyBuyOrder 药品采购
     * @return 药品采购
     */
    @Override
    public List<PharmacyBuyOrder> selectPharmacyBuyOrderList(PharmacyBuyOrder pharmacyBuyOrder)
    {
        return pharmacyBuyOrderMapper.selectPharmacyBuyOrderList(pharmacyBuyOrder);
    }

    /**
     * 新增药品采购
     * 
     * @param pharmacyBuyOrder 药品采购
     * @return 结果
     */
    @Override
    public int insertPharmacyBuyOrder(PharmacyBuyOrder pharmacyBuyOrder)
    {
        return pharmacyBuyOrderMapper.insertPharmacyBuyOrder(pharmacyBuyOrder);
    }

    /**
     * 修改药品采购
     * 
     * @param pharmacyBuyOrder 药品采购
     * @return 结果
     */
    @Override
    public int updatePharmacyBuyOrder(PharmacyBuyOrder pharmacyBuyOrder)
    {
        return pharmacyBuyOrderMapper.updatePharmacyBuyOrder(pharmacyBuyOrder);
    }

    /**
     * 批量删除药品采购
     * 
     * @param purchaseOrderIds 需要删除的药品采购主键
     * @return 结果
     */
    @Override
    public int deletePharmacyBuyOrderByPurchaseOrderIds(Long[] purchaseOrderIds)
    {
        return pharmacyBuyOrderMapper.deletePharmacyBuyOrderByPurchaseOrderIds(purchaseOrderIds);
    }

    /**
     * 删除药品采购信息
     * 
     * @param purchaseOrderId 药品采购主键
     * @return 结果
     */
    @Override
    public int deletePharmacyBuyOrderByPurchaseOrderId(Long purchaseOrderId)
    {
        return pharmacyBuyOrderMapper.deletePharmacyBuyOrderByPurchaseOrderId(purchaseOrderId);
    }
}
