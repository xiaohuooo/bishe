package com.tuoyu.project.system.mapper;

import java.util.List;
import com.tuoyu.project.system.domain.PharmacyPatientRecord;

/**
 * 患者就诊记Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
public interface PharmacyPatientRecordMapper 
{
    /**
     * 查询患者就诊记
     * 
     * @param consultationRecordId 患者就诊记主键
     * @return 患者就诊记
     */
    public PharmacyPatientRecord selectPharmacyPatientRecordByConsultationRecordId(Long consultationRecordId);

    /**
     * 查询患者就诊记列表
     * 
     * @param pharmacyPatientRecord 患者就诊记
     * @return 患者就诊记集合
     */
    public List<PharmacyPatientRecord> selectPharmacyPatientRecordList(PharmacyPatientRecord pharmacyPatientRecord);

    /**
     * 新增患者就诊记
     * 
     * @param pharmacyPatientRecord 患者就诊记
     * @return 结果
     */
    public int insertPharmacyPatientRecord(PharmacyPatientRecord pharmacyPatientRecord);

    /**
     * 修改患者就诊记
     * 
     * @param pharmacyPatientRecord 患者就诊记
     * @return 结果
     */
    public int updatePharmacyPatientRecord(PharmacyPatientRecord pharmacyPatientRecord);

    /**
     * 删除患者就诊记
     * 
     * @param consultationRecordId 患者就诊记主键
     * @return 结果
     */
    public int deletePharmacyPatientRecordByConsultationRecordId(Long consultationRecordId);

    /**
     * 批量删除患者就诊记
     * 
     * @param consultationRecordIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePharmacyPatientRecordByConsultationRecordIds(Long[] consultationRecordIds);
}
