package com.tuoyu.project.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.tuoyu.framework.aspectj.lang.annotation.Excel;
import com.tuoyu.framework.web.domain.BaseEntity;

/**
 * 药品采购明细对象 pharmacy_buy_order_item
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
public class PharmacyBuyOrderItem extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 采购明细ID */
    private Long purchaseDetailId;

    /** 药品ID */
    @Excel(name = "药品ID")
    private Long drugId;

    /** 生产日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "生产日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date manufactureDate;

    /** 有效期 */
    @Excel(name = "有效期")
    private Long expirationDate;

    /** 药品数量 */
    @Excel(name = "药品数量")
    private Long drugQuantity;

    /** 生产厂家名称 */
    @Excel(name = "生产厂家名称")
    private String manufacturerName;

    /** 进货价 */
    @Excel(name = "进货价")
    private BigDecimal purchasePrice;

    /** 销售价 */
    @Excel(name = "销售价")
    private BigDecimal sellingPrice;

    /** 存放位置 */
    @Excel(name = "存放位置")
    private String storageLocation;

    /** 采购单ID */
    @Excel(name = "采购单ID")
    private Long purchaseOrderId;

    public void setPurchaseDetailId(Long purchaseDetailId) 
    {
        this.purchaseDetailId = purchaseDetailId;
    }

    public Long getPurchaseDetailId() 
    {
        return purchaseDetailId;
    }
    public void setDrugId(Long drugId) 
    {
        this.drugId = drugId;
    }

    public Long getDrugId() 
    {
        return drugId;
    }
    public void setManufactureDate(Date manufactureDate) 
    {
        this.manufactureDate = manufactureDate;
    }

    public Date getManufactureDate() 
    {
        return manufactureDate;
    }
    public void setExpirationDate(Long expirationDate) 
    {
        this.expirationDate = expirationDate;
    }

    public Long getExpirationDate() 
    {
        return expirationDate;
    }
    public void setDrugQuantity(Long drugQuantity) 
    {
        this.drugQuantity = drugQuantity;
    }

    public Long getDrugQuantity() 
    {
        return drugQuantity;
    }
    public void setManufacturerName(String manufacturerName) 
    {
        this.manufacturerName = manufacturerName;
    }

    public String getManufacturerName() 
    {
        return manufacturerName;
    }
    public void setPurchasePrice(BigDecimal purchasePrice) 
    {
        this.purchasePrice = purchasePrice;
    }

    public BigDecimal getPurchasePrice() 
    {
        return purchasePrice;
    }
    public void setSellingPrice(BigDecimal sellingPrice) 
    {
        this.sellingPrice = sellingPrice;
    }

    public BigDecimal getSellingPrice() 
    {
        return sellingPrice;
    }
    public void setStorageLocation(String storageLocation) 
    {
        this.storageLocation = storageLocation;
    }

    public String getStorageLocation() 
    {
        return storageLocation;
    }
    public void setPurchaseOrderId(Long purchaseOrderId) 
    {
        this.purchaseOrderId = purchaseOrderId;
    }

    public Long getPurchaseOrderId() 
    {
        return purchaseOrderId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("purchaseDetailId", getPurchaseDetailId())
            .append("drugId", getDrugId())
            .append("manufactureDate", getManufactureDate())
            .append("expirationDate", getExpirationDate())
            .append("drugQuantity", getDrugQuantity())
            .append("manufacturerName", getManufacturerName())
            .append("purchasePrice", getPurchasePrice())
            .append("sellingPrice", getSellingPrice())
            .append("storageLocation", getStorageLocation())
            .append("purchaseOrderId", getPurchaseOrderId())
            .toString();
    }
}
