package com.tuoyu.project.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.tuoyu.framework.aspectj.lang.annotation.Excel;
import com.tuoyu.framework.web.domain.BaseEntity;

/**
 * 患者就诊记对象 pharmacy_patient_record
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
public class PharmacyPatientRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 问诊记录ID */
    private Long consultationRecordId;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 诊断 */
    @Excel(name = "诊断")
    private String diagnosis;

    /** 医师ID */
    @Excel(name = "医师ID")
    private Long physicianId;

    /** 医师姓名 */
    @Excel(name = "医师姓名")
    private String physicianName;

    /** 问诊时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "问诊时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date consultationDatetime;

    /** 药品销售单ID */
    @Excel(name = "药品销售单ID")
    private Long drugSalesOrderId;

    /** 档案ID */
    @Excel(name = "档案ID")
    private Long recordId;

    public void setConsultationRecordId(Long consultationRecordId) 
    {
        this.consultationRecordId = consultationRecordId;
    }

    public Long getConsultationRecordId() 
    {
        return consultationRecordId;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setDiagnosis(String diagnosis) 
    {
        this.diagnosis = diagnosis;
    }

    public String getDiagnosis() 
    {
        return diagnosis;
    }
    public void setPhysicianId(Long physicianId) 
    {
        this.physicianId = physicianId;
    }

    public Long getPhysicianId() 
    {
        return physicianId;
    }
    public void setPhysicianName(String physicianName) 
    {
        this.physicianName = physicianName;
    }

    public String getPhysicianName() 
    {
        return physicianName;
    }
    public void setConsultationDatetime(Date consultationDatetime) 
    {
        this.consultationDatetime = consultationDatetime;
    }

    public Date getConsultationDatetime() 
    {
        return consultationDatetime;
    }
    public void setDrugSalesOrderId(Long drugSalesOrderId) 
    {
        this.drugSalesOrderId = drugSalesOrderId;
    }

    public Long getDrugSalesOrderId() 
    {
        return drugSalesOrderId;
    }
    public void setRecordId(Long recordId) 
    {
        this.recordId = recordId;
    }

    public Long getRecordId() 
    {
        return recordId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("consultationRecordId", getConsultationRecordId())
            .append("status", getStatus())
            .append("diagnosis", getDiagnosis())
            .append("physicianId", getPhysicianId())
            .append("physicianName", getPhysicianName())
            .append("consultationDatetime", getConsultationDatetime())
            .append("drugSalesOrderId", getDrugSalesOrderId())
            .append("recordId", getRecordId())
            .toString();
    }
}
