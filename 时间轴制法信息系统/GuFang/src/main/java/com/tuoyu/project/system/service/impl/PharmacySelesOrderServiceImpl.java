package com.tuoyu.project.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tuoyu.project.system.mapper.PharmacySelesOrderMapper;
import com.tuoyu.project.system.domain.PharmacySelesOrder;
import com.tuoyu.project.system.service.IPharmacySelesOrderService;

/**
 * 药品销售Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
@Service
public class PharmacySelesOrderServiceImpl implements IPharmacySelesOrderService 
{
    @Autowired
    private PharmacySelesOrderMapper pharmacySelesOrderMapper;

    /**
     * 查询药品销售
     * 
     * @param salesOrderId 药品销售主键
     * @return 药品销售
     */
    @Override
    public PharmacySelesOrder selectPharmacySelesOrderBySalesOrderId(Long salesOrderId)
    {
        return pharmacySelesOrderMapper.selectPharmacySelesOrderBySalesOrderId(salesOrderId);
    }

    /**
     * 查询药品销售列表
     * 
     * @param pharmacySelesOrder 药品销售
     * @return 药品销售
     */
    @Override
    public List<PharmacySelesOrder> selectPharmacySelesOrderList(PharmacySelesOrder pharmacySelesOrder)
    {
        return pharmacySelesOrderMapper.selectPharmacySelesOrderList(pharmacySelesOrder);
    }

    /**
     * 新增药品销售
     * 
     * @param pharmacySelesOrder 药品销售
     * @return 结果
     */
    @Override
    public int insertPharmacySelesOrder(PharmacySelesOrder pharmacySelesOrder)
    {
        return pharmacySelesOrderMapper.insertPharmacySelesOrder(pharmacySelesOrder);
    }

    /**
     * 修改药品销售
     * 
     * @param pharmacySelesOrder 药品销售
     * @return 结果
     */
    @Override
    public int updatePharmacySelesOrder(PharmacySelesOrder pharmacySelesOrder)
    {
        return pharmacySelesOrderMapper.updatePharmacySelesOrder(pharmacySelesOrder);
    }

    /**
     * 批量删除药品销售
     * 
     * @param salesOrderIds 需要删除的药品销售主键
     * @return 结果
     */
    @Override
    public int deletePharmacySelesOrderBySalesOrderIds(Long[] salesOrderIds)
    {
        return pharmacySelesOrderMapper.deletePharmacySelesOrderBySalesOrderIds(salesOrderIds);
    }

    /**
     * 删除药品销售信息
     * 
     * @param salesOrderId 药品销售主键
     * @return 结果
     */
    @Override
    public int deletePharmacySelesOrderBySalesOrderId(Long salesOrderId)
    {
        return pharmacySelesOrderMapper.deletePharmacySelesOrderBySalesOrderId(salesOrderId);
    }
}
