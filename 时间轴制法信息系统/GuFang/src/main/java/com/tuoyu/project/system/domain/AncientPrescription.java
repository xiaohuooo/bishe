package com.tuoyu.project.system.domain;

import java.util.List;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.tuoyu.framework.aspectj.lang.annotation.Excel;
import com.tuoyu.framework.web.domain.BaseEntity;

/**
 * 古方对象 ancientPrescription
 * 
 * @author ruoyi
 * @date 2024-04-24
 */
public class AncientPrescription extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 古方id */
    private Long id;

    /** 古方名称 */
    @Excel(name = "古方名称")
    private String name;

    /** 临床应用 */
    @Excel(name = "临床应用")
    private String clinical;

    /** 排序 */
    @Excel(name = "排序")
    private Long ordera;

    /** 古方历史信息 */
    private List<AncientPrescriptionHistory> ancientPrescriptionHistoryList;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setClinical(String clinical) 
    {
        this.clinical = clinical;
    }

    public String getClinical() 
    {
        return clinical;
    }
    public void setOrdera(Long ordera) 
    {
        this.ordera = ordera;
    }

    public Long getOrdera() 
    {
        return ordera;
    }

    public List<AncientPrescriptionHistory> getAncientPrescriptionHistoryList()
    {
        return ancientPrescriptionHistoryList;
    }

    public void setAncientPrescriptionHistoryList(List<AncientPrescriptionHistory> ancientPrescriptionHistoryList)
    {
        this.ancientPrescriptionHistoryList = ancientPrescriptionHistoryList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("clinical", getClinical())
            .append("ordera", getOrdera())
            .append("ancientPrescriptionHistoryList", getAncientPrescriptionHistoryList())
            .toString();
    }
}
