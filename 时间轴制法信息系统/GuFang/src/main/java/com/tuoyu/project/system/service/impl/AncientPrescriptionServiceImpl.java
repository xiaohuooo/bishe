package com.tuoyu.project.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import com.tuoyu.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.tuoyu.project.system.domain.AncientPrescriptionHistory;
import com.tuoyu.project.system.mapper.AncientPrescriptionMapper;
import com.tuoyu.project.system.domain.AncientPrescription;
import com.tuoyu.project.system.service.IAncientPrescriptionService;

/**
 * 古方Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-24
 */
@Service
public class AncientPrescriptionServiceImpl implements IAncientPrescriptionService 
{
    @Autowired
    private AncientPrescriptionMapper ancientPrescriptionMapper;

    /**
     * 查询古方
     * 
     * @param id 古方主键
     * @return 古方
     */
    @Override
    public AncientPrescription selectAncientPrescriptionById(Long id)
    {
        return ancientPrescriptionMapper.selectAncientPrescriptionById(id);
    }

    /**
     * 查询古方列表
     * 
     * @param ancientPrescription 古方
     * @return 古方
     */
    @Override
    public List<AncientPrescription> selectAncientPrescriptionList(AncientPrescription ancientPrescription)
    {
        return ancientPrescriptionMapper.selectAncientPrescriptionList(ancientPrescription);
    }

    /**
     * 新增古方
     * 
     * @param ancientPrescription 古方
     * @return 结果
     */
    @Transactional
    @Override
    public int insertAncientPrescription(AncientPrescription ancientPrescription)
    {
        int rows = ancientPrescriptionMapper.insertAncientPrescription(ancientPrescription);
        insertAncientPrescriptionHistory(ancientPrescription);
        return rows;
    }

    /**
     * 修改古方
     * 
     * @param ancientPrescription 古方
     * @return 结果
     */
    @Transactional
    @Override
    public int updateAncientPrescription(AncientPrescription ancientPrescription)
    {
        ancientPrescriptionMapper.deleteAncientPrescriptionHistoryByAid(ancientPrescription.getId());
        insertAncientPrescriptionHistory(ancientPrescription);
        return ancientPrescriptionMapper.updateAncientPrescription(ancientPrescription);
    }

    /**
     * 批量删除古方
     * 
     * @param ids 需要删除的古方主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteAncientPrescriptionByIds(Long[] ids)
    {
        ancientPrescriptionMapper.deleteAncientPrescriptionHistoryByAids(ids);
        return ancientPrescriptionMapper.deleteAncientPrescriptionByIds(ids);
    }

    /**
     * 删除古方信息
     * 
     * @param id 古方主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteAncientPrescriptionById(Long id)
    {
        ancientPrescriptionMapper.deleteAncientPrescriptionHistoryByAid(id);
        return ancientPrescriptionMapper.deleteAncientPrescriptionById(id);
    }

    /**
     * 新增古方历史信息
     * 
     * @param ancientPrescription 古方对象
     */
    public void insertAncientPrescriptionHistory(AncientPrescription ancientPrescription)
    {
        List<AncientPrescriptionHistory> ancientPrescriptionHistoryList = ancientPrescription.getAncientPrescriptionHistoryList();
        Long id = ancientPrescription.getId();
        if (StringUtils.isNotNull(ancientPrescriptionHistoryList))
        {
            List<AncientPrescriptionHistory> list = new ArrayList<AncientPrescriptionHistory>();
            for (AncientPrescriptionHistory ancientPrescriptionHistory : ancientPrescriptionHistoryList)
            {
                ancientPrescriptionHistory.setAid(id);
                list.add(ancientPrescriptionHistory);
            }
            if (list.size() > 0)
            {
                ancientPrescriptionMapper.batchAncientPrescriptionHistory(list);
            }
        }
    }
}
