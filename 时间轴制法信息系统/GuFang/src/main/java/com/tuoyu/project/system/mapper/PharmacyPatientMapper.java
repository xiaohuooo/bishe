package com.tuoyu.project.system.mapper;

import java.util.List;
import com.tuoyu.project.system.domain.PharmacyPatient;

/**
 * 患者档案Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
public interface PharmacyPatientMapper 
{
    /**
     * 查询患者档案
     * 
     * @param recordId 患者档案主键
     * @return 患者档案
     */
    public PharmacyPatient selectPharmacyPatientByRecordId(Long recordId);

    /**
     * 查询患者档案列表
     * 
     * @param pharmacyPatient 患者档案
     * @return 患者档案集合
     */
    public List<PharmacyPatient> selectPharmacyPatientList(PharmacyPatient pharmacyPatient);

    /**
     * 新增患者档案
     * 
     * @param pharmacyPatient 患者档案
     * @return 结果
     */
    public int insertPharmacyPatient(PharmacyPatient pharmacyPatient);

    /**
     * 修改患者档案
     * 
     * @param pharmacyPatient 患者档案
     * @return 结果
     */
    public int updatePharmacyPatient(PharmacyPatient pharmacyPatient);

    /**
     * 删除患者档案
     * 
     * @param recordId 患者档案主键
     * @return 结果
     */
    public int deletePharmacyPatientByRecordId(Long recordId);

    /**
     * 批量删除患者档案
     * 
     * @param recordIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePharmacyPatientByRecordIds(Long[] recordIds);
}
