package com.tuoyu.project.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tuoyu.project.system.mapper.PharmacyPatientRecordMapper;
import com.tuoyu.project.system.domain.PharmacyPatientRecord;
import com.tuoyu.project.system.service.IPharmacyPatientRecordService;

/**
 * 患者就诊记Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
@Service
public class PharmacyPatientRecordServiceImpl implements IPharmacyPatientRecordService 
{
    @Autowired
    private PharmacyPatientRecordMapper pharmacyPatientRecordMapper;

    /**
     * 查询患者就诊记
     * 
     * @param consultationRecordId 患者就诊记主键
     * @return 患者就诊记
     */
    @Override
    public PharmacyPatientRecord selectPharmacyPatientRecordByConsultationRecordId(Long consultationRecordId)
    {
        return pharmacyPatientRecordMapper.selectPharmacyPatientRecordByConsultationRecordId(consultationRecordId);
    }

    /**
     * 查询患者就诊记列表
     * 
     * @param pharmacyPatientRecord 患者就诊记
     * @return 患者就诊记
     */
    @Override
    public List<PharmacyPatientRecord> selectPharmacyPatientRecordList(PharmacyPatientRecord pharmacyPatientRecord)
    {
        return pharmacyPatientRecordMapper.selectPharmacyPatientRecordList(pharmacyPatientRecord);
    }

    /**
     * 新增患者就诊记
     * 
     * @param pharmacyPatientRecord 患者就诊记
     * @return 结果
     */
    @Override
    public int insertPharmacyPatientRecord(PharmacyPatientRecord pharmacyPatientRecord)
    {
        return pharmacyPatientRecordMapper.insertPharmacyPatientRecord(pharmacyPatientRecord);
    }

    /**
     * 修改患者就诊记
     * 
     * @param pharmacyPatientRecord 患者就诊记
     * @return 结果
     */
    @Override
    public int updatePharmacyPatientRecord(PharmacyPatientRecord pharmacyPatientRecord)
    {
        return pharmacyPatientRecordMapper.updatePharmacyPatientRecord(pharmacyPatientRecord);
    }

    /**
     * 批量删除患者就诊记
     * 
     * @param consultationRecordIds 需要删除的患者就诊记主键
     * @return 结果
     */
    @Override
    public int deletePharmacyPatientRecordByConsultationRecordIds(Long[] consultationRecordIds)
    {
        return pharmacyPatientRecordMapper.deletePharmacyPatientRecordByConsultationRecordIds(consultationRecordIds);
    }

    /**
     * 删除患者就诊记信息
     * 
     * @param consultationRecordId 患者就诊记主键
     * @return 结果
     */
    @Override
    public int deletePharmacyPatientRecordByConsultationRecordId(Long consultationRecordId)
    {
        return pharmacyPatientRecordMapper.deletePharmacyPatientRecordByConsultationRecordId(consultationRecordId);
    }
}
