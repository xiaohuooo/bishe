package com.tuoyu.project.system.mapper;

import java.util.List;
import com.tuoyu.project.system.domain.AncientPrescription;
import com.tuoyu.project.system.domain.AncientPrescriptionHistory;

/**
 * 古方Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-24
 */
public interface AncientPrescriptionMapper 
{
    /**
     * 查询古方
     * 
     * @param id 古方主键
     * @return 古方
     */
    public AncientPrescription selectAncientPrescriptionById(Long id);

    /**
     * 查询古方列表
     * 
     * @param ancientPrescription 古方
     * @return 古方集合
     */
    public List<AncientPrescription> selectAncientPrescriptionList(AncientPrescription ancientPrescription);

    /**
     * 新增古方
     * 
     * @param ancientPrescription 古方
     * @return 结果
     */
    public int insertAncientPrescription(AncientPrescription ancientPrescription);

    /**
     * 修改古方
     * 
     * @param ancientPrescription 古方
     * @return 结果
     */
    public int updateAncientPrescription(AncientPrescription ancientPrescription);

    /**
     * 删除古方
     * 
     * @param id 古方主键
     * @return 结果
     */
    public int deleteAncientPrescriptionById(Long id);

    /**
     * 批量删除古方
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAncientPrescriptionByIds(Long[] ids);

    /**
     * 批量删除古方历史
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAncientPrescriptionHistoryByAids(Long[] ids);
    
    /**
     * 批量新增古方历史
     * 
     * @param ancientPrescriptionHistoryList 古方历史列表
     * @return 结果
     */
    public int batchAncientPrescriptionHistory(List<AncientPrescriptionHistory> ancientPrescriptionHistoryList);
    

    /**
     * 通过古方主键删除古方历史信息
     * 
     * @param id 古方ID
     * @return 结果
     */
    public int deleteAncientPrescriptionHistoryByAid(Long id);
}
