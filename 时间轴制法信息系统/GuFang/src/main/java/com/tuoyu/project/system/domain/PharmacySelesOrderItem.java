package com.tuoyu.project.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.tuoyu.framework.aspectj.lang.annotation.Excel;
import com.tuoyu.framework.web.domain.BaseEntity;

/**
 * 药品销售明细对象 pharmacy_seles_order_item
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
public class PharmacySelesOrderItem extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 销售明细ID */
    private Long salesDetailId;

    /** 药品ID */
    @Excel(name = "药品ID")
    private Long drugId;

    /** 生产日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "生产日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date manufactureDate;

    /** 有效期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "有效期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date expirationDate;

    /** 药品数量 */
    @Excel(name = "药品数量")
    private String drugQuantity;

    /** 生产厂家名称 */
    @Excel(name = "生产厂家名称")
    private String manufacturerName;

    /** 销售价 */
    @Excel(name = "销售价")
    private BigDecimal sellingPrice;

    /** 总额 */
    @Excel(name = "总额")
    private String totalAmount;

    /** 存放位置 */
    @Excel(name = "存放位置")
    private String storageLocation;

    /** 销售单ID */
    @Excel(name = "销售单ID")
    private Long salesOrderId;

    public void setSalesDetailId(Long salesDetailId) 
    {
        this.salesDetailId = salesDetailId;
    }

    public Long getSalesDetailId() 
    {
        return salesDetailId;
    }
    public void setDrugId(Long drugId) 
    {
        this.drugId = drugId;
    }

    public Long getDrugId() 
    {
        return drugId;
    }
    public void setManufactureDate(Date manufactureDate) 
    {
        this.manufactureDate = manufactureDate;
    }

    public Date getManufactureDate() 
    {
        return manufactureDate;
    }
    public void setExpirationDate(Date expirationDate) 
    {
        this.expirationDate = expirationDate;
    }

    public Date getExpirationDate() 
    {
        return expirationDate;
    }
    public void setDrugQuantity(String drugQuantity) 
    {
        this.drugQuantity = drugQuantity;
    }

    public String getDrugQuantity() 
    {
        return drugQuantity;
    }
    public void setManufacturerName(String manufacturerName) 
    {
        this.manufacturerName = manufacturerName;
    }

    public String getManufacturerName() 
    {
        return manufacturerName;
    }
    public void setSellingPrice(BigDecimal sellingPrice) 
    {
        this.sellingPrice = sellingPrice;
    }

    public BigDecimal getSellingPrice() 
    {
        return sellingPrice;
    }
    public void setTotalAmount(String totalAmount) 
    {
        this.totalAmount = totalAmount;
    }

    public String getTotalAmount() 
    {
        return totalAmount;
    }
    public void setStorageLocation(String storageLocation) 
    {
        this.storageLocation = storageLocation;
    }

    public String getStorageLocation() 
    {
        return storageLocation;
    }
    public void setSalesOrderId(Long salesOrderId) 
    {
        this.salesOrderId = salesOrderId;
    }

    public Long getSalesOrderId() 
    {
        return salesOrderId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("salesDetailId", getSalesDetailId())
            .append("drugId", getDrugId())
            .append("manufactureDate", getManufactureDate())
            .append("expirationDate", getExpirationDate())
            .append("drugQuantity", getDrugQuantity())
            .append("manufacturerName", getManufacturerName())
            .append("sellingPrice", getSellingPrice())
            .append("totalAmount", getTotalAmount())
            .append("storageLocation", getStorageLocation())
            .append("salesOrderId", getSalesOrderId())
            .toString();
    }
}
