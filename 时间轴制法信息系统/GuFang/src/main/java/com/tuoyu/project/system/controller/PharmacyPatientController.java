package com.tuoyu.project.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tuoyu.framework.aspectj.lang.annotation.Log;
import com.tuoyu.framework.aspectj.lang.enums.BusinessType;
import com.tuoyu.project.system.domain.PharmacyPatient;
import com.tuoyu.project.system.service.IPharmacyPatientService;
import com.tuoyu.framework.web.controller.BaseController;
import com.tuoyu.framework.web.domain.AjaxResult;
import com.tuoyu.common.utils.poi.ExcelUtil;
import com.tuoyu.framework.web.page.TableDataInfo;

/**
 * 患者档案Controller
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
@RestController
@RequestMapping("/system/patient")
public class PharmacyPatientController extends BaseController
{
    @Autowired
    private IPharmacyPatientService pharmacyPatientService;

    /**
     * 查询患者档案列表
     */
    @PreAuthorize("@ss.hasPermi('system:patient:list')")
    @GetMapping("/list")
    public TableDataInfo list(PharmacyPatient pharmacyPatient)
    {
        startPage();
        List<PharmacyPatient> list = pharmacyPatientService.selectPharmacyPatientList(pharmacyPatient);
        return getDataTable(list);
    }

    /**
     * 导出患者档案列表
     */
    @PreAuthorize("@ss.hasPermi('system:patient:export')")
    @Log(title = "患者档案", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PharmacyPatient pharmacyPatient)
    {
        List<PharmacyPatient> list = pharmacyPatientService.selectPharmacyPatientList(pharmacyPatient);
        ExcelUtil<PharmacyPatient> util = new ExcelUtil<PharmacyPatient>(PharmacyPatient.class);
        util.exportExcel(response, list, "患者档案数据");
    }

    /**
     * 获取患者档案详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:patient:query')")
    @GetMapping(value = "/{recordId}")
    public AjaxResult getInfo(@PathVariable("recordId") Long recordId)
    {
        return success(pharmacyPatientService.selectPharmacyPatientByRecordId(recordId));
    }

    /**
     * 新增患者档案
     */
    @PreAuthorize("@ss.hasPermi('system:patient:add')")
    @Log(title = "患者档案", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PharmacyPatient pharmacyPatient)
    {
        return toAjax(pharmacyPatientService.insertPharmacyPatient(pharmacyPatient));
    }

    /**
     * 修改患者档案
     */
    @PreAuthorize("@ss.hasPermi('system:patient:edit')")
    @Log(title = "患者档案", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PharmacyPatient pharmacyPatient)
    {
        return toAjax(pharmacyPatientService.updatePharmacyPatient(pharmacyPatient));
    }

    /**
     * 删除患者档案
     */
    @PreAuthorize("@ss.hasPermi('system:patient:remove')")
    @Log(title = "患者档案", businessType = BusinessType.DELETE)
	@DeleteMapping("/{recordIds}")
    public AjaxResult remove(@PathVariable Long[] recordIds)
    {
        return toAjax(pharmacyPatientService.deletePharmacyPatientByRecordIds(recordIds));
    }
}
