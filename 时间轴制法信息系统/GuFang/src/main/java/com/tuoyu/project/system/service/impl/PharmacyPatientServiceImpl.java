package com.tuoyu.project.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tuoyu.project.system.mapper.PharmacyPatientMapper;
import com.tuoyu.project.system.domain.PharmacyPatient;
import com.tuoyu.project.system.service.IPharmacyPatientService;

/**
 * 患者档案Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
@Service
public class PharmacyPatientServiceImpl implements IPharmacyPatientService 
{
    @Autowired
    private PharmacyPatientMapper pharmacyPatientMapper;

    /**
     * 查询患者档案
     * 
     * @param recordId 患者档案主键
     * @return 患者档案
     */
    @Override
    public PharmacyPatient selectPharmacyPatientByRecordId(Long recordId)
    {
        return pharmacyPatientMapper.selectPharmacyPatientByRecordId(recordId);
    }

    /**
     * 查询患者档案列表
     * 
     * @param pharmacyPatient 患者档案
     * @return 患者档案
     */
    @Override
    public List<PharmacyPatient> selectPharmacyPatientList(PharmacyPatient pharmacyPatient)
    {
        return pharmacyPatientMapper.selectPharmacyPatientList(pharmacyPatient);
    }

    /**
     * 新增患者档案
     * 
     * @param pharmacyPatient 患者档案
     * @return 结果
     */
    @Override
    public int insertPharmacyPatient(PharmacyPatient pharmacyPatient)
    {
        return pharmacyPatientMapper.insertPharmacyPatient(pharmacyPatient);
    }

    /**
     * 修改患者档案
     * 
     * @param pharmacyPatient 患者档案
     * @return 结果
     */
    @Override
    public int updatePharmacyPatient(PharmacyPatient pharmacyPatient)
    {
        return pharmacyPatientMapper.updatePharmacyPatient(pharmacyPatient);
    }

    /**
     * 批量删除患者档案
     * 
     * @param recordIds 需要删除的患者档案主键
     * @return 结果
     */
    @Override
    public int deletePharmacyPatientByRecordIds(Long[] recordIds)
    {
        return pharmacyPatientMapper.deletePharmacyPatientByRecordIds(recordIds);
    }

    /**
     * 删除患者档案信息
     * 
     * @param recordId 患者档案主键
     * @return 结果
     */
    @Override
    public int deletePharmacyPatientByRecordId(Long recordId)
    {
        return pharmacyPatientMapper.deletePharmacyPatientByRecordId(recordId);
    }
}
