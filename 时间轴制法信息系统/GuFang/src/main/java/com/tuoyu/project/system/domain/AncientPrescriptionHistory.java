package com.tuoyu.project.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.tuoyu.framework.aspectj.lang.annotation.Excel;
import com.tuoyu.framework.web.domain.BaseEntity;

/**
 * 古方历史对象 ancientPrescriptionHistory
 * 
 * @author ruoyi
 * @date 2024-04-24
 */
public class AncientPrescriptionHistory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 历史id */
    private Long hid;

    /** 朝代 */
    @Excel(name = "朝代")
    private String dynasty;

    /** 古籍名称 */
    @Excel(name = "古籍名称")
    private String name;

    /** 作者 */
    @Excel(name = "作者")
    private String author;

    /** 方名 */
    @Excel(name = "方名")
    private String alias;

    /** 药物组成 */
    @Excel(name = "药物组成")
    private String fromto;

    /** 煎服法 */
    @Excel(name = "煎服法")
    private String decoction;

    /** 功效主治 */
    @Excel(name = "功效主治")
    private String indications;

    /** 古方id */
    @Excel(name = "古方id")
    private Long aid;

    /** 排序 */
    @Excel(name = "排序")
    private Long orderh;

    public void setHid(Long hid) 
    {
        this.hid = hid;
    }

    public Long getHid() 
    {
        return hid;
    }
    public void setDynasty(String dynasty) 
    {
        this.dynasty = dynasty;
    }

    public String getDynasty() 
    {
        return dynasty;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setAuthor(String author) 
    {
        this.author = author;
    }

    public String getAuthor() 
    {
        return author;
    }
    public void setAlias(String alias) 
    {
        this.alias = alias;
    }

    public String getAlias() 
    {
        return alias;
    }
    public void setFromto(String fromto) 
    {
        this.fromto = fromto;
    }

    public String getFromto() 
    {
        return fromto;
    }
    public void setDecoction(String decoction) 
    {
        this.decoction = decoction;
    }

    public String getDecoction() 
    {
        return decoction;
    }
    public void setIndications(String indications) 
    {
        this.indications = indications;
    }

    public String getIndications() 
    {
        return indications;
    }
    public void setAid(Long aid) 
    {
        this.aid = aid;
    }

    public Long getAid() 
    {
        return aid;
    }
    public void setOrderh(Long orderh) 
    {
        this.orderh = orderh;
    }

    public Long getOrderh() 
    {
        return orderh;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("hid", getHid())
            .append("dynasty", getDynasty())
            .append("name", getName())
            .append("author", getAuthor())
            .append("alias", getAlias())
            .append("fromto", getFromto())
            .append("decoction", getDecoction())
            .append("indications", getIndications())
            .append("aid", getAid())
            .append("orderh", getOrderh())
            .toString();
    }
}
