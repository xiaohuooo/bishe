package com.tuoyu.project.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.tuoyu.framework.aspectj.lang.annotation.Excel;
import com.tuoyu.framework.web.domain.BaseEntity;

/**
 * 药品信息对象 pharmacy_medicine
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
public class PharmacyMedicine extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 药品ID */
    private Long drugId;

    /** 药品名称 */
    @Excel(name = "药品名称")
    private String drugName;

    /** 药品品牌 */
    @Excel(name = "药品品牌")
    private String drugBrand;

    /** 药品大分类 */
    @Excel(name = "药品大分类")
    private String drugMainCategory;

    /** 药品小分类 */
    @Excel(name = "药品小分类")
    private String drugSubCategory;

    /** 规格 */
    @Excel(name = "规格")
    private String drugSpecification;

    /** 药品缩写 */
    @Excel(name = "药品缩写")
    private String drugAbbreviation;

    /** 药品编码 */
    @Excel(name = "药品编码")
    private String drugCode;

    /** 剂型 */
    @Excel(name = "剂型")
    private String dosageForm;

    /** 是否OTC */
    @Excel(name = "是否OTC")
    private Long isOtc;

    /** 是否医保药品 */
    @Excel(name = "是否医保药品")
    private Long isReimbursable;

    /** 库存上限 */
    @Excel(name = "库存上限")
    private Long stockLimit;

    public void setDrugId(Long drugId) 
    {
        this.drugId = drugId;
    }

    public Long getDrugId() 
    {
        return drugId;
    }
    public void setDrugName(String drugName) 
    {
        this.drugName = drugName;
    }

    public String getDrugName() 
    {
        return drugName;
    }
    public void setDrugBrand(String drugBrand) 
    {
        this.drugBrand = drugBrand;
    }

    public String getDrugBrand() 
    {
        return drugBrand;
    }
    public void setDrugMainCategory(String drugMainCategory) 
    {
        this.drugMainCategory = drugMainCategory;
    }

    public String getDrugMainCategory() 
    {
        return drugMainCategory;
    }
    public void setDrugSubCategory(String drugSubCategory) 
    {
        this.drugSubCategory = drugSubCategory;
    }

    public String getDrugSubCategory() 
    {
        return drugSubCategory;
    }
    public void setDrugSpecification(String drugSpecification) 
    {
        this.drugSpecification = drugSpecification;
    }

    public String getDrugSpecification() 
    {
        return drugSpecification;
    }
    public void setDrugAbbreviation(String drugAbbreviation) 
    {
        this.drugAbbreviation = drugAbbreviation;
    }

    public String getDrugAbbreviation() 
    {
        return drugAbbreviation;
    }
    public void setDrugCode(String drugCode) 
    {
        this.drugCode = drugCode;
    }

    public String getDrugCode() 
    {
        return drugCode;
    }
    public void setDosageForm(String dosageForm) 
    {
        this.dosageForm = dosageForm;
    }

    public String getDosageForm() 
    {
        return dosageForm;
    }
    public void setIsOtc(Long isOtc) 
    {
        this.isOtc = isOtc;
    }

    public Long getIsOtc() 
    {
        return isOtc;
    }
    public void setIsReimbursable(Long isReimbursable) 
    {
        this.isReimbursable = isReimbursable;
    }

    public Long getIsReimbursable() 
    {
        return isReimbursable;
    }
    public void setStockLimit(Long stockLimit) 
    {
        this.stockLimit = stockLimit;
    }

    public Long getStockLimit() 
    {
        return stockLimit;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("drugId", getDrugId())
            .append("drugName", getDrugName())
            .append("drugBrand", getDrugBrand())
            .append("drugMainCategory", getDrugMainCategory())
            .append("drugSubCategory", getDrugSubCategory())
            .append("drugSpecification", getDrugSpecification())
            .append("drugAbbreviation", getDrugAbbreviation())
            .append("drugCode", getDrugCode())
            .append("dosageForm", getDosageForm())
            .append("isOtc", getIsOtc())
            .append("isReimbursable", getIsReimbursable())
            .append("stockLimit", getStockLimit())
            .toString();
    }
}
