package com.tuoyu.project.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tuoyu.framework.aspectj.lang.annotation.Log;
import com.tuoyu.framework.aspectj.lang.enums.BusinessType;
import com.tuoyu.project.system.domain.PharmacyMedicine;
import com.tuoyu.project.system.service.IPharmacyMedicineService;
import com.tuoyu.framework.web.controller.BaseController;
import com.tuoyu.framework.web.domain.AjaxResult;
import com.tuoyu.common.utils.poi.ExcelUtil;
import com.tuoyu.framework.web.page.TableDataInfo;

/**
 * 药品信息Controller
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
@RestController
@RequestMapping("/system/medicine")
public class PharmacyMedicineController extends BaseController
{
    @Autowired
    private IPharmacyMedicineService pharmacyMedicineService;

    /**
     * 查询药品信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:medicine:list')")
    @GetMapping("/list")
    public TableDataInfo list(PharmacyMedicine pharmacyMedicine)
    {
        startPage();
        List<PharmacyMedicine> list = pharmacyMedicineService.selectPharmacyMedicineList(pharmacyMedicine);
        return getDataTable(list);
    }

    /**
     * 导出药品信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:medicine:export')")
    @Log(title = "药品信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PharmacyMedicine pharmacyMedicine)
    {
        List<PharmacyMedicine> list = pharmacyMedicineService.selectPharmacyMedicineList(pharmacyMedicine);
        ExcelUtil<PharmacyMedicine> util = new ExcelUtil<PharmacyMedicine>(PharmacyMedicine.class);
        util.exportExcel(response, list, "药品信息数据");
    }

    /**
     * 获取药品信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:medicine:query')")
    @GetMapping(value = "/{drugId}")
    public AjaxResult getInfo(@PathVariable("drugId") Long drugId)
    {
        return success(pharmacyMedicineService.selectPharmacyMedicineByDrugId(drugId));
    }

    /**
     * 新增药品信息
     */
    @PreAuthorize("@ss.hasPermi('system:medicine:add')")
    @Log(title = "药品信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PharmacyMedicine pharmacyMedicine)
    {
        return toAjax(pharmacyMedicineService.insertPharmacyMedicine(pharmacyMedicine));
    }

    /**
     * 修改药品信息
     */
    @PreAuthorize("@ss.hasPermi('system:medicine:edit')")
    @Log(title = "药品信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PharmacyMedicine pharmacyMedicine)
    {
        return toAjax(pharmacyMedicineService.updatePharmacyMedicine(pharmacyMedicine));
    }

    /**
     * 删除药品信息
     */
    @PreAuthorize("@ss.hasPermi('system:medicine:remove')")
    @Log(title = "药品信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{drugIds}")
    public AjaxResult remove(@PathVariable Long[] drugIds)
    {
        return toAjax(pharmacyMedicineService.deletePharmacyMedicineByDrugIds(drugIds));
    }
}
