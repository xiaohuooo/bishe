package com.tuoyu.project.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tuoyu.project.system.mapper.AncientPrescriptionHistoryMapper;
import com.tuoyu.project.system.domain.AncientPrescriptionHistory;
import com.tuoyu.project.system.service.IAncientPrescriptionHistoryService;

/**
 * 古方历史Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-24
 */
@Service
public class AncientPrescriptionHistoryServiceImpl implements IAncientPrescriptionHistoryService 
{
    @Autowired
    private AncientPrescriptionHistoryMapper ancientPrescriptionHistoryMapper;

    /**
     * 查询古方历史
     * 
     * @param hid 古方历史主键
     * @return 古方历史
     */
    @Override
    public AncientPrescriptionHistory selectAncientPrescriptionHistoryByHid(Long hid)
    {
        return ancientPrescriptionHistoryMapper.selectAncientPrescriptionHistoryByHid(hid);
    }

    /**
     * 查询古方历史列表
     * 
     * @param ancientPrescriptionHistory 古方历史
     * @return 古方历史
     */
    @Override
    public List<AncientPrescriptionHistory> selectAncientPrescriptionHistoryList(AncientPrescriptionHistory ancientPrescriptionHistory)
    {
        return ancientPrescriptionHistoryMapper.selectAncientPrescriptionHistoryList(ancientPrescriptionHistory);
    }

    /**
     * 新增古方历史
     * 
     * @param ancientPrescriptionHistory 古方历史
     * @return 结果
     */
    @Override
    public int insertAncientPrescriptionHistory(AncientPrescriptionHistory ancientPrescriptionHistory)
    {
        return ancientPrescriptionHistoryMapper.insertAncientPrescriptionHistory(ancientPrescriptionHistory);
    }

    /**
     * 修改古方历史
     * 
     * @param ancientPrescriptionHistory 古方历史
     * @return 结果
     */
    @Override
    public int updateAncientPrescriptionHistory(AncientPrescriptionHistory ancientPrescriptionHistory)
    {
        return ancientPrescriptionHistoryMapper.updateAncientPrescriptionHistory(ancientPrescriptionHistory);
    }

    /**
     * 批量删除古方历史
     * 
     * @param hids 需要删除的古方历史主键
     * @return 结果
     */
    @Override
    public int deleteAncientPrescriptionHistoryByHids(Long[] hids)
    {
        return ancientPrescriptionHistoryMapper.deleteAncientPrescriptionHistoryByHids(hids);
    }

    /**
     * 删除古方历史信息
     * 
     * @param hid 古方历史主键
     * @return 结果
     */
    @Override
    public int deleteAncientPrescriptionHistoryByHid(Long hid)
    {
        return ancientPrescriptionHistoryMapper.deleteAncientPrescriptionHistoryByHid(hid);
    }
}
