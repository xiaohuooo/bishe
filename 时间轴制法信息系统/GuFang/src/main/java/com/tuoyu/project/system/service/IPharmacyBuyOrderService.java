package com.tuoyu.project.system.service;

import java.util.List;
import com.tuoyu.project.system.domain.PharmacyBuyOrder;

/**
 * 药品采购Service接口
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
public interface IPharmacyBuyOrderService 
{
    /**
     * 查询药品采购
     * 
     * @param purchaseOrderId 药品采购主键
     * @return 药品采购
     */
    public PharmacyBuyOrder selectPharmacyBuyOrderByPurchaseOrderId(Long purchaseOrderId);

    /**
     * 查询药品采购列表
     * 
     * @param pharmacyBuyOrder 药品采购
     * @return 药品采购集合
     */
    public List<PharmacyBuyOrder> selectPharmacyBuyOrderList(PharmacyBuyOrder pharmacyBuyOrder);

    /**
     * 新增药品采购
     * 
     * @param pharmacyBuyOrder 药品采购
     * @return 结果
     */
    public int insertPharmacyBuyOrder(PharmacyBuyOrder pharmacyBuyOrder);

    /**
     * 修改药品采购
     * 
     * @param pharmacyBuyOrder 药品采购
     * @return 结果
     */
    public int updatePharmacyBuyOrder(PharmacyBuyOrder pharmacyBuyOrder);

    /**
     * 批量删除药品采购
     * 
     * @param purchaseOrderIds 需要删除的药品采购主键集合
     * @return 结果
     */
    public int deletePharmacyBuyOrderByPurchaseOrderIds(Long[] purchaseOrderIds);

    /**
     * 删除药品采购信息
     * 
     * @param purchaseOrderId 药品采购主键
     * @return 结果
     */
    public int deletePharmacyBuyOrderByPurchaseOrderId(Long purchaseOrderId);
}
