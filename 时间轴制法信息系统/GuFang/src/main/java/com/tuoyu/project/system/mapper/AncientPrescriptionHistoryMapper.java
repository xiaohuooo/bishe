package com.tuoyu.project.system.mapper;

import java.util.List;
import com.tuoyu.project.system.domain.AncientPrescriptionHistory;

/**
 * 古方历史Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-24
 */
public interface AncientPrescriptionHistoryMapper 
{
    /**
     * 查询古方历史
     * 
     * @param hid 古方历史主键
     * @return 古方历史
     */
    public AncientPrescriptionHistory selectAncientPrescriptionHistoryByHid(Long hid);

    /**
     * 查询古方历史列表
     * 
     * @param ancientPrescriptionHistory 古方历史
     * @return 古方历史集合
     */
    public List<AncientPrescriptionHistory> selectAncientPrescriptionHistoryList(AncientPrescriptionHistory ancientPrescriptionHistory);

    /**
     * 新增古方历史
     * 
     * @param ancientPrescriptionHistory 古方历史
     * @return 结果
     */
    public int insertAncientPrescriptionHistory(AncientPrescriptionHistory ancientPrescriptionHistory);

    /**
     * 修改古方历史
     * 
     * @param ancientPrescriptionHistory 古方历史
     * @return 结果
     */
    public int updateAncientPrescriptionHistory(AncientPrescriptionHistory ancientPrescriptionHistory);

    /**
     * 删除古方历史
     * 
     * @param hid 古方历史主键
     * @return 结果
     */
    public int deleteAncientPrescriptionHistoryByHid(Long hid);

    /**
     * 批量删除古方历史
     * 
     * @param hids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAncientPrescriptionHistoryByHids(Long[] hids);
}
