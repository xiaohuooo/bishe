package com.tuoyu.project.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tuoyu.framework.aspectj.lang.annotation.Log;
import com.tuoyu.framework.aspectj.lang.enums.BusinessType;
import com.tuoyu.project.system.domain.AncientPrescriptionHistory;
import com.tuoyu.project.system.service.IAncientPrescriptionHistoryService;
import com.tuoyu.framework.web.controller.BaseController;
import com.tuoyu.framework.web.domain.AjaxResult;
import com.tuoyu.common.utils.poi.ExcelUtil;
import com.tuoyu.framework.web.page.TableDataInfo;

/**
 * 古方历史Controller
 * 
 * @author ruoyi
 * @date 2024-04-24
 */
@RestController
@RequestMapping("/system/ancientPrescriptionHistory")
public class AncientPrescriptionHistoryController extends BaseController
{
    @Autowired
    private IAncientPrescriptionHistoryService ancientPrescriptionHistoryService;

    /**
     * 查询古方历史列表
     */
    @PreAuthorize("@ss.hasPermi('system:ancientPrescriptionHistory:list')")
    @GetMapping("/list")
    public TableDataInfo list(AncientPrescriptionHistory ancientPrescriptionHistory)
    {
        startPage();
        List<AncientPrescriptionHistory> list = ancientPrescriptionHistoryService.selectAncientPrescriptionHistoryList(ancientPrescriptionHistory);
        return getDataTable(list);
    }

    /**
     * 导出古方历史列表
     */
    @PreAuthorize("@ss.hasPermi('system:ancientPrescriptionHistory:export')")
    @Log(title = "古方历史", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AncientPrescriptionHistory ancientPrescriptionHistory)
    {
        List<AncientPrescriptionHistory> list = ancientPrescriptionHistoryService.selectAncientPrescriptionHistoryList(ancientPrescriptionHistory);
        ExcelUtil<AncientPrescriptionHistory> util = new ExcelUtil<AncientPrescriptionHistory>(AncientPrescriptionHistory.class);
        util.exportExcel(response, list, "古方历史数据");
    }

    /**
     * 获取古方历史详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:ancientPrescriptionHistory:query')")
    @GetMapping(value = "/{hid}")
    public AjaxResult getInfo(@PathVariable("hid") Long hid)
    {
        return success(ancientPrescriptionHistoryService.selectAncientPrescriptionHistoryByHid(hid));
    }

    /**
     * 新增古方历史
     */
    @PreAuthorize("@ss.hasPermi('system:ancientPrescriptionHistory:add')")
    @Log(title = "古方历史", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AncientPrescriptionHistory ancientPrescriptionHistory)
    {
        return toAjax(ancientPrescriptionHistoryService.insertAncientPrescriptionHistory(ancientPrescriptionHistory));
    }

    /**
     * 修改古方历史
     */
    @PreAuthorize("@ss.hasPermi('system:ancientPrescriptionHistory:edit')")
    @Log(title = "古方历史", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AncientPrescriptionHistory ancientPrescriptionHistory)
    {
        return toAjax(ancientPrescriptionHistoryService.updateAncientPrescriptionHistory(ancientPrescriptionHistory));
    }

    /**
     * 删除古方历史
     */
    @PreAuthorize("@ss.hasPermi('system:ancientPrescriptionHistory:remove')")
    @Log(title = "古方历史", businessType = BusinessType.DELETE)
	@DeleteMapping("/{hids}")
    public AjaxResult remove(@PathVariable Long[] hids)
    {
        return toAjax(ancientPrescriptionHistoryService.deleteAncientPrescriptionHistoryByHids(hids));
    }
}
