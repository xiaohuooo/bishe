package com.tuoyu.project.system.service;

import java.util.List;
import com.tuoyu.project.system.domain.PharmacySelesOrder;

/**
 * 药品销售Service接口
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
public interface IPharmacySelesOrderService 
{
    /**
     * 查询药品销售
     * 
     * @param salesOrderId 药品销售主键
     * @return 药品销售
     */
    public PharmacySelesOrder selectPharmacySelesOrderBySalesOrderId(Long salesOrderId);

    /**
     * 查询药品销售列表
     * 
     * @param pharmacySelesOrder 药品销售
     * @return 药品销售集合
     */
    public List<PharmacySelesOrder> selectPharmacySelesOrderList(PharmacySelesOrder pharmacySelesOrder);

    /**
     * 新增药品销售
     * 
     * @param pharmacySelesOrder 药品销售
     * @return 结果
     */
    public int insertPharmacySelesOrder(PharmacySelesOrder pharmacySelesOrder);

    /**
     * 修改药品销售
     * 
     * @param pharmacySelesOrder 药品销售
     * @return 结果
     */
    public int updatePharmacySelesOrder(PharmacySelesOrder pharmacySelesOrder);

    /**
     * 批量删除药品销售
     * 
     * @param salesOrderIds 需要删除的药品销售主键集合
     * @return 结果
     */
    public int deletePharmacySelesOrderBySalesOrderIds(Long[] salesOrderIds);

    /**
     * 删除药品销售信息
     * 
     * @param salesOrderId 药品销售主键
     * @return 结果
     */
    public int deletePharmacySelesOrderBySalesOrderId(Long salesOrderId);
}
