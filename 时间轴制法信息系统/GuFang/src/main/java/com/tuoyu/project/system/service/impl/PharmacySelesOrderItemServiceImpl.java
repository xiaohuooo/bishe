package com.tuoyu.project.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tuoyu.project.system.mapper.PharmacySelesOrderItemMapper;
import com.tuoyu.project.system.domain.PharmacySelesOrderItem;
import com.tuoyu.project.system.service.IPharmacySelesOrderItemService;

/**
 * 药品销售明细Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
@Service
public class PharmacySelesOrderItemServiceImpl implements IPharmacySelesOrderItemService 
{
    @Autowired
    private PharmacySelesOrderItemMapper pharmacySelesOrderItemMapper;

    /**
     * 查询药品销售明细
     * 
     * @param salesDetailId 药品销售明细主键
     * @return 药品销售明细
     */
    @Override
    public PharmacySelesOrderItem selectPharmacySelesOrderItemBySalesDetailId(Long salesDetailId)
    {
        return pharmacySelesOrderItemMapper.selectPharmacySelesOrderItemBySalesDetailId(salesDetailId);
    }

    /**
     * 查询药品销售明细列表
     * 
     * @param pharmacySelesOrderItem 药品销售明细
     * @return 药品销售明细
     */
    @Override
    public List<PharmacySelesOrderItem> selectPharmacySelesOrderItemList(PharmacySelesOrderItem pharmacySelesOrderItem)
    {
        return pharmacySelesOrderItemMapper.selectPharmacySelesOrderItemList(pharmacySelesOrderItem);
    }

    /**
     * 新增药品销售明细
     * 
     * @param pharmacySelesOrderItem 药品销售明细
     * @return 结果
     */
    @Override
    public int insertPharmacySelesOrderItem(PharmacySelesOrderItem pharmacySelesOrderItem)
    {
        return pharmacySelesOrderItemMapper.insertPharmacySelesOrderItem(pharmacySelesOrderItem);
    }

    /**
     * 修改药品销售明细
     * 
     * @param pharmacySelesOrderItem 药品销售明细
     * @return 结果
     */
    @Override
    public int updatePharmacySelesOrderItem(PharmacySelesOrderItem pharmacySelesOrderItem)
    {
        return pharmacySelesOrderItemMapper.updatePharmacySelesOrderItem(pharmacySelesOrderItem);
    }

    /**
     * 批量删除药品销售明细
     * 
     * @param salesDetailIds 需要删除的药品销售明细主键
     * @return 结果
     */
    @Override
    public int deletePharmacySelesOrderItemBySalesDetailIds(Long[] salesDetailIds)
    {
        return pharmacySelesOrderItemMapper.deletePharmacySelesOrderItemBySalesDetailIds(salesDetailIds);
    }

    /**
     * 删除药品销售明细信息
     * 
     * @param salesDetailId 药品销售明细主键
     * @return 结果
     */
    @Override
    public int deletePharmacySelesOrderItemBySalesDetailId(Long salesDetailId)
    {
        return pharmacySelesOrderItemMapper.deletePharmacySelesOrderItemBySalesDetailId(salesDetailId);
    }
}
