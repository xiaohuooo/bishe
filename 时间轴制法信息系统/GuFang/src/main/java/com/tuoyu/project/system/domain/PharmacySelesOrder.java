package com.tuoyu.project.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.tuoyu.framework.aspectj.lang.annotation.Excel;
import com.tuoyu.framework.web.domain.BaseEntity;

/**
 * 药品销售对象 pharmacy_seles_order
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
public class PharmacySelesOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 销售单ID */
    private Long salesOrderId;

    /** 发药日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发药日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dispensingDate;

    /** 发药时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发药时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dispensingTime;

    /** 药师用户ID */
    @Excel(name = "药师用户ID")
    private Long pharmacistUserId;

    /** 药师姓名 */
    @Excel(name = "药师姓名")
    private String pharmacistName;

    /** 问诊记录ID */
    @Excel(name = "问诊记录ID")
    private Long medicalRecordId;

    public void setSalesOrderId(Long salesOrderId) 
    {
        this.salesOrderId = salesOrderId;
    }

    public Long getSalesOrderId() 
    {
        return salesOrderId;
    }
    public void setDispensingDate(Date dispensingDate) 
    {
        this.dispensingDate = dispensingDate;
    }

    public Date getDispensingDate() 
    {
        return dispensingDate;
    }
    public void setDispensingTime(Date dispensingTime) 
    {
        this.dispensingTime = dispensingTime;
    }

    public Date getDispensingTime() 
    {
        return dispensingTime;
    }
    public void setPharmacistUserId(Long pharmacistUserId) 
    {
        this.pharmacistUserId = pharmacistUserId;
    }

    public Long getPharmacistUserId() 
    {
        return pharmacistUserId;
    }
    public void setPharmacistName(String pharmacistName) 
    {
        this.pharmacistName = pharmacistName;
    }

    public String getPharmacistName() 
    {
        return pharmacistName;
    }
    public void setMedicalRecordId(Long medicalRecordId) 
    {
        this.medicalRecordId = medicalRecordId;
    }

    public Long getMedicalRecordId() 
    {
        return medicalRecordId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("salesOrderId", getSalesOrderId())
            .append("dispensingDate", getDispensingDate())
            .append("dispensingTime", getDispensingTime())
            .append("pharmacistUserId", getPharmacistUserId())
            .append("pharmacistName", getPharmacistName())
            .append("medicalRecordId", getMedicalRecordId())
            .toString();
    }
}
