package com.tuoyu.project.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tuoyu.framework.aspectj.lang.annotation.Log;
import com.tuoyu.framework.aspectj.lang.enums.BusinessType;
import com.tuoyu.project.system.domain.PharmacySelesOrder;
import com.tuoyu.project.system.service.IPharmacySelesOrderService;
import com.tuoyu.framework.web.controller.BaseController;
import com.tuoyu.framework.web.domain.AjaxResult;
import com.tuoyu.common.utils.poi.ExcelUtil;
import com.tuoyu.framework.web.page.TableDataInfo;

/**
 * 药品销售Controller
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
@RestController
@RequestMapping("/system/salesOrder")
public class PharmacySelesOrderController extends BaseController
{
    @Autowired
    private IPharmacySelesOrderService pharmacySelesOrderService;

    /**
     * 查询药品销售列表
     */
    @PreAuthorize("@ss.hasPermi('system:salesOrder:list')")
    @GetMapping("/list")
    public TableDataInfo list(PharmacySelesOrder pharmacySelesOrder)
    {
        startPage();
        List<PharmacySelesOrder> list = pharmacySelesOrderService.selectPharmacySelesOrderList(pharmacySelesOrder);
        return getDataTable(list);
    }

    /**
     * 导出药品销售列表
     */
    @PreAuthorize("@ss.hasPermi('system:salesOrder:export')")
    @Log(title = "药品销售", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PharmacySelesOrder pharmacySelesOrder)
    {
        List<PharmacySelesOrder> list = pharmacySelesOrderService.selectPharmacySelesOrderList(pharmacySelesOrder);
        ExcelUtil<PharmacySelesOrder> util = new ExcelUtil<PharmacySelesOrder>(PharmacySelesOrder.class);
        util.exportExcel(response, list, "药品销售数据");
    }

    /**
     * 获取药品销售详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:salesOrder:query')")
    @GetMapping(value = "/{salesOrderId}")
    public AjaxResult getInfo(@PathVariable("salesOrderId") Long salesOrderId)
    {
        return success(pharmacySelesOrderService.selectPharmacySelesOrderBySalesOrderId(salesOrderId));
    }

    /**
     * 新增药品销售
     */
    @PreAuthorize("@ss.hasPermi('system:salesOrder:add')")
    @Log(title = "药品销售", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PharmacySelesOrder pharmacySelesOrder)
    {
        return toAjax(pharmacySelesOrderService.insertPharmacySelesOrder(pharmacySelesOrder));
    }

    /**
     * 修改药品销售
     */
    @PreAuthorize("@ss.hasPermi('system:salesOrder:edit')")
    @Log(title = "药品销售", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PharmacySelesOrder pharmacySelesOrder)
    {
        return toAjax(pharmacySelesOrderService.updatePharmacySelesOrder(pharmacySelesOrder));
    }

    /**
     * 删除药品销售
     */
    @PreAuthorize("@ss.hasPermi('system:salesOrder:remove')")
    @Log(title = "药品销售", businessType = BusinessType.DELETE)
	@DeleteMapping("/{salesOrderIds}")
    public AjaxResult remove(@PathVariable Long[] salesOrderIds)
    {
        return toAjax(pharmacySelesOrderService.deletePharmacySelesOrderBySalesOrderIds(salesOrderIds));
    }
}
