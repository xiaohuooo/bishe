package com.tuoyu.project.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tuoyu.project.system.mapper.PharmacyBuyOrderItemMapper;
import com.tuoyu.project.system.domain.PharmacyBuyOrderItem;
import com.tuoyu.project.system.service.IPharmacyBuyOrderItemService;

/**
 * 药品采购明细Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
@Service
public class PharmacyBuyOrderItemServiceImpl implements IPharmacyBuyOrderItemService 
{
    @Autowired
    private PharmacyBuyOrderItemMapper pharmacyBuyOrderItemMapper;

    /**
     * 查询药品采购明细
     * 
     * @param purchaseDetailId 药品采购明细主键
     * @return 药品采购明细
     */
    @Override
    public PharmacyBuyOrderItem selectPharmacyBuyOrderItemByPurchaseDetailId(Long purchaseDetailId)
    {
        return pharmacyBuyOrderItemMapper.selectPharmacyBuyOrderItemByPurchaseDetailId(purchaseDetailId);
    }

    /**
     * 查询药品采购明细列表
     * 
     * @param pharmacyBuyOrderItem 药品采购明细
     * @return 药品采购明细
     */
    @Override
    public List<PharmacyBuyOrderItem> selectPharmacyBuyOrderItemList(PharmacyBuyOrderItem pharmacyBuyOrderItem)
    {
        return pharmacyBuyOrderItemMapper.selectPharmacyBuyOrderItemList(pharmacyBuyOrderItem);
    }

    /**
     * 新增药品采购明细
     * 
     * @param pharmacyBuyOrderItem 药品采购明细
     * @return 结果
     */
    @Override
    public int insertPharmacyBuyOrderItem(PharmacyBuyOrderItem pharmacyBuyOrderItem)
    {
        return pharmacyBuyOrderItemMapper.insertPharmacyBuyOrderItem(pharmacyBuyOrderItem);
    }

    /**
     * 修改药品采购明细
     * 
     * @param pharmacyBuyOrderItem 药品采购明细
     * @return 结果
     */
    @Override
    public int updatePharmacyBuyOrderItem(PharmacyBuyOrderItem pharmacyBuyOrderItem)
    {
        return pharmacyBuyOrderItemMapper.updatePharmacyBuyOrderItem(pharmacyBuyOrderItem);
    }

    /**
     * 批量删除药品采购明细
     * 
     * @param purchaseDetailIds 需要删除的药品采购明细主键
     * @return 结果
     */
    @Override
    public int deletePharmacyBuyOrderItemByPurchaseDetailIds(Long[] purchaseDetailIds)
    {
        return pharmacyBuyOrderItemMapper.deletePharmacyBuyOrderItemByPurchaseDetailIds(purchaseDetailIds);
    }

    /**
     * 删除药品采购明细信息
     * 
     * @param purchaseDetailId 药品采购明细主键
     * @return 结果
     */
    @Override
    public int deletePharmacyBuyOrderItemByPurchaseDetailId(Long purchaseDetailId)
    {
        return pharmacyBuyOrderItemMapper.deletePharmacyBuyOrderItemByPurchaseDetailId(purchaseDetailId);
    }
}
