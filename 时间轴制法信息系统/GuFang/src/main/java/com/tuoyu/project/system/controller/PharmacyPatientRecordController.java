package com.tuoyu.project.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tuoyu.framework.aspectj.lang.annotation.Log;
import com.tuoyu.framework.aspectj.lang.enums.BusinessType;
import com.tuoyu.project.system.domain.PharmacyPatientRecord;
import com.tuoyu.project.system.service.IPharmacyPatientRecordService;
import com.tuoyu.framework.web.controller.BaseController;
import com.tuoyu.framework.web.domain.AjaxResult;
import com.tuoyu.common.utils.poi.ExcelUtil;
import com.tuoyu.framework.web.page.TableDataInfo;

/**
 * 患者就诊记Controller
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
@RestController
@RequestMapping("/system/record")
public class PharmacyPatientRecordController extends BaseController
{
    @Autowired
    private IPharmacyPatientRecordService pharmacyPatientRecordService;

    /**
     * 查询患者就诊记列表
     */
    @PreAuthorize("@ss.hasPermi('system:record:list')")
    @GetMapping("/list")
    public TableDataInfo list(PharmacyPatientRecord pharmacyPatientRecord)
    {
        startPage();
        List<PharmacyPatientRecord> list = pharmacyPatientRecordService.selectPharmacyPatientRecordList(pharmacyPatientRecord);
        return getDataTable(list);
    }

    /**
     * 导出患者就诊记列表
     */
    @PreAuthorize("@ss.hasPermi('system:record:export')")
    @Log(title = "患者就诊记", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PharmacyPatientRecord pharmacyPatientRecord)
    {
        List<PharmacyPatientRecord> list = pharmacyPatientRecordService.selectPharmacyPatientRecordList(pharmacyPatientRecord);
        ExcelUtil<PharmacyPatientRecord> util = new ExcelUtil<PharmacyPatientRecord>(PharmacyPatientRecord.class);
        util.exportExcel(response, list, "患者就诊记数据");
    }

    /**
     * 获取患者就诊记详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:record:query')")
    @GetMapping(value = "/{consultationRecordId}")
    public AjaxResult getInfo(@PathVariable("consultationRecordId") Long consultationRecordId)
    {
        return success(pharmacyPatientRecordService.selectPharmacyPatientRecordByConsultationRecordId(consultationRecordId));
    }

    /**
     * 新增患者就诊记
     */
    @PreAuthorize("@ss.hasPermi('system:record:add')")
    @Log(title = "患者就诊记", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PharmacyPatientRecord pharmacyPatientRecord)
    {
        return toAjax(pharmacyPatientRecordService.insertPharmacyPatientRecord(pharmacyPatientRecord));
    }

    /**
     * 修改患者就诊记
     */
    @PreAuthorize("@ss.hasPermi('system:record:edit')")
    @Log(title = "患者就诊记", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PharmacyPatientRecord pharmacyPatientRecord)
    {
        return toAjax(pharmacyPatientRecordService.updatePharmacyPatientRecord(pharmacyPatientRecord));
    }

    /**
     * 删除患者就诊记
     */
    @PreAuthorize("@ss.hasPermi('system:record:remove')")
    @Log(title = "患者就诊记", businessType = BusinessType.DELETE)
	@DeleteMapping("/{consultationRecordIds}")
    public AjaxResult remove(@PathVariable Long[] consultationRecordIds)
    {
        return toAjax(pharmacyPatientRecordService.deletePharmacyPatientRecordByConsultationRecordIds(consultationRecordIds));
    }
}
