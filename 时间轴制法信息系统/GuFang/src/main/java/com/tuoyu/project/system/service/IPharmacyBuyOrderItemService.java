package com.tuoyu.project.system.service;

import java.util.List;
import com.tuoyu.project.system.domain.PharmacyBuyOrderItem;

/**
 * 药品采购明细Service接口
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
public interface IPharmacyBuyOrderItemService 
{
    /**
     * 查询药品采购明细
     * 
     * @param purchaseDetailId 药品采购明细主键
     * @return 药品采购明细
     */
    public PharmacyBuyOrderItem selectPharmacyBuyOrderItemByPurchaseDetailId(Long purchaseDetailId);

    /**
     * 查询药品采购明细列表
     * 
     * @param pharmacyBuyOrderItem 药品采购明细
     * @return 药品采购明细集合
     */
    public List<PharmacyBuyOrderItem> selectPharmacyBuyOrderItemList(PharmacyBuyOrderItem pharmacyBuyOrderItem);

    /**
     * 新增药品采购明细
     * 
     * @param pharmacyBuyOrderItem 药品采购明细
     * @return 结果
     */
    public int insertPharmacyBuyOrderItem(PharmacyBuyOrderItem pharmacyBuyOrderItem);

    /**
     * 修改药品采购明细
     * 
     * @param pharmacyBuyOrderItem 药品采购明细
     * @return 结果
     */
    public int updatePharmacyBuyOrderItem(PharmacyBuyOrderItem pharmacyBuyOrderItem);

    /**
     * 批量删除药品采购明细
     * 
     * @param purchaseDetailIds 需要删除的药品采购明细主键集合
     * @return 结果
     */
    public int deletePharmacyBuyOrderItemByPurchaseDetailIds(Long[] purchaseDetailIds);

    /**
     * 删除药品采购明细信息
     * 
     * @param purchaseDetailId 药品采购明细主键
     * @return 结果
     */
    public int deletePharmacyBuyOrderItemByPurchaseDetailId(Long purchaseDetailId);
}
