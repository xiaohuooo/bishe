package com.tuoyu.project.system.service;

import java.util.List;
import com.tuoyu.project.system.domain.AncientPrescription;

/**
 * 古方Service接口
 * 
 * @author ruoyi
 * @date 2024-04-24
 */
public interface IAncientPrescriptionService 
{
    /**
     * 查询古方
     * 
     * @param id 古方主键
     * @return 古方
     */
    public AncientPrescription selectAncientPrescriptionById(Long id);

    /**
     * 查询古方列表
     * 
     * @param ancientPrescription 古方
     * @return 古方集合
     */
    public List<AncientPrescription> selectAncientPrescriptionList(AncientPrescription ancientPrescription);

    /**
     * 新增古方
     * 
     * @param ancientPrescription 古方
     * @return 结果
     */
    public int insertAncientPrescription(AncientPrescription ancientPrescription);

    /**
     * 修改古方
     * 
     * @param ancientPrescription 古方
     * @return 结果
     */
    public int updateAncientPrescription(AncientPrescription ancientPrescription);

    /**
     * 批量删除古方
     * 
     * @param ids 需要删除的古方主键集合
     * @return 结果
     */
    public int deleteAncientPrescriptionByIds(Long[] ids);

    /**
     * 删除古方信息
     * 
     * @param id 古方主键
     * @return 结果
     */
    public int deleteAncientPrescriptionById(Long id);
}
