package com.tuoyu.project.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tuoyu.framework.aspectj.lang.annotation.Log;
import com.tuoyu.framework.aspectj.lang.enums.BusinessType;
import com.tuoyu.project.system.domain.PharmacyBuyOrder;
import com.tuoyu.project.system.service.IPharmacyBuyOrderService;
import com.tuoyu.framework.web.controller.BaseController;
import com.tuoyu.framework.web.domain.AjaxResult;
import com.tuoyu.common.utils.poi.ExcelUtil;
import com.tuoyu.framework.web.page.TableDataInfo;

/**
 * 药品采购Controller
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
@RestController
@RequestMapping("/system/order")
public class PharmacyBuyOrderController extends BaseController
{
    @Autowired
    private IPharmacyBuyOrderService pharmacyBuyOrderService;

    /**
     * 查询药品采购列表
     */
    @PreAuthorize("@ss.hasPermi('system:order:list')")
    @GetMapping("/list")
    public TableDataInfo list(PharmacyBuyOrder pharmacyBuyOrder)
    {
        startPage();
        List<PharmacyBuyOrder> list = pharmacyBuyOrderService.selectPharmacyBuyOrderList(pharmacyBuyOrder);
        return getDataTable(list);
    }

    /**
     * 导出药品采购列表
     */
    @PreAuthorize("@ss.hasPermi('system:order:export')")
    @Log(title = "药品采购", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PharmacyBuyOrder pharmacyBuyOrder)
    {
        List<PharmacyBuyOrder> list = pharmacyBuyOrderService.selectPharmacyBuyOrderList(pharmacyBuyOrder);
        ExcelUtil<PharmacyBuyOrder> util = new ExcelUtil<PharmacyBuyOrder>(PharmacyBuyOrder.class);
        util.exportExcel(response, list, "药品采购数据");
    }

    /**
     * 获取药品采购详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:order:query')")
    @GetMapping(value = "/{purchaseOrderId}")
    public AjaxResult getInfo(@PathVariable("purchaseOrderId") Long purchaseOrderId)
    {
        return success(pharmacyBuyOrderService.selectPharmacyBuyOrderByPurchaseOrderId(purchaseOrderId));
    }

    /**
     * 新增药品采购
     */
    @PreAuthorize("@ss.hasPermi('system:order:add')")
    @Log(title = "药品采购", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PharmacyBuyOrder pharmacyBuyOrder)
    {
        return toAjax(pharmacyBuyOrderService.insertPharmacyBuyOrder(pharmacyBuyOrder));
    }

    /**
     * 修改药品采购
     */
    @PreAuthorize("@ss.hasPermi('system:order:edit')")
    @Log(title = "药品采购", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PharmacyBuyOrder pharmacyBuyOrder)
    {
        return toAjax(pharmacyBuyOrderService.updatePharmacyBuyOrder(pharmacyBuyOrder));
    }

    /**
     * 删除药品采购
     */
    @PreAuthorize("@ss.hasPermi('system:order:remove')")
    @Log(title = "药品采购", businessType = BusinessType.DELETE)
	@DeleteMapping("/{purchaseOrderIds}")
    public AjaxResult remove(@PathVariable Long[] purchaseOrderIds)
    {
        return toAjax(pharmacyBuyOrderService.deletePharmacyBuyOrderByPurchaseOrderIds(purchaseOrderIds));
    }
}
