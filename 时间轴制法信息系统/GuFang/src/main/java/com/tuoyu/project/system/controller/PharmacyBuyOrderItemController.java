package com.tuoyu.project.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tuoyu.framework.aspectj.lang.annotation.Log;
import com.tuoyu.framework.aspectj.lang.enums.BusinessType;
import com.tuoyu.project.system.domain.PharmacyBuyOrderItem;
import com.tuoyu.project.system.service.IPharmacyBuyOrderItemService;
import com.tuoyu.framework.web.controller.BaseController;
import com.tuoyu.framework.web.domain.AjaxResult;
import com.tuoyu.common.utils.poi.ExcelUtil;
import com.tuoyu.framework.web.page.TableDataInfo;

/**
 * 药品采购明细Controller
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
@RestController
@RequestMapping("/system/item")
public class PharmacyBuyOrderItemController extends BaseController
{
    @Autowired
    private IPharmacyBuyOrderItemService pharmacyBuyOrderItemService;

    /**
     * 查询药品采购明细列表
     */
    @PreAuthorize("@ss.hasPermi('system:item:list')")
    @GetMapping("/list")
    public TableDataInfo list(PharmacyBuyOrderItem pharmacyBuyOrderItem)
    {
        startPage();
        List<PharmacyBuyOrderItem> list = pharmacyBuyOrderItemService.selectPharmacyBuyOrderItemList(pharmacyBuyOrderItem);
        return getDataTable(list);
    }

    /**
     * 导出药品采购明细列表
     */
    @PreAuthorize("@ss.hasPermi('system:item:export')")
    @Log(title = "药品采购明细", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PharmacyBuyOrderItem pharmacyBuyOrderItem)
    {
        List<PharmacyBuyOrderItem> list = pharmacyBuyOrderItemService.selectPharmacyBuyOrderItemList(pharmacyBuyOrderItem);
        ExcelUtil<PharmacyBuyOrderItem> util = new ExcelUtil<PharmacyBuyOrderItem>(PharmacyBuyOrderItem.class);
        util.exportExcel(response, list, "药品采购明细数据");
    }

    /**
     * 获取药品采购明细详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:item:query')")
    @GetMapping(value = "/{purchaseDetailId}")
    public AjaxResult getInfo(@PathVariable("purchaseDetailId") Long purchaseDetailId)
    {
        return success(pharmacyBuyOrderItemService.selectPharmacyBuyOrderItemByPurchaseDetailId(purchaseDetailId));
    }

    /**
     * 新增药品采购明细
     */
    @PreAuthorize("@ss.hasPermi('system:item:add')")
    @Log(title = "药品采购明细", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PharmacyBuyOrderItem pharmacyBuyOrderItem)
    {
        return toAjax(pharmacyBuyOrderItemService.insertPharmacyBuyOrderItem(pharmacyBuyOrderItem));
    }

    /**
     * 修改药品采购明细
     */
    @PreAuthorize("@ss.hasPermi('system:item:edit')")
    @Log(title = "药品采购明细", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PharmacyBuyOrderItem pharmacyBuyOrderItem)
    {
        return toAjax(pharmacyBuyOrderItemService.updatePharmacyBuyOrderItem(pharmacyBuyOrderItem));
    }

    /**
     * 删除药品采购明细
     */
    @PreAuthorize("@ss.hasPermi('system:item:remove')")
    @Log(title = "药品采购明细", businessType = BusinessType.DELETE)
	@DeleteMapping("/{purchaseDetailIds}")
    public AjaxResult remove(@PathVariable Long[] purchaseDetailIds)
    {
        return toAjax(pharmacyBuyOrderItemService.deletePharmacyBuyOrderItemByPurchaseDetailIds(purchaseDetailIds));
    }
}
