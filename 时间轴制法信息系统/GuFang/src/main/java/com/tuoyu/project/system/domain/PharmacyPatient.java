package com.tuoyu.project.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.tuoyu.framework.aspectj.lang.annotation.Excel;
import com.tuoyu.framework.web.domain.BaseEntity;

/**
 * 患者档案对象 pharmacy_patient
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
public class PharmacyPatient extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 档案ID */
    private Long recordId;

    /** 患者姓名 */
    @Excel(name = "患者姓名")
    private String patientName;

    /** 患者联系电话 */
    @Excel(name = "患者联系电话")
    private String patientContact;

    /** 病人年龄 */
    @Excel(name = "病人年龄")
    private Long patientAge;

    /** 患者性别 */
    @Excel(name = "患者性别")
    private Long patientGender;

    /** 病史 */
    @Excel(name = "病史")
    private String medicalHistory;

    /** 医师ID */
    @Excel(name = "医师ID")
    private Long physicianId;

    /** 医师姓名 */
    @Excel(name = "医师姓名")
    private String physicianName;

    public void setRecordId(Long recordId) 
    {
        this.recordId = recordId;
    }

    public Long getRecordId() 
    {
        return recordId;
    }
    public void setPatientName(String patientName) 
    {
        this.patientName = patientName;
    }

    public String getPatientName() 
    {
        return patientName;
    }
    public void setPatientContact(String patientContact) 
    {
        this.patientContact = patientContact;
    }

    public String getPatientContact() 
    {
        return patientContact;
    }
    public void setPatientAge(Long patientAge) 
    {
        this.patientAge = patientAge;
    }

    public Long getPatientAge() 
    {
        return patientAge;
    }
    public void setPatientGender(Long patientGender) 
    {
        this.patientGender = patientGender;
    }

    public Long getPatientGender() 
    {
        return patientGender;
    }
    public void setMedicalHistory(String medicalHistory) 
    {
        this.medicalHistory = medicalHistory;
    }

    public String getMedicalHistory() 
    {
        return medicalHistory;
    }
    public void setPhysicianId(Long physicianId) 
    {
        this.physicianId = physicianId;
    }

    public Long getPhysicianId() 
    {
        return physicianId;
    }
    public void setPhysicianName(String physicianName) 
    {
        this.physicianName = physicianName;
    }

    public String getPhysicianName() 
    {
        return physicianName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("recordId", getRecordId())
            .append("patientName", getPatientName())
            .append("patientContact", getPatientContact())
            .append("patientAge", getPatientAge())
            .append("patientGender", getPatientGender())
            .append("medicalHistory", getMedicalHistory())
            .append("physicianId", getPhysicianId())
            .append("physicianName", getPhysicianName())
            .toString();
    }
}
