package com.tuoyu.project.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.tuoyu.framework.aspectj.lang.annotation.Anonymous;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tuoyu.framework.aspectj.lang.annotation.Log;
import com.tuoyu.framework.aspectj.lang.enums.BusinessType;
import com.tuoyu.project.system.domain.AncientPrescription;
import com.tuoyu.project.system.service.IAncientPrescriptionService;
import com.tuoyu.framework.web.controller.BaseController;
import com.tuoyu.framework.web.domain.AjaxResult;
import com.tuoyu.common.utils.poi.ExcelUtil;
import com.tuoyu.framework.web.page.TableDataInfo;

/**
 * 古方Controller
 * 
 * @author ruoyi
 * @date 2024-04-24
 */
@RestController
@RequestMapping("/system/ancientPrescriptionManage")
public class AncientPrescriptionController extends BaseController
{
    @Autowired
    private IAncientPrescriptionService ancientPrescriptionService;

    /**
     * 查询古方列表
     */
    @Anonymous
    @GetMapping("/list")
    public TableDataInfo list(AncientPrescription ancientPrescription)
    {
        startPage();
        List<AncientPrescription> list = ancientPrescriptionService.selectAncientPrescriptionList(ancientPrescription);
        return getDataTable(list);
    }

    /**
     * 导出古方列表
     */
    @PreAuthorize("@ss.hasPermi('system:ancientPrescriptionManage:export')")
    @Log(title = "古方", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AncientPrescription ancientPrescription)
    {
        List<AncientPrescription> list = ancientPrescriptionService.selectAncientPrescriptionList(ancientPrescription);
        ExcelUtil<AncientPrescription> util = new ExcelUtil<AncientPrescription>(AncientPrescription.class);
        util.exportExcel(response, list, "古方数据");
    }

    /**
     * 获取古方详细信息
     */
    @Anonymous
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(ancientPrescriptionService.selectAncientPrescriptionById(id));
    }

    /**
     * 新增古方
     */
    @PreAuthorize("@ss.hasPermi('system:ancientPrescriptionManage:add')")
    @Log(title = "古方", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AncientPrescription ancientPrescription)
    {
        return toAjax(ancientPrescriptionService.insertAncientPrescription(ancientPrescription));
    }

    /**
     * 修改古方
     */
    @PreAuthorize("@ss.hasPermi('system:ancientPrescriptionManage:edit')")
    @Log(title = "古方", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AncientPrescription ancientPrescription)
    {
        return toAjax(ancientPrescriptionService.updateAncientPrescription(ancientPrescription));
    }

    /**
     * 删除古方
     */
    @PreAuthorize("@ss.hasPermi('system:ancientPrescriptionManage:remove')")
    @Log(title = "古方", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(ancientPrescriptionService.deleteAncientPrescriptionByIds(ids));
    }
}
