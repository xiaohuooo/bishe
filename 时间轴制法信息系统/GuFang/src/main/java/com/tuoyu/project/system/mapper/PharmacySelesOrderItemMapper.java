package com.tuoyu.project.system.mapper;

import java.util.List;
import com.tuoyu.project.system.domain.PharmacySelesOrderItem;

/**
 * 药品销售明细Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
public interface PharmacySelesOrderItemMapper 
{
    /**
     * 查询药品销售明细
     * 
     * @param salesDetailId 药品销售明细主键
     * @return 药品销售明细
     */
    public PharmacySelesOrderItem selectPharmacySelesOrderItemBySalesDetailId(Long salesDetailId);

    /**
     * 查询药品销售明细列表
     * 
     * @param pharmacySelesOrderItem 药品销售明细
     * @return 药品销售明细集合
     */
    public List<PharmacySelesOrderItem> selectPharmacySelesOrderItemList(PharmacySelesOrderItem pharmacySelesOrderItem);

    /**
     * 新增药品销售明细
     * 
     * @param pharmacySelesOrderItem 药品销售明细
     * @return 结果
     */
    public int insertPharmacySelesOrderItem(PharmacySelesOrderItem pharmacySelesOrderItem);

    /**
     * 修改药品销售明细
     * 
     * @param pharmacySelesOrderItem 药品销售明细
     * @return 结果
     */
    public int updatePharmacySelesOrderItem(PharmacySelesOrderItem pharmacySelesOrderItem);

    /**
     * 删除药品销售明细
     * 
     * @param salesDetailId 药品销售明细主键
     * @return 结果
     */
    public int deletePharmacySelesOrderItemBySalesDetailId(Long salesDetailId);

    /**
     * 批量删除药品销售明细
     * 
     * @param salesDetailIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePharmacySelesOrderItemBySalesDetailIds(Long[] salesDetailIds);
}
