package com.tuoyu.project.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tuoyu.project.system.mapper.PharmacyMedicineMapper;
import com.tuoyu.project.system.domain.PharmacyMedicine;
import com.tuoyu.project.system.service.IPharmacyMedicineService;

/**
 * 药品信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
@Service
public class PharmacyMedicineServiceImpl implements IPharmacyMedicineService 
{
    @Autowired
    private PharmacyMedicineMapper pharmacyMedicineMapper;

    /**
     * 查询药品信息
     * 
     * @param drugId 药品信息主键
     * @return 药品信息
     */
    @Override
    public PharmacyMedicine selectPharmacyMedicineByDrugId(Long drugId)
    {
        return pharmacyMedicineMapper.selectPharmacyMedicineByDrugId(drugId);
    }

    /**
     * 查询药品信息列表
     * 
     * @param pharmacyMedicine 药品信息
     * @return 药品信息
     */
    @Override
    public List<PharmacyMedicine> selectPharmacyMedicineList(PharmacyMedicine pharmacyMedicine)
    {
        return pharmacyMedicineMapper.selectPharmacyMedicineList(pharmacyMedicine);
    }

    /**
     * 新增药品信息
     * 
     * @param pharmacyMedicine 药品信息
     * @return 结果
     */
    @Override
    public int insertPharmacyMedicine(PharmacyMedicine pharmacyMedicine)
    {
        return pharmacyMedicineMapper.insertPharmacyMedicine(pharmacyMedicine);
    }

    /**
     * 修改药品信息
     * 
     * @param pharmacyMedicine 药品信息
     * @return 结果
     */
    @Override
    public int updatePharmacyMedicine(PharmacyMedicine pharmacyMedicine)
    {
        return pharmacyMedicineMapper.updatePharmacyMedicine(pharmacyMedicine);
    }

    /**
     * 批量删除药品信息
     * 
     * @param drugIds 需要删除的药品信息主键
     * @return 结果
     */
    @Override
    public int deletePharmacyMedicineByDrugIds(Long[] drugIds)
    {
        return pharmacyMedicineMapper.deletePharmacyMedicineByDrugIds(drugIds);
    }

    /**
     * 删除药品信息信息
     * 
     * @param drugId 药品信息主键
     * @return 结果
     */
    @Override
    public int deletePharmacyMedicineByDrugId(Long drugId)
    {
        return pharmacyMedicineMapper.deletePharmacyMedicineByDrugId(drugId);
    }
}
