package com.tuoyu.project.system.mapper;

import java.util.List;
import com.tuoyu.project.system.domain.PharmacyMedicine;

/**
 * 药品信息Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
public interface PharmacyMedicineMapper 
{
    /**
     * 查询药品信息
     * 
     * @param drugId 药品信息主键
     * @return 药品信息
     */
    public PharmacyMedicine selectPharmacyMedicineByDrugId(Long drugId);

    /**
     * 查询药品信息列表
     * 
     * @param pharmacyMedicine 药品信息
     * @return 药品信息集合
     */
    public List<PharmacyMedicine> selectPharmacyMedicineList(PharmacyMedicine pharmacyMedicine);

    /**
     * 新增药品信息
     * 
     * @param pharmacyMedicine 药品信息
     * @return 结果
     */
    public int insertPharmacyMedicine(PharmacyMedicine pharmacyMedicine);

    /**
     * 修改药品信息
     * 
     * @param pharmacyMedicine 药品信息
     * @return 结果
     */
    public int updatePharmacyMedicine(PharmacyMedicine pharmacyMedicine);

    /**
     * 删除药品信息
     * 
     * @param drugId 药品信息主键
     * @return 结果
     */
    public int deletePharmacyMedicineByDrugId(Long drugId);

    /**
     * 批量删除药品信息
     * 
     * @param drugIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePharmacyMedicineByDrugIds(Long[] drugIds);
}
