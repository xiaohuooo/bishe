import request from '@/utils/request'

// 查询药品采购明细列表
export function listItem(query) {
  return request({
    url: '/system/item/list',
    method: 'get',
    params: query
  })
}

// 查询药品采购明细详细
export function getItem(purchaseDetailId) {
  return request({
    url: '/system/item/' + purchaseDetailId,
    method: 'get'
  })
}

// 新增药品采购明细
export function addItem(data) {
  return request({
    url: '/system/item',
    method: 'post',
    data: data
  })
}

// 修改药品采购明细
export function updateItem(data) {
  return request({
    url: '/system/item',
    method: 'put',
    data: data
  })
}

// 删除药品采购明细
export function delItem(purchaseDetailId) {
  return request({
    url: '/system/item/' + purchaseDetailId,
    method: 'delete'
  })
}
