import request from '@/utils/request'

// 查询药品销售明细列表
export function listSalesItem(query) {
  return request({
    url: '/system/salesItem/list',
    method: 'get',
    params: query
  })
}

// 查询药品销售明细详细
export function getSalesItem(salesDetailId) {
  return request({
    url: '/system/salesItem/' + salesDetailId,
    method: 'get'
  })
}

// 新增药品销售明细
export function addSalesItem(data) {
  return request({
    url: '/system/salesItem',
    method: 'post',
    data: data
  })
}

// 修改药品销售明细
export function updateSalesItem(data) {
  return request({
    url: '/system/salesItem',
    method: 'put',
    data: data
  })
}

// 删除药品销售明细
export function delSalesItem(salesDetailId) {
  return request({
    url: '/system/salesItem/' + salesDetailId,
    method: 'delete'
  })
}
