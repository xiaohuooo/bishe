import request from '@/utils/request'

// 查询药品采购列表
export function listOrder(query) {
  return request({
    url: '/system/order/list',
    method: 'get',
    params: query
  })
}

// 查询药品采购详细
export function getOrder(purchaseOrderId) {
  return request({
    url: '/system/order/' + purchaseOrderId,
    method: 'get'
  })
}

// 新增药品采购
export function addOrder(data) {
  return request({
    url: '/system/order',
    method: 'post',
    data: data
  })
}

// 修改药品采购
export function updateOrder(data) {
  return request({
    url: '/system/order',
    method: 'put',
    data: data
  })
}

// 删除药品采购
export function delOrder(purchaseOrderId) {
  return request({
    url: '/system/order/' + purchaseOrderId,
    method: 'delete'
  })
}
