import request from '@/utils/request'

// 查询古方历史列表
export function listAncientPrescriptionHistory(query) {
  return request({
    url: '/system/ancientPrescriptionHistory/list',
    method: 'get',
    params: query
  })
}

// 查询古方历史详细
export function getAncientPrescriptionHistory(hid) {
  return request({
    url: '/system/ancientPrescriptionHistory/' + hid,
    method: 'get'
  })
}

// 新增古方历史
export function addAncientPrescriptionHistory(data) {
  return request({
    url: '/system/ancientPrescriptionHistory',
    method: 'post',
    data: data
  })
}

// 修改古方历史
export function updateAncientPrescriptionHistory(data) {
  return request({
    url: '/system/ancientPrescriptionHistory',
    method: 'put',
    data: data
  })
}

// 删除古方历史
export function delAncientPrescriptionHistory(hid) {
  return request({
    url: '/system/ancientPrescriptionHistory/' + hid,
    method: 'delete'
  })
}
