import request from '@/utils/request'

// 查询患者档案列表
export function listPatient(query) {
  return request({
    url: '/system/patient/list',
    method: 'get',
    params: query
  })
}

// 查询患者档案详细
export function getPatient(recordId) {
  return request({
    url: '/system/patient/' + recordId,
    method: 'get'
  })
}

// 新增患者档案
export function addPatient(data) {
  return request({
    url: '/system/patient',
    method: 'post',
    data: data
  })
}

// 修改患者档案
export function updatePatient(data) {
  return request({
    url: '/system/patient',
    method: 'put',
    data: data
  })
}

// 删除患者档案
export function delPatient(recordId) {
  return request({
    url: '/system/patient/' + recordId,
    method: 'delete'
  })
}
