import request from '@/utils/request'

// 查询古方列表
export function listAncientPrescriptionManage(query) {
  return request({
    url: '/system/ancientPrescriptionManage/list',
    method: 'get',
    params: query
  })
}

// 查询古方详细
export function getAncientPrescriptionManage(id) {
  return request({
    url: '/system/ancientPrescriptionManage/' + id,
    method: 'get'
  })
}

// 新增古方
export function addAncientPrescriptionManage(data) {
  return request({
    url: '/system/ancientPrescriptionManage',
    method: 'post',
    data: data
  })
}

// 修改古方
export function updateAncientPrescriptionManage(data) {
  return request({
    url: '/system/ancientPrescriptionManage',
    method: 'put',
    data: data
  })
}

// 删除古方
export function delAncientPrescriptionManage(id) {
  return request({
    url: '/system/ancientPrescriptionManage/' + id,
    method: 'delete'
  })
}
