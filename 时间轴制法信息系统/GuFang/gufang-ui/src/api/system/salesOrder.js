import request from '@/utils/request'

// 查询药品销售列表
export function listSalesOrder(query) {
  return request({
    url: '/system/salesOrder/list',
    method: 'get',
    params: query
  })
}

// 查询药品销售详细
export function getSalesOrder(salesOrderId) {
  return request({
    url: '/system/salesOrder/' + salesOrderId,
    method: 'get'
  })
}

// 新增药品销售
export function addSalesOrder(data) {
  return request({
    url: '/system/salesOrder',
    method: 'post',
    data: data
  })
}

// 修改药品销售
export function updateSalesOrder(data) {
  return request({
    url: '/system/salesOrder',
    method: 'put',
    data: data
  })
}

// 删除药品销售
export function delSalesOrder(salesOrderId) {
  return request({
    url: '/system/salesOrder/' + salesOrderId,
    method: 'delete'
  })
}
