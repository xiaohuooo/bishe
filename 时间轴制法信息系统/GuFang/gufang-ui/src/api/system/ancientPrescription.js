import request from '@/utils/request'

// 查询古方列表
export function listAncientPrescription(query) {
  return request({
    url: '/system/ancientPrescription/list',
    method: 'get',
    params: query
  })
}

// 查询古方详细
export function getAncientPrescription(id) {
  return request({
    url: '/system/ancientPrescription/' + id,
    method: 'get'
  })
}

// 新增古方
export function addAncientPrescription(data) {
  return request({
    url: '/system/ancientPrescription',
    method: 'post',
    data: data
  })
}

// 修改古方
export function updateAncientPrescription(data) {
  return request({
    url: '/system/ancientPrescription',
    method: 'put',
    data: data
  })
}

// 删除古方
export function delAncientPrescription(id) {
  return request({
    url: '/system/ancientPrescription/' + id,
    method: 'delete'
  })
}
