import request from '@/utils/request'

// 查询患者就诊记列表
export function listRecord(query) {
  return request({
    url: '/system/record/list',
    method: 'get',
    params: query
  })
}

// 查询患者就诊记详细
export function getRecord(consultationRecordId) {
  return request({
    url: '/system/record/' + consultationRecordId,
    method: 'get'
  })
}

// 新增患者就诊记
export function addRecord(data) {
  return request({
    url: '/system/record',
    method: 'post',
    data: data
  })
}

// 修改患者就诊记
export function updateRecord(data) {
  return request({
    url: '/system/record',
    method: 'put',
    data: data
  })
}

// 删除患者就诊记
export function delRecord(consultationRecordId) {
  return request({
    url: '/system/record/' + consultationRecordId,
    method: 'delete'
  })
}
