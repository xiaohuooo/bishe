import request from "network/request.js"

export function getAllCosmetic() {
  return request({
    url: "/cosmetic/getAll",
  })
}

export function deleteCosmetic(params) {
  return request({
    url: "/cosmetic/deleteCosmetic/" + params.id,
  })
}

export function deleteCare(params) {
  return request({
    url: "/care/deleteCare/" + params.id,
  })
}

export function getAllAm() {
  return request({
    url: "/medicalAppointment/getAll",
  })
}

export function deleteAm(params) {
  return request({
    url: "/medicalAppointment/delete/" + params.id,
  })
}
export function getTrainById(params) {
  return request({
    url: "/train/getTrainById/" + params.id,
  })
}
export function updateTrainById(params) {
  return request({
    url: "/train/updateTrainById",
    method:"post",
    data:params
  })
}
export function saveTrain(params) {
  return request({
    url: "/train/saveTrain",
    method:"post",
    data:params
  })
}

export function getAllTrain() {
  return request({
    url: "/train/getAll",
  })
}

export function getAllCare() {
  return request({
    url: "/care/getAllCare",
  })
}
export function deleteTrain(params) {
  return request({
    url: "/train/deleteTrain/" + params.id,
  })
}


export function updateBeautyById(params) {
  return request({
    url: "/cosmetic/updateBeautyById",
    method:"post",
    data:params
  })
}

export function getBeautyById(params) {
  return request({
    url: "/cosmetic/getBeautyById/" + params.id,
  })
}

export function saveBeauty(params) {
  return request({
    url: "/cosmetic/saveBeauty",
    method:"post",
    data:params
  })
}

export function save(params) {
  return request({
    url: "/save",
    method:"post",
    data:params
  })
}

export function getAllMedical() {
  return request({
    url: "/medical/getAll",
  })
}

export function saveAm(params) {
  return request({
    url: "/medicalAppointment/save",
    method:"post",
    data:params
  })
}

export function saveFC(params) {
  return request({
    url: "/care/save",
    method:"post",
    data:params
  })
}




