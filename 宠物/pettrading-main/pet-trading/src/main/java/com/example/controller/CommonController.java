package com.example.controller;

import com.example.domain.Medical;
import com.example.domain.Order;
import com.example.service.MedicalService;
import com.example.service.OrderService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class CommonController {

    @Resource
    private OrderService orderService;

    @Resource
    private MedicalService medicalService;

    @PostMapping("/save")
    public void save(@RequestBody Order order){
        orderService.save(order);
    }

    @GetMapping("/medical/getAll")
    public List<Medical> getAll(){
        return medicalService.getAll();
    }
}
