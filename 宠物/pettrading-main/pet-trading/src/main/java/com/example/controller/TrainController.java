package com.example.controller;

import com.example.domain.Train;
import com.example.service.TrainService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/train")
public class TrainController {

    @Resource
    private TrainService trainService;

    @GetMapping("/getAll")
    public Map<String,Object> getAll(){
        HashMap<String, Object> map = new HashMap<>();
        List<Train> trainList = trainService.getAll();
        map.put("data",trainList);
        map.put("pageSize",5);
        int size = trainList.size();
        map.put("total", size);
        int pageCount = 0;
        if (size / 5 == 0){
            pageCount = 1;
        }else {
            pageCount = size / 5;
        }
        map.put("pageCount",pageCount);
        return map;
    }

    @GetMapping("/deleteTrain/{id}")
    public String delete(@PathVariable Integer id){
        return trainService.deleteTrain(id);
    }

    @GetMapping("/getTrainById/{id}")
    public Train getById(@PathVariable Integer id){
        return trainService.getById(id);
    }

    @PostMapping("/updateTrainById")
    public String updateTrainById(Train train){
        return trainService.updateTrainById(train);
    }

    @PostMapping("/saveTrain")
    public String saveTrain(Train train){
        return trainService.saveTrain(train);
    }
}
