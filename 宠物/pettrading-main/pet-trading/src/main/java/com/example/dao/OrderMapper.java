package com.example.dao;

import com.example.domain.Order;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderMapper {

    @Insert("insert into `order` (name, time, uid) values (#{name},#{time},#{uid})")
    void save(Order order);
}
