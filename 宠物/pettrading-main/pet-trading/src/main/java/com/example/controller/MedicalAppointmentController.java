package com.example.controller;

import com.example.domain.MedicalAppointment;
import com.example.service.MedicalAppointmentService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/medicalAppointment")
public class MedicalAppointmentController {

    @Resource
    private MedicalAppointmentService medicalAppointmentService;

    @GetMapping("/getAll")
    public Map<String,Object> getAll(){
        HashMap<String, Object> map = new HashMap<>();
        List<MedicalAppointment> medicalAppointments = medicalAppointmentService.getAll();
        for (MedicalAppointment medicalAppointment : medicalAppointments) {
            String time = medicalAppointment.getTime();
            time = time.substring(0, 10);
            medicalAppointment.setTime(time);
        }
        map.put("data",medicalAppointments);
        map.put("pageSize",5);
        int size = medicalAppointments.size();
        map.put("total", size);
        int pageCount = 0;
        if (size / 5 == 0){
            pageCount = 1;
        }else {
            pageCount = size / 5;
        }
        map.put("pageCount",pageCount);
        return map;
    }


    @PostMapping("/save")
    public String save(@RequestBody MedicalAppointment medicalAppointment){
        medicalAppointmentService.save(medicalAppointment);
        return "保存成功";
    }

    @GetMapping("/delete/{id}")
    public void delete(@PathVariable Integer id){
        medicalAppointmentService.delete(id);
    }
}
