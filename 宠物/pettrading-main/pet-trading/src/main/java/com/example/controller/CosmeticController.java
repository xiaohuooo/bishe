package com.example.controller;

import com.example.domain.Cosmetic;
import com.example.domain.Train;
import com.example.service.CosmeticService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 美容项目管理
 */
@RestController
@RequestMapping("/cosmetic")
public class CosmeticController {

    @Resource
    private CosmeticService cosmeticService;

    @GetMapping("/getAll")
    public Map<String,Object> getAll(){
        HashMap<String, Object> map = new HashMap<>();
        List<Cosmetic> cosmeticList = cosmeticService.getAll();
        map.put("data",cosmeticList);
        map.put("pageSize",5);
        int size = cosmeticList.size();
        map.put("total", size);
        int pageCount = 0;
        if (size / 5 == 0){
            pageCount = 1;
        }else {
            pageCount = size / 5;
        }
        map.put("pageCount",pageCount);
        return map;
    }

    @GetMapping("/deleteCosmetic/{id}")
    public String delete(@PathVariable Integer id){
        return cosmeticService.delete(id);
    }


    @GetMapping("/getBeautyById/{id}")
    public Train getById(@PathVariable Integer id){
        return cosmeticService.getById(id);
    }

    @PostMapping("/updateBeautyById")
    public String updateTrainById(Cosmetic cosmetic){
        return cosmeticService.update(cosmetic);
    }

    @PostMapping("/saveBeauty")
    public String saveTrain(Cosmetic cosmetic){
        return cosmeticService.save(cosmetic);
    }
}
