package com.example.service;

import com.example.dao.FosterCareMapper;
import com.example.domain.FosterCare;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class FosterCareServiceImpl implements FosterCareService{

    @Resource
    private FosterCareMapper fosterCareMapper;

    @Override
    public List<FosterCare> getAll() {
        return fosterCareMapper.getAll();
    }

    @Override
    public String delete(Integer id) {
        fosterCareMapper.delete(id);
        return "删除成功";
    }

    @Override
    public String save(FosterCare fosterCare) {
        fosterCareMapper.save(fosterCare);
        return "保存成功";
    }
}
