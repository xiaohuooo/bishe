package com.example.service;

import com.example.domain.MedicalAppointment;

import java.util.List;

public interface MedicalAppointmentService {
    List<MedicalAppointment> getAll();

    void save(MedicalAppointment medicalAppointment);

    void delete(Integer id);
}
