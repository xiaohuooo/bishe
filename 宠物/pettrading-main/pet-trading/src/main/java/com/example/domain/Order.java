package com.example.domain;

import lombok.Data;

@Data
public class Order {

    private Integer id;

    private Integer uid;

    private String name;

    private String price;

    private String time;
}
