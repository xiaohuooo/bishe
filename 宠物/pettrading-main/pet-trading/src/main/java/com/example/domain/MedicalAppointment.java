package com.example.domain;

import lombok.Data;

import java.util.Date;

@Data
public class MedicalAppointment {

    private Integer id;

    private Integer uid;

    private String time;

    private String price;

    private String name;
}
