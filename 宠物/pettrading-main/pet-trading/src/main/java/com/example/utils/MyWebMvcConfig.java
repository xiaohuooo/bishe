package com.example.utils;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

//配置静态资源映射，解决新上传的文件无法访问的问题
@Configuration
public class MyWebMvcConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        registry.addResourceHandler("/avatar/**").addResourceLocations("file:D:/高级技术视频/20期宠物交易商城系统/pettrading-main/pet-trading/src/main/resources/resources/avatar/");
        registry.addResourceHandler("/avatar/**").addResourceLocations("file:D:/BaiduNetdiskDownload/Java精品项目源码前后端分离项目第20期宠物商城系统/Java精品项目源码前后端分离项目第20期宠物商城系统/pettrading-main/pettrading-main/pet-trading/src/main/resources/resources/avatar/");
//        registry.addResourceHandler("/petimg/**").addResourceLocations("file:D:/高级技术视频/20期宠物交易商城系统/pettrading-main/pet-trading/src/main/resources/resources/petimg/");
        registry.addResourceHandler("/petimg/**").addResourceLocations("file:D:/BaiduNetdiskDownload/Java精品项目源码前后端分离项目第20期宠物商城系统/Java精品项目源码前后端分离项目第20期宠物商城系统/pettrading-main/pettrading-main/pet-trading/src/main/resources/resources/petimg/");
    }
}
