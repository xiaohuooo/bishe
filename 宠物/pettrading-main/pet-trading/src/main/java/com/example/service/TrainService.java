package com.example.service;

import com.example.domain.Cosmetic;
import com.example.domain.Train;

import java.util.List;

public interface TrainService {
    List<Train> getAll();

    String deleteTrain(Integer id);

    Train getById(Integer id);

    String saveTrain(Train train);

    String updateTrainById(Train train);
}
