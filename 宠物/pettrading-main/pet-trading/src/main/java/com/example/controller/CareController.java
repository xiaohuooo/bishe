package com.example.controller;

import com.example.domain.Cosmetic;
import com.example.domain.FosterCare;
import com.example.service.FosterCareService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/care")
public class CareController {

    @Resource
    private FosterCareService fosterCareService;

    @GetMapping("/getAllCare")
    public Map<String,Object> getAll(){
        HashMap<String, Object> map = new HashMap<>();
        List<FosterCare> fosterCares = fosterCareService.getAll();
        map.put("data",fosterCares);
        map.put("pageSize",5);
        int size = fosterCares.size();
        map.put("total", size);
        int pageCount = 0;
        if (size / 5 == 0){
            pageCount = 1;
        }else {
            pageCount = size / 5;
        }
        map.put("pageCount",pageCount);
        return map;
    }

    @GetMapping("/deleteCare/{id}")
    public String delete(@PathVariable Integer id){
        return fosterCareService.delete(id);
    }

    @PostMapping("/save")
    public String save(FosterCare fosterCare){
        return fosterCareService.save(fosterCare);
    }

}
