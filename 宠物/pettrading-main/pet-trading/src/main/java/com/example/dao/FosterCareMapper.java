package com.example.dao;

import com.example.domain.FosterCare;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Mapper
public interface FosterCareMapper {

    @Select("select * from foster_care;")
    List<FosterCare> getAll();


    @Delete("delete from foster_care where id = #{id}")
    void delete(@PathVariable("id") Integer id);

    @Insert("insert into foster_care (time, price, remark, username) values (#{time},#{price},#{remark},#{username})")
    void save(FosterCare fosterCare);
}
