package com.example.service;

import com.example.domain.FosterCare;

import java.util.List;

public interface FosterCareService {
    List<FosterCare> getAll();

    String delete(Integer id);

    String save(FosterCare fosterCare);
}
