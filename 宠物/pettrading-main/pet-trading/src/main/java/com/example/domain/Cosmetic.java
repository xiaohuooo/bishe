package com.example.domain;


import lombok.Data;

/**
 * 美容项目表
 */
@Data
public class Cosmetic {

    /**
     * 主键id
     */
    private Integer id;

    /**
     * 项目名称
     */
    private String name;

    /**
     * 价格
     */
    private String price;

    /**
     * 具体
     */
    private String content;
}
