package com.example.dao;

import com.example.domain.Cosmetic;
import com.example.domain.Train;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface CosmeticMapper {

    @Select("select * from cosmetic")
    List<Cosmetic> getAll();

    @Delete("delete from cosmetic where id = #{id}")
    void delete(@Param("id") Integer id);

    @Select("select * from cosmetic where id = #{id}")
    Train getById(@Param("id") Integer id);

    @Update("update cosmetic set name = #{name},price = #{price},content=#{content} where id = #{id}")
    int update(Cosmetic cosmetic);

    @Insert("insert into cosmetic (name, price, content) values (#{name},#{price},#{content})")
    void save(Cosmetic cosmetic);
}
