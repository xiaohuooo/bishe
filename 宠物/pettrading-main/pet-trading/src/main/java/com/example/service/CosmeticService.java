package com.example.service;

import com.example.domain.Cosmetic;
import com.example.domain.Train;

import java.util.List;

public interface CosmeticService {
    List<Cosmetic> getAll();

    String delete(Integer id);

    String update(Cosmetic cosmetic);

    String save(Cosmetic cosmetic);

    Train getById(Integer id);
}
