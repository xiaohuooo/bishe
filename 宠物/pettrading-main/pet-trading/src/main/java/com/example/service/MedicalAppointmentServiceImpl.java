package com.example.service;

import com.example.dao.MedicalAppointmentMapper;
import com.example.domain.MedicalAppointment;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class MedicalAppointmentServiceImpl implements MedicalAppointmentService{

    @Resource
    private MedicalAppointmentMapper medicalAppointmentMapper;

    @Override
    public List<MedicalAppointment> getAll() {
        return medicalAppointmentMapper.getAll();
    }

    @Override
    public void save(MedicalAppointment medicalAppointment) {
        medicalAppointmentMapper.save(medicalAppointment);
    }

    @Override
    public void delete(Integer id) {
        medicalAppointmentMapper.delete(id);
    }
}
