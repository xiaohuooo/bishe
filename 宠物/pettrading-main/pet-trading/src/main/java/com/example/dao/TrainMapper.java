package com.example.dao;

import com.example.domain.Train;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface TrainMapper {

    @Select("select * from train")
    List<Train> getAll();

    @Delete("delete from train where id = #{id}")
    void delete(@Param("id") Integer id);

    @Select("select * from train where id = #{id}")
    Train getById(@Param("id") Integer id);

    @Insert("insert into train (name, price, content) values (#{name},#{price},#{content})")
    void saveTrain(Train train);

    @Update("update train set name = #{name},price = #{price},content=#{content} where id = #{id}")
    int updateTrainById(Train train);
}
