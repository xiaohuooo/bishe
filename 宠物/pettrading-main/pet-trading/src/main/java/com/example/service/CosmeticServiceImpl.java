package com.example.service;

import com.example.dao.CosmeticMapper;
import com.example.domain.Cosmetic;
import com.example.domain.Train;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CosmeticServiceImpl implements CosmeticService{

    @Resource
    private CosmeticMapper cosmeticMapper;

    @Override
    public List<Cosmetic> getAll() {
        return cosmeticMapper.getAll();
    }

    @Override
    public String delete(Integer id) {
        cosmeticMapper.delete(id);
        return "删除成功";
    }

    @Override
    public Train getById(Integer id) {
        return cosmeticMapper.getById(id);
    }

    @Override
    public String save(Cosmetic cosmetic) {
        cosmeticMapper.save(cosmetic);
        return "保存成功";
    }

    @Override
    public String update(Cosmetic cosmetic) {
        int i = cosmeticMapper.update(cosmetic);
        System.out.println(i);
        return "更新成功";
    }
}
