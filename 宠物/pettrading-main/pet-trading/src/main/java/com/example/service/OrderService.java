package com.example.service;

import com.example.domain.Order;

public interface OrderService {
    void save(Order order);
}
