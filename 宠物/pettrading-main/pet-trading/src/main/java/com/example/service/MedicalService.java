package com.example.service;

import com.example.domain.Medical;

import java.util.List;

public interface MedicalService {
    List<Medical> getAll();
}
