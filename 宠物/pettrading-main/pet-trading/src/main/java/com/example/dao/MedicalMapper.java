package com.example.dao;

import com.example.domain.Medical;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface MedicalMapper {

    @Select("select * from medical")
    List<Medical> getAll();
}
