package com.example.service;

import com.example.dao.MedicalMapper;
import com.example.domain.Medical;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class MedicalServiceImpl implements MedicalService{

    @Resource
    private MedicalMapper medicalMapper;

    @Override
    public List<Medical> getAll() {
        return medicalMapper.getAll();
    }
}
