package com.example.dao;

import com.example.domain.MedicalAppointment;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Mapper
public interface MedicalAppointmentMapper {

    @Select("select * from medical_appointment")
    List<MedicalAppointment> getAll();

    @Insert("insert into medical_appointment (uid, time, price,name) " +
            "VALUES (#{uid},#{time},#{price},#{name})")
    void save(MedicalAppointment medicalAppointment);

    @Delete("delete from medical_appointment where id = #{id}")
    void delete(@PathVariable Integer id);
}
