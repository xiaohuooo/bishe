package com.example.domain;

import lombok.Data;

@Data
public class Medical {


    private Integer id;

    private String name;

    private String price;

    private String img;
}

