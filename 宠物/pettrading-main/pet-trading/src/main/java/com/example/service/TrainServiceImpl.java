package com.example.service;

import com.example.dao.TrainMapper;
import com.example.domain.Train;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class TrainServiceImpl implements TrainService{

    @Resource
    private TrainMapper trainMapper;

    @Override
    public List<Train> getAll() {
        return trainMapper.getAll();
    }

    @Override
    public String deleteTrain(Integer id) {
        trainMapper.delete(id);
        return "删除成功";
    }

    @Override
    public Train getById(Integer id) {
        return trainMapper.getById(id);
    }

    @Override
    public String saveTrain(Train train) {
        trainMapper.saveTrain(train);
        return "保存成功";
    }

    @Override
    public String updateTrainById(Train train) {
        int i = trainMapper.updateTrainById(train);
        System.out.println(i);
        return "更新成功";
    }
}
