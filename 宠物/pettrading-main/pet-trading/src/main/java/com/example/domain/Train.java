package com.example.domain;

import lombok.Data;

/**
 * 训练服务表
 */
@Data
public class Train {

    private Integer id;

    /**
     * 训练服务
     */
    private String name;

    /**
     * 费用
     */
    private String price;

    /**
     * 内容
     */
    private String content;
}
