package com.example.domain;

import lombok.Data;

/**
 * 宠物寄养表
 */
@Data
public class FosterCare {

    private Integer id;

    /**
     * 寄养时间
     */
    private String time;

    /**
     * 费用
     */
    private String price;

    /**
     * 注意事项
     */
    private String remark;


    private String username;
}
